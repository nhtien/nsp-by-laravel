<?php

namespace App\Console\Commands;

use App\Events\FolderCreatedByProductEvent;
use App\Models\Product;
use Illuminate\Console\Command;

class ProductFolderCreation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:folder-creation';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tạo folder chứa images, datasheet cho product';

    public function handle(): void
    {
        /**
         * @var Product $productModel
         */
        $productModel = app('ProductModel');
        $products = $productModel::parentQuery()->select(['prod_id', 'part_number'])->cursor();
        foreach ($products as $product) {
            event(new FolderCreatedByProductEvent($product));
        }
    }
}
