<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RemoveSession extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'session:remove-file';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tự động xóa session 1 tháng/lần';

    public function handle(): void
    {
        $sessionPath = storage_path('framework/sessions');
        $sessions = get_file_name_in_folder($sessionPath);
        foreach ($sessions as $session) {
            delete_file($sessionPath.'/'.$session);
        }
    }
}
