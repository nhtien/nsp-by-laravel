<?php

namespace App\Enums;

/**
 * https://stitcher.io/blog/php-enums
 */
enum Product: string
{
    case DATASHEET_PATH = 'uploads/datasheet/';
    case PRODUCT_RELATED_PATH = 'uploads/images/product--related/';
    case PRODUCT_THUMBNAIL_PATH = 'uploads/images/thumbs/product/';
    case PRODUCT_FILE_PATH = 'uploads/product--file/';

    public function path(): string
    {
        return match ($this) {
            self::DATASHEET_PATH => public_path(self::DATASHEET_PATH->value),
            self::PRODUCT_RELATED_PATH => public_path(self::PRODUCT_RELATED_PATH->value),
            self::PRODUCT_THUMBNAIL_PATH => public_path(self::PRODUCT_THUMBNAIL_PATH->value),
            self::PRODUCT_FILE_PATH => public_path(self::PRODUCT_FILE_PATH->value),
        };
    }
}
