<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WriteModelLogEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $model;

    public string $eventName;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($model, $eventName = 'deleted')
    {
        $this->model = $model;
        $this->eventName = $eventName;
    }
}
