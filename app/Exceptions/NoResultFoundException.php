<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * https://viblo.asia/p/exceptions-trong-laravel-lam-the-nao-de-catch-handle-va-tu-tao-mot-exception-de-xu-ly-van-de-cua-rieng-minh-bJzKmGnOl9N
 */
class NoResultFoundException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     *
     * @param  Request  $request
     */
    public function render($request): Response
    {
        return response()->view('errors.404-no-result-found');
    }
}
