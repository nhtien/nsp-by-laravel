<?php

namespace App\Helpers;

class Partition
{
    private $configs = [
        'tblcheck_in_out' => [
            'type' => 'month',
            'from' => '2023-01',
            'to' => '2024-12',
        ],
        'tbltimesheets_timesheet' => [
            'type' => 'year',
            'from' => '2014',
            'to' => '2024',
        ],
    ];

    public function getPartitionNameByMonth($tableName, string $from, string $to)
    {
        if (! isset($this->configs[$tableName]) || $this->configs[$tableName]['type'] != 'month' || strtotime($from) > strtotime($to)) {
            return '';
        }

        $fromConfig = $this->configs[$tableName]['from'];
        $toConfig = $this->configs[$tableName]['to'];

        if (strtotime($fromConfig) > strtotime($from)) {
            $startYear = date('Y', strtotime($fromConfig));
            $startMonth = date('m', strtotime($fromConfig));
        } else {
            $startYear = date('Y', strtotime($from));
            $startMonth = date('m', strtotime($from));
        }

        if (strtotime($toConfig) < strtotime($to)) {
            $endYear = date('Y', strtotime($toConfig));
            $endMonth = date('m', strtotime($toConfig));
        } else {
            $endYear = date('Y', strtotime($to));
            $endMonth = date('m', strtotime($to));
        }

        if ($startYear === $endYear && $startMonth === $endMonth) {
            return "p{$startYear}{$startMonth}";
        }

        $partitionNameArr = [];
        for ($year = $startYear; $year <= $endYear; $year++) {
            $monthStart = ($year === $startYear) ? $startMonth : 1;
            $monthEnd = ($year === $endYear) ? $endMonth : 12;

            for ($month = $monthStart; $month <= $monthEnd; $month++) {
                $formattedMonth = sprintf('%02d', $month);
                $partitionNameArr[] = 'p'.$year.$formattedMonth;
            }
        }

        if (count($partitionNameArr)) {
            return implode(',', $partitionNameArr);
        }

        return '';
    }

    public function getPartitionNameByYear($tableName, int $from, int $to)
    {
        if (! isset($this->configs[$tableName]) || $this->configs[$tableName]['type'] != 'year' || $from > $to) {
            return '';
        }

        $fromConfig = $this->configs[$tableName]['from'];
        $toConfig = $this->configs[$tableName]['to'];

        $startYear = ($fromConfig > $from) ? $fromConfig : $from;
        $endYear = ($to > $toConfig) ? $toConfig : $to;

        if ($startYear === $endYear) {
            return "p{$startYear}";
        }

        $partitionNameArr = [];
        for ($year = $startYear; $year <= $endYear; $year++) {
            $partitionNameArr[] = 'p'.$year;
        }

        if (count($partitionNameArr)) {
            return implode(',', $partitionNameArr);
        }

        return '';
    }
}
