<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\MenuCollection;
use App\Models\MenuFrontend;
use Exception;
use Illuminate\Http\JsonResponse;

class MenuController extends BaseController
{
    public function __construct(public MenuFrontend $menuFrontendModel)
    {
    }

    public function index(): JsonResponse
    {
        try {
            $menus = $this->menuFrontendModel->findsMenuHeader();
            $menus = collect($menus)->groupBy('level');
            $menusLevel1 = $menus->get(1);
            $menusLevel2 = $menus->get(2);
            $menusLevel3 = $menus->get(3);

            foreach ($menusLevel2 as $index => $menu) {
                $items = collect($menusLevel3)->where('parent', $menu->id);
                if ($items->count()) {
                    $menusLevel2[$index]['childrens'] = $items->toArray();
                }
            }
            foreach ($menusLevel1 as $index => $menu) {
                $items = collect($menusLevel2)->where('parent', $menu->id);
                if ($items->count()) {
                    $menusLevel1[$index]['childrens'] = $items->toArray();
                }
            }
            $menus = new MenuCollection($menusLevel1);
        } catch (Exception $e) {
            return $this->sendError('Con not get menus', [], 422);
        }

        return $this->sendResponse($menus, 'Get menus successful');
    }
}
