<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PostCollection;
use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class PostController extends BaseController
{
    public function __construct(
        public Post $postModel,
    ) {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function findsByCate($catId): JsonResponse
    {
        $keyword = trim(request()?->get('keyword'));
        $posts = $this->postModel::query()
            ->where('pgrp_id', $catId)
            ->where('status', 'activated')
            ->when(! empty($keyword), function ($q) use ($keyword) {
                return $q->whereFullText('content', "$keyword");
            })
            ->paginate(30);
        if (! $posts) {
            return $this->sendResponse([], 'Get posts successful');
        }

        $posts = new PostCollection($posts);

        return $this->sendResponse($posts, 'Get posts successful');
    }
}
