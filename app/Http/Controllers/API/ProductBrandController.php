<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductBrandCollection;
use App\Models\ProductBrand;
use Exception;
use Illuminate\Http\JsonResponse;

class ProductBrandController extends BaseController
{
    public function __construct(public ProductBrand $productBrandModel)
    {
    }

    public function index(): JsonResponse
    {
        try {
            $brands = $this->productBrandModel::query()->where('status', 'activated')->get();
            $brands = new ProductBrandCollection($brands);
        } catch (Exception $e) {
            return $this->sendError('Con not get product brand', [], 422);
        }

        return $this->sendResponse($brands, 'Get product brand successful');
    }
}
