<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductCategoryCollection;
use App\Http\Resources\ProductCategoryResource;
use App\Models\ProductCategory;
use Exception;
use Illuminate\Http\JsonResponse;

class ProductCategoryController extends BaseController
{
    public function __construct(public ProductCategory $productCategoryModel)
    {
    }

    public function getHeaderMenu(): JsonResponse
    {
        try {
            $categories = $this->productCategoryModel->findsHeaderMenu();

            $menus = collect($categories)->groupBy('level');
            $catsLevel1 = $menus->get(1);
            $catsLevel2 = $menus->get(2);

            foreach ($catsLevel1 as $index => $cat) {
                $items = collect($catsLevel2)->where('parent', $cat->prcat_id);
                if ($items->count()) {
                    $catsLevel1[$index]['childrens'] = $items->toArray();
                }
            }
            $cats = new ProductCategoryCollection($catsLevel1);
        } catch (Exception $e) {
            return $this->sendError('Can not get product cat', [], 422);
        }

        return $this->sendResponse($cats, 'Get categories successful');
    }

    public function getCategoriesByLevel($level): JsonResponse
    {
        $categories = $this->productCategoryModel->findsByLevel($level, 'activated');
        if ($categories->count()) {
            $categories = new ProductCategoryCollection($categories);

            return $this->sendResponse($categories, 'Get product category successful');
        }

        return $this->sendError('Category not found', [], 422);
    }

    public function getChildrenCategoriesByBranch($catId): JsonResponse
    {
        $categories = $this->productCategoryModel->findsChildrenByBranch($catId, 'activated', 3);
        if ($categories->count()) {
            $categories = new ProductCategoryCollection($categories);

            return $this->sendResponse($categories, 'Get product category successful');
        }

        return $this->sendError('Category not found', [], 422);
    }

    public function getCategoriesByBranch($catId): JsonResponse
    {
        $categories = $this->productCategoryModel->findsByBranch($catId, 'activated', 3);
        if ($categories->count()) {
            $categories = new ProductCategoryCollection($categories);

            return $this->sendResponse($categories, 'Get product category successful');
        }

        return $this->sendError('Category not found', [], 422);
    }

    public function detail($id): JsonResponse
    {
        $category = $this->productCategoryModel::query()->where('status', 'activated')->find($id);
        if ($category) {
            $category = new ProductCategoryResource($category);

            return $this->sendResponse($category, 'Get product category successful');
        }

        return $this->sendError('Category not found', [], 422);
    }

    public function getFirstAncestor($id): JsonResponse
    {
        $category = $this->productCategoryModel->findFirstAncestor($id, 'activated');
        if ($category) {
            $category = new ProductCategoryResource($category);

            return $this->sendResponse($category, 'Get product category successful');
        }

        return $this->sendError('Category not found', [], 422);
    }
}
