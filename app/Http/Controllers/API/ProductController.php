<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductChildrenCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductDatasheetCollection;
use App\Http\Resources\ProductPropertyCollection;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductTagCollection;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductChildren;
use App\Models\ProductDatasheet;
use App\Models\ProductProperty;
use App\Models\ProductTag;
use Illuminate\Http\JsonResponse;
use JsonException;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductController extends BaseController
{
    public function __construct(
        public Product $productModel,
        public ProductCategory $productCategoryModel,
        public ProductProperty $productPropertyModel,
        public ProductChildren $productChildrenModel,
        public ProductDatasheet $productDatasheetModel,
        public ProductTag $productTagModel
    ) {
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function getProductsByCategory($catId): JsonResponse
    {
        $brandId = request()->get('brandId');
        $tagId = request()->get('tagId');
        /**
         * @var ProductCategory $categories
         */
        $categories = $this->productCategoryModel->findsByBranch($catId);
        if ($categories->count() < 1) {
            return $this->sendError('Category not found', [], 422);
        }

        $catIds = explode('||', $categories->implode('prcat_id', '||'));

        $products = $this->productModel::query()
            ->select('*')
            ->when($brandId != 0, function ($q) use ($brandId) {
                return $q->where('prodbrd_id', $brandId);
            })
            ->when($tagId != 0, function ($q) use ($tagId) {
                return $q->whereJsonContains('product_tag_ids->tag_ids', $tagId);
            })
            ->where('status', 'activated')
            ->whereIn('prcat_id', $catIds)
            ->orderBy('prod_id', 'desc')
            ->paginate(18);

        $prods = new ProductCollection($products);

        return $this->sendResponse($prods, 'Get products successful');
    }

    /**
     * @throws JsonException
     */
    public function detail($id): JsonResponse
    {
        $product = $this->productModel::query()
            ->select(['products.*', 'product_brands.name as prodbrd_name'])
            ->join('product_brands', 'product_brands.prodbrd_id', 'products.prodbrd_id')
            ->where('products.status', 'activated')
            ->find($id);
        if ($product) {
            /*
            |--------------------------------------------------------------------------
            | Get properties of product
            |--------------------------------------------------------------------------
            |
            | Thêm property vào product
            |
            */
            $properties = $this->productPropertyModel::query()
                ->where('status', 'activated')
                ->where('prod_id', $product->prod_id)
                ->orderBy('rank', 'asc')
                ->get();
            if ($properties->count()) {
                $properties = new ProductPropertyCollection($properties);
                $propertyArr = json_decode($properties->toJson(), true, 512, JSON_THROW_ON_ERROR);
                $product->setAttribute('product_properties', $propertyArr['product-properties']);
            }

            /*
            |--------------------------------------------------------------------------
            | Get children items of product
            |--------------------------------------------------------------------------
            |
            | Thêm vào product
            |
            */
            $childrens = $this->productChildrenModel::query()
                ->where('status', 'activated')
                ->where('prod_id', $product->prod_id)
                ->orderBy('rank', 'asc')
                ->get();
            if ($childrens->count()) {
                $childrens = new ProductChildrenCollection($childrens);
                $childrenArr = json_decode($childrens->toJson(), true, 512, JSON_THROW_ON_ERROR);
                $product->setAttribute('product_childrens', $childrenArr['product-childrens']);
            }

            /*
            |--------------------------------------------------------------------------
            | Get datasheet items of product
            |--------------------------------------------------------------------------
            |
            | Thêm vào product
            |
            */
            $datasheets = $this->productDatasheetModel::query()
                ->select(['product_datasheet.*', 'product_datasheet_groups.name as prodsheetgrp_name'])
                ->join('product_datasheet_groups', 'product_datasheet_groups.prdagrp_id', 'product_datasheet.prdagrp_id')
                ->where([
                    'product_datasheet.status' => 'activated',
                    'product_datasheet.prod_id' => $product->prod_id,
                ])->orderBy('product_datasheet.rank', 'asc')
                ->get();
            if ($datasheets->count()) {
                $datasheets = new ProductDatasheetCollection($datasheets);
                $datasheetArr = json_decode($datasheets->toJson(), true, 512, JSON_THROW_ON_ERROR);
                $product->setAttribute('product_datasheets', $datasheetArr['product-datasheets']);
            }

            /*
            |--------------------------------------------------------------------------
            | Get tag items of product
            |--------------------------------------------------------------------------
            |
            | Thêm vào product
            |
            */
            if (count($product->product_tag_ids)) {
                $tags = $this->productTagModel::query()
                    ->where('status', 'activated')
                    ->whereIn('prtag_id', $product->product_tag_ids)
                    ->get();
                if ($tags->count()) {
                    $tags = new ProductTagCollection($tags);
                    $tagArr = json_decode($tags->toJson(), true, 512, JSON_THROW_ON_ERROR);
                    $product->setAttribute('product_tags', $tagArr['product-tags']);
                }
            }

            $product = new ProductResource($product);
        }

        return $this->sendResponse($product, 'Get products successful');
    }

    public function getProductsRelated($id): JsonResponse
    {
        $product = $this->productModel::query()->where('status', 'activated')->find($id);
        if (! $product) {
            return $this->sendResponse([], 'Get products successful');
        }

        $products = $this->productModel::query()
            ->select(['products.*', 'product_brands.name as prodbrd_name'])
            ->join('product_brands', 'product_brands.prodbrd_id', 'products.prodbrd_id')
            ->where('products.status', 'activated')
            ->where('products.prcat_id', $product->prcat_id)
            ->where('products.prod_id', '<>', $id)
            ->limit(5)
            ->inRandomOrder()
            ->get();
        $prods = new ProductCollection($products);

        return $this->sendResponse($prods, 'Get products successful');
    }

    public function getProductsByTag($tagId): JsonResponse
    {
        $products = $this->productModel::parentQuery()
            ->where('status', 'activated')
            ->whereJsonContains('product_tag_ids->tag_ids', (string) $tagId)
            ->paginate();

        if (! $products) {
            return $this->sendResponse([], 'Get products successful');
        }

        return $this->sendResponse(new ProductCollection($products), 'Get products successful');
    }

    public function getProductsBySolution($id): JsonResponse
    {
        $keyword = trim(request()->get('keyword'));

        $products = $this->productModel::query()
            ->whereJsonContains('solution_ids->solution_ids', $id)
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('part_number', $keyword);
                    $q->orWhere('part_number_old', $keyword);
                    $q->orWhereFullText('name', $keyword);
                });
            })
            ->where('status', 'activated')
            ->orderBy('prod_id', 'desc')->paginate();
        if (! $products) {
            return $this->sendResponse([], 'Get products successful');
        }

        return $this->sendResponse(new ProductCollection($products), 'Get products successful');
    }

    public function getProductsByBrand($id): JsonResponse
    {
        $keyword = trim(request()->get('keyword'));

        $products = $this->productModel::query()
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('part_number', $keyword);
                    $q->orWhere('part_number_old', $keyword);
                    $q->orWhereFullText('name', $keyword);
                });
            })
            ->where('prodbrd_id', $id)
            ->where('status', 'activated')
            ->orderBy('prod_id', 'desc')->paginate();
        if (! $products) {
            return $this->sendResponse([], 'Get products successful');
        }

        return $this->sendResponse(new ProductCollection($products), 'Get products successful');
    }
}
