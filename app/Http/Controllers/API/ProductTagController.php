<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductTagCollection;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use Exception;
use Illuminate\Http\JsonResponse;

class ProductTagController extends BaseController
{
    public function __construct(
        public ProductTag $productTagModel,
        public ProductCategory $productCategoryModel
    ) {
    }

    public function getTagsByBranch($branchId): JsonResponse
    {
        $categories = $this->productCategoryModel->findsByBranch($branchId, 'activated');
        $tagIds = [];
        foreach ($categories as $category) {
            $tagIds = array_merge($tagIds, $category->product_tag_ids);
        }

        if (count($tagIds) < 1) {
            return $this->sendResponse([], 'Get product tags successful');
        }

        try {
            $tags = $this->productTagModel::query()
                ->whereIn('prtag_id', $tagIds)
                ->where('status', 'activated')
                ->orderBy('name', 'asc')
                ->get();
            $tags = new ProductTagCollection($tags);
        } catch (Exception $e) {
            return $this->sendError('Error', [], 422);
        }

        return $this->sendResponse($tags, 'Get product tag successful');
    }
}
