<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\SliderCollection;
use App\Models\Slider;
use Exception;
use Illuminate\Http\JsonResponse;

class SliderController extends BaseController
{
    public function __construct(public Slider $sliderModel)
    {
    }

    public function index(): JsonResponse
    {
        try {
            $sliders = $this->sliderModel::parentQuery()
                ->where('status', 'activated')
                ->orderBy('position', 'asc')
                ->get();
            $sliders = new SliderCollection($sliders);
        } catch (Exception $e) {
            return $this->sendError('Con not get slider', [], 422);
        }

        return $this->sendResponse($sliders, 'Get slider successful');
    }
}
