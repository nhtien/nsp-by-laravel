<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\ProductBrandCollection;
use App\Models\Solution;
use Exception;
use Illuminate\Http\JsonResponse;

class SolutionController extends BaseController
{
    public function __construct(public Solution $solutionModel)
    {
    }

    public function index(): JsonResponse
    {
        try {
            $solutions = $this->solutionModel::query()->where('status', 'activated')->get();
            //$solutions = new ProductBrandCollection($solutions);
        } catch (Exception $e) {
            return $this->sendError('Con not get product brand', [], 422);
        }

        return $this->sendResponse($solutions, 'Get product brand successful');
    }
}
