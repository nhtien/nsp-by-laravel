<?php

namespace App\Http\Controllers\BE\Auth;

use App\Http\Controllers\BE\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    private string $loginForm = '/login.html';

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @throws ValidationException
     */
    public function login(Request $request): Redirector|RedirectResponse|Application
    {
        /**
         * Validate the form data
         */
        $this->validate($request, [
            'username' => 'bail|required',
            'password' => 'bail|required',
        ]);

        /**
         * Verify user
         */
        $credentials = $request->only('username', 'password');
        $rememberMe = (bool) $request->get('remember-me');
        if (Auth::guard()->attempt(array_merge($credentials, ['status' => 'activated']), $rememberMe)) {
            $request->session()->regenerate();

            return redirect()->route('dashboard-backend');
        } else {
            return redirect($this->loginForm)->withErrors(['msg' => 'Tài khoản không đúng !'], 'login');
        }
    }

    public function loginForm(): View|Factory|Application
    {
        return view('be.login.login_v2');
    }
}
