<?php

namespace App\Http\Controllers\BE;

use App\Models\Contact;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ContactController extends Controller
{
    public function __construct(public Contact $contactModel)
    {
        parent::__construct();
    }

    public function index(): View|Factory|Application
    {
        $contacts = $this->contactModel->searching();

        return view('be.contact.index', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function view($id)
    {
        $contact = $this->contactModel::query()->find($id);

        abort_if(! $contact, 404);

        $contact->is_new = 0;
        $contact->save();

        return view('be.contact.view', [
            'contact' => $contact,
        ]);
    }
}
