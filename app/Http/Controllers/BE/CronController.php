<?php

namespace App\Http\Controllers\BE;

use App\Events\FolderCreatedByProductEvent;
use App\Models\Product;
use Illuminate\Http\JsonResponse;

class CronController extends Controller
{
    public function __construct(protected Product $productModel)
    {
        parent::__construct();
    }

    /**
     * Tạo folder chứa image và datasheet của product.
     * Event tạo folder cũng sẽ được chạy trong 'Model updated event and created event'
     *
     * @return JsonResponse
     */
    public function createFolderByProduct()
    {
        $products = $this->productModel::all(['prod_id', 'part_number']);
        foreach ($products as $product) {
            event(new FolderCreatedByProductEvent($product));
        }

        return response()->json([
            'status' => true,
            'message' => 'Đã tạo folder của sản phẩm thành công',
        ]);
    }
}
