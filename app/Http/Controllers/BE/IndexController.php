<?php

namespace App\Http\Controllers\BE;

use App\Models\Post;
use App\Models\Product;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class IndexController extends Controller
{
    public User $userModel;

    public Post $postModel;

    public Product $productModel;

    public function __construct(User $userModel, Post $postModel, Product $productModel)
    {
        parent::__construct();
        $this->userModel = $userModel;
        $this->postModel = $postModel;
        $this->productModel = $productModel;
    }

    public function dashboard(): Factory|View|Application
    {
        return view('be.index.dashboard', [
            'totalUsers' => $this->userModel->totalUsers(),
            'totalPosts' => $this->postModel->totalPosts(),
            'totalProducts' => $this->productModel->totalProducts(),
        ]);
    }

    public function cache(): View|Factory|Application
    {
        return \view('be.index.cache');
    }
}
