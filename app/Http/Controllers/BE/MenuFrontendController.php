<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\MenuFrontendRequest;
use App\Models\MenuFrontend;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class MenuFrontendController extends Controller
{
    public MenuFrontend $menuFrontendModel;

    public function __construct(MenuFrontend $menuFrontendModel)
    {
        parent::__construct();
        $this->menuFrontendModel = $menuFrontendModel;
    }

    public function index(): View|Factory|Application
    {
        $menus = $this->menuFrontendModel::query()->orderBy('left', 'asc')->paginate();

        return view('be.menu-frontend.index', [
            'menus' => $menus,
            'menuSpecial' => $this->menuFrontendModel::SPECIAL,
        ]);
    }

    public function create(): View|Factory|Application
    {
        $menus = $this->menuFrontendModel->getAllItems(0);

        return view('be.menu-frontend.create', [
            'menuStatus' => $this->menuFrontendModel::STATUS,
            'menus' => $menus,
            'menuSpecial' => $this->menuFrontendModel::SPECIAL,
        ]);
    }

    /**
     * @throws Exception
     */
    public function store(MenuFrontendRequest $menuFrontendRequest): RedirectResponse
    {
        $data = $this->menuFrontendModel->getDataFields($menuFrontendRequest->post());
        $this->menuFrontendModel->createNode($data, $data['position']);

        return redirect()->route('be.menufrontend.index')->with('success', config('message.store_successful'));
    }

    public function show($id): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $id = \request()->get('id', 0);
        if (! $menu = $this->menuFrontendModel::query()->find($id)) {
            throw new NoResultFoundException();
        }
        $menus = $this->menuFrontendModel->getAllItems($id);

        return \view('be.menu-frontend.edit', [
            'menu' => $menu,
            'menus' => $menus,
            'menuStatus' => $this->menuFrontendModel::STATUS,
            'menuSpecial' => $this->menuFrontendModel::SPECIAL,
        ]);
    }

    /**
     * @throws Exception
     */
    public function update(MenuFrontendRequest $menuFrontendRequest): RedirectResponse
    {
        $menuId = \request()->post('menu_fe_id');
        $menu = $this->menuFrontendModel::query()->find($menuId);
        $data = $this->menuFrontendModel->getDataFields($menuFrontendRequest->post());

        $positionNode = \request()->post('menu_fe_position');
        $this->menuFrontendModel->updateNode($data, $menuId, $positionNode);

        if (\request()->post('menu_fe_status') == 'activated') {
            // Thực hiện update dữ liệu (Danh sách node tổ tiên và bản thân node đó)
            $this->menuFrontendModel::parentQuery()->breadcrumb($menu->left, $menu->right)->update(['status' => 'activated']);
        }

        return redirect()->route('be.menufrontend.index')->with('success', config('message.update_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function destroy(): RedirectResponse
    {
        $params['id'] = \request()->get('id', 0);
        $this->menuFrontendModel->deleteNode($params);

        return redirect()->route('be.menufrontend.index')->with('success', config('message.destroy_successful'));
    }
}
