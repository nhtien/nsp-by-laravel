<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Models\ModelLog;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ModelLogController extends Controller
{
    public ModelLog $modelLog;

    public function __construct(ModelLog $modelLog)
    {
        parent::__construct();
        $this->modelLog = $modelLog;
    }

    public function index(): Factory|View|Application
    {
        $modelLogs = $this->modelLog::query()->with('user')->orderBy('id', 'desc')->paginate();

        return view('be.model-log.index', [
            'modelLogs' => $modelLogs,
        ]);
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function show(): View|Factory|Application
    {
        $id = request()->get('id', 0);
        if (! $modelLog = $this->modelLog::query()->with('user')->find($id)) {
            throw new NoResultFoundException();
        }

        return view('be.model-log.show', [
            'modelLog' => $modelLog,
        ]);
    }
}
