<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\PostGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

class PostController extends Controller
{
    protected Post $postModel;

    protected PostGroup $postGroupModel;

    public function __construct(Post $postModel, PostGroup $postGroupModel)
    {
        parent::__construct();
        $this->postModel = $postModel;
        $this->postGroupModel = $postGroupModel;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): View|Factory|Application
    {
        $posts = $this->postModel->searching();
        $postGroups = $this->postGroupModel::parentQuery()->get();

        return view('be.post.index', [
            'posts' => $posts,
            'pstStatus' => $this->postModel::STATUS,
            'postGroups' => $postGroups,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Factory|View|Application
    {
        $userGroups = $this->postGroupModel::query()->get();

        return view('be.post.create', [
            'postGroups' => $userGroups,
            'pstStatus' => $this->postModel::STATUS,
        ]);
    }

    public function store(PostRequest $postRequest): RedirectResponse
    {
        $data = $this->postModel->getDataFields($postRequest->post(), ['views']);
        $data['user_id'] = auth()->user()->user_id;
        $post = $this->postModel::query()->create($data);

        // upload thumbnail
        $this->postModel->uploadAndSaveThumbnail('pst_thumbnail', $post, false);

        return redirect()->route('be.post.index')->with('success', \config('message.store_successful'));
    }

    public function show(): Response|bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function edit(): View|Factory|Application
    {
        $id = \request()->get('id', 0);
        //$this->authorize('update', $post);
        $post = $this->postModel::query()->find($id);
        throw_if(! $post, new NoResultFoundException());

        $postGroups = $this->postGroupModel::parentQuery()->get();

        return \view('be.post.edit', [
            'post' => $post,
            'postGroups' => $postGroups,
            'pstStatus' => $this->postModel::STATUS,
        ]);
    }

    public function update(PostRequest $postRequest): RedirectResponse
    {
        $post = $this->postModel::query()->find(\request()->post('pst_id'));
        $data = $this->postModel->getDataFields(
            $postRequest->post(),
            ['views', 'user_id', 'short_name', 'short_name_en', 'thumbnail']
        );
        $post->update($data);
        // upload thumbnail
        $this->postModel->uploadAndSaveThumbnail('pst_thumbnail', $post);

        return redirect()->route('be.post.index')->with('success', \config('message.update_successful'));
    }

    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->postModel::query()->find($id)->delete();

        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
