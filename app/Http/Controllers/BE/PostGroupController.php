<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\PostGroupRequest;
use App\Models\PostGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class PostGroupController extends Controller
{
    public PostGroup $postGroupModel;

    public function __construct(PostGroup $postGroupModel)
    {
        parent::__construct();
        $this->postGroupModel = $postGroupModel;
    }

    public function index(): View|Factory|Application
    {
        $postGroups = $this->postGroupModel::query()->paginate();

        return view('be.post-group.index', ['postGroups' => $postGroups]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.post-group.create', [
            'groupStatus' => $this->postGroupModel::STATUS,
        ]);
    }

    public function store(PostGroupRequest $postGroupRequest): RedirectResponse
    {
        $data = $this->postGroupModel->getDataFields($postGroupRequest->post());
        $this->postGroupModel::parentQuery()->create($data);

        return redirect()->route('be.postgroup.index')->with('success', \config('message.store_successful'));
    }

    public function show($id)
    {
        //
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws \Throwable
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        $postGroup = $this->postGroupModel::query()->find($id);
        throw_if(! $postGroup, new NoResultFoundException());

        return \view('be.post-group.edit', [
            'groupStatus' => $this->postGroupModel::STATUS,
            'postGroup' => $postGroup,
        ]);
    }

    public function update(PostGroupRequest $postGroupRequest): RedirectResponse
    {
        $postGroup = $this->postGroupModel::query()->find(\request()->post('pst_grp_id'));
        $data = $this->postGroupModel->getDataFields($postGroupRequest->post());
        $postGroup->update($data);

        return redirect()->route('be.postgroup.index')->with('success', \config('message.update_successful'));
    }

    public function destroy($id)
    {
    }
}
