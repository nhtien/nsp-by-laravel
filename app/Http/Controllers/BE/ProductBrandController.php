<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductBrandRequest;
use App\Models\ProductBrand;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductBrandController extends Controller
{
    public ProductBrand $mainModel;

    public function __construct(ProductBrand $mainModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
    }

    public function index(): Factory|View|Application
    {
        $productBrands = $this->mainModel::parentQuery()->paginate();

        return view('be.product-brand.index', [
            'productBrands' => $productBrands,
            'brandStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.product-brand.create', [
            'brandStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(ProductBrandRequest $productBrandRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($productBrandRequest->post());
        $this->mainModel::parentQuery()->create($data);

        return redirect()->route('be.productbrand.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $brand = $this->mainModel::parentQuery()->find($id)) {
            throw new NoResultFoundException();
        }

        return \view('be.product-brand.edit', [
            'brand' => $brand,
            'brandStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(ProductBrandRequest $productBrandRequest): RedirectResponse
    {
        $brand = $this->mainModel::query()->find(\request()->post('prod_brd_id'));
        $data = $this->mainModel->getDataFields($productBrandRequest->post());
        $brand->update($data);

        return redirect()->route('be.productbrand.index')->with('success', \config('message.update_successful'));
    }

    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->mainModel::query()->find($id)->delete();

        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
