<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductCategoryRequest;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductCategoryController extends Controller
{
    public ProductTag $productTagModel;

    public ProductCategory $mainModel;

    public function __construct(ProductCategory $mainModel, ProductTag $productTagModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
        $this->productTagModel = $productTagModel;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): View|Factory|Application
    {
        $cats = $this->mainModel->searching();

        // Lấy phần tử cha
        $parentItems = [];
        if ($cats->isNotEmpty()) {
            $parentIds = convert_ids_to_array(collect($cats->getCollection())->implode('parent', '||'));
            $parentIds = collect($parentIds)->unique()->toArray();

            $catsParent = $this->mainModel::query()->select('prcat_id', 'name')->whereIn('prcat_id', $parentIds)->get();
            $parentItems = collect($catsParent->toArray())->mapWithKeys(function ($item) {
                return [$item['prcat_id'] => $item['name']];
            });
        }

        $catsLevel1 = $this->mainModel->findsByLevel(1);

        return view('be.product-category.index', [
            'cats' => $cats,
            'parentItems' => $parentItems,
            'catsLevel1' => $catsLevel1,
            'catStatus' => $this->mainModel::STATUS,
            'catType' => $this->mainModel::TYPE,
            'catDisplaySidebar' => $this->mainModel::DISPLAY_SIDEBAR,
            'catDisplayHeaderMenu' => $this->mainModel::DISPLAY_HEADER_MENU,
        ]);
    }

    public function create(): View|Factory|Application
    {
        $cats = $this->mainModel->getAllItems();
        $tags = $this->productTagModel::query()->orderBy('left', 'asc')->get();

        return \view('be.product-category.create', [
            'tags' => $tags,
            'cats' => $cats,
            'catStatus' => $this->mainModel::STATUS,
            'catType' => $this->mainModel::TYPE,
            'catDisplaySidebar' => $this->mainModel::DISPLAY_SIDEBAR,
            'catDisplayHeaderMenu' => $this->mainModel::DISPLAY_HEADER_MENU,
        ]);
    }

    /**
     * @throws Exception
     */
    public function store(ProductCategoryRequest $productCategoryRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($productCategoryRequest->post());
        // upload thumbnail
        $thumbnails = $this->mainModel->uploadThumbnail('prod_cat_thumbnail');
        if ($thumbnails['status'] === true) {
            $data['thumbnail'] = $thumbnails['filename'];
        }

        $this->mainModel->createNode($data, $data['position']);

        return redirect()->route('be.productcategory.index')->with('success', config('message.store_successful'));
    }

    public function show($id): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $id = \request()->get('id', 0);
        if (! $cat = $this->mainModel::query()->find($id)) {
            throw new NoResultFoundException();
        }

        $cats = $this->mainModel->getAllItems($id);
        $tags = $this->productTagModel::query()->orderBy('left', 'asc')->get();

        $catAncestor = $this->mainModel::query()->orderBy('left', 'asc')->breadcrumb($cat->left, $cat->right)->first();
        $catBranchs = $this->mainModel::query()->branch($catAncestor->left, $catAncestor->right)->orderBy('left', 'asc')->get();

        return \view('be.product-category.edit', [
            'catBranchs' => $catBranchs,
            'tags' => $tags,
            'cat' => $cat,
            'cats' => $cats,
            'catStatus' => $this->mainModel::STATUS,
            'catType' => $this->mainModel::TYPE,
            'catDisplaySidebar' => $this->mainModel::DISPLAY_SIDEBAR,
            'catDisplayHeaderMenu' => $this->mainModel::DISPLAY_HEADER_MENU,
        ]);
    }

    /**
     * @throws Exception
     */
    public function update(ProductCategoryRequest $productCategoryRequest): RedirectResponse
    {
        $catId = \request()->post('prod_cat_id');
        $cat = $this->mainModel::query()->find($catId);
        $data = $this->mainModel->getDataFields($productCategoryRequest->post());
        $positionNode = \request()->post('prod_cat_position', null);
        $this->mainModel->updateNode($data, $catId, $positionNode);

        // upload thumbnail
        $this->mainModel->uploadAndSaveThumbnail('prod_cat_thumbnail', $cat);

        if (\request()->post('prod_cat_status') == 'activated') {
            // Thực hiện update dữ liệu (Danh sách node tổ tiên và bản thân node đó)
            $this->mainModel::parentQuery()->breadcrumb($cat->left, $cat->right)->update(['status' => 'activated']);
        }

        return redirect()->route('be.productcategory.index')->with('success', config('message.update_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function destroy(): RedirectResponse
    {
        $params['id'] = \request()->get('id', 0);
        $this->mainModel->deleteNode($params);

        return redirect()->route('be.productcategory.index')->with('success', config('message.destroy_successful'));
    }
}
