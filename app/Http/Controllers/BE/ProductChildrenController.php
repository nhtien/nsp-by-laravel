<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductChildrenRequest;
use App\Models\Product;
use App\Models\ProductChildren;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductChildrenController extends Controller
{
    public ProductChildren $mainModel;

    public function __construct(ProductChildren $mainModel, public Product $productModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NoResultFoundException
     * @throws NotFoundExceptionInterface
     */
    public function create(): View|Factory|Application
    {
        $prodId = \request()->get('prod_id', 0);
        if (! $product = $this->productModel::parentQuery()->select(['prod_id', 'part_number', 'name'])->find($prodId)) {
            throw new NoResultFoundException();
        }

        return \view('be.product-children.create', [
            'product' => $product,
            'childrenStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(ProductChildrenRequest $productChildrenRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields(($productChildrenRequest->post()));
        $this->mainModel::parentQuery()->create($data);

        return redirect()->route('be.product.edit', ['id' => request()->post('prod_id')])->with('success', config('message.store_successful'));
    }

    public function show($id)
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $childId = \request()->get('prod_child_id', 0);
        $prodId = \request()->get('prod_id', 0);
        if (! $children = $this->mainModel::query()->prodId($prodId)->find($childId)) {
            throw new NoResultFoundException();
        }
        if (! $product = $children->product()->select(['prod_id', 'part_number', 'name'])->first()) {
            throw new NoResultFoundException();
        }

        return \view('be.product-children.edit', [
            'children' => $children,
            'product' => $product,
            'childrenStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(ProductChildrenRequest $productChildrenRequest): RedirectResponse
    {
        $children = $this->mainModel::query()->find(\request()->post('prod_child_id'));
        $data = $this->mainModel->getDataFields($productChildrenRequest->post(), ['prod_id']);
        $children->update($data);

        return redirect()->route('be.product.edit', ['id' => $children->prod_id])->with('success', \config('message.update_successful'));
    }

    /**
     * @throws NoResultFoundException
     */
    public function destroy(): RedirectResponse
    {
        $childrenId = \request()->post('id', 0);
        if (! $children = $this->mainModel::query()->byHashId($childrenId)->first()) {
            throw new NoResultFoundException();
        }
        $prodId = $children->prod_id;
        if ($prodId < 1) {
            throw new NoResultFoundException();
        }
        $children->delete();

        return redirect()->route('be.product.edit', ['id' => $prodId])->with('success', 'Xóa thông tin đặt hàng thành công');
    }
}
