<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductChildren;
use App\Models\ProductDatasheet;
use App\Models\ProductFile;
use App\Models\ProductProperty;
use App\Models\ProductTag;
use App\Models\Solution;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Throwable;

class ProductController extends Controller
{
    public ProductTag $productTagModel;

    public ProductBrand $productBrandModel;

    public Product $mainModel;

    public ProductCategory $productCategoryModel;

    public Solution $solutionModel;

    public ProductProperty $productPropertyModel;

    public ProductDatasheet $productDatasheetModel;

    public ProductChildren $productChildrenModel;

    public ProductFile $productFileModel;

    public function __construct(
        ProductCategory $productCategoryModel,
        ProductTag $productTagModel,
        Product $mainModel,
        ProductBrand $productBrandModel,
        Solution $solutionModel,
        ProductProperty $productPropertyModel,
        ProductDatasheet $productDatasheetModel,
        ProductChildren $productChildrenModel,
        ProductFile $productFileModel,
    ) {
        parent::__construct();
        $this->productCategoryModel = $productCategoryModel;
        $this->productTagModel = $productTagModel;
        $this->mainModel = $mainModel;
        $this->productBrandModel = $productBrandModel;
        $this->solutionModel = $solutionModel;
        $this->productPropertyModel = $productPropertyModel;
        $this->productDatasheetModel = $productDatasheetModel;
        $this->productChildrenModel = $productChildrenModel;
        $this->productFileModel = $productFileModel;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): View|Factory|Application
    {
        $products = $this->mainModel->searching();
        $brands = $this->productBrandModel::query()->get();
        $cats = $this->productCategoryModel::query()->orderBy('left', 'asc')->get();

        return view('be.product.index', [
            'products' => $products,
            'cats' => $cats,
            'brands' => $brands,
            'prodStatus' => $this->mainModel::STATUS,
            'prodPartNumberType' => $this->mainModel::PART_NUMBER_DISPLAY_TYPE,
            'prodType' => $this->mainModel::TYPE,
        ]);
    }

    public function create(): View|Factory|Application
    {
        $brands = $this->productBrandModel::query()->get();
        $solutions = $this->solutionModel::query()->get();
        $files = $this->productFileModel::query()->get();
        $cats = $this->productCategoryModel->getAllItems();
        $tags = $this->productTagModel::query()->orderBy('left', 'asc')->get();

        return view('be.product.create', [
            'tags' => $tags,
            'solutions' => $solutions,
            'files' => $files,
            'cats' => $cats,
            'brands' => $brands,
            'prodStatus' => $this->mainModel::STATUS,
            'prodPartNumberType' => $this->mainModel::PART_NUMBER_DISPLAY_TYPE,
            'prodType' => $this->mainModel::TYPE,
        ]);
    }

    public function store(ProductRequest $productRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($productRequest->post(), ['views', 'thumbnail']);
        $data['user_id'] = auth()->user()->user_id;
        $product = $this->mainModel::query()->create($data);

        // upload thumbnail
        $this->mainModel->uploadAndSaveThumbnail('prod_thumbnail', $product, false);

        return redirect()->route('be.product.edit', ['id' => $product->prod_id])->with('success', \config('message.store_successful'));
    }

    public function show($id): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $id = \request()->get('id', 0);
        $product = $this->mainModel::query()->find($id);
        throw_if(! $product, new NoResultFoundException());

        $brands = $this->productBrandModel::query()->get();
        $cats = $this->productCategoryModel->getAllItems();
        $tags = $this->productTagModel::query()->orderBy('left', 'asc')->get();
        $datasheets = $product->productDatasheets()->orderBy('rank', 'asc')->get();
        $childrens = $product->productChildrens()->orderBy('rank', 'asc')->get();
        $properties = $product->productProperties()->orderBy('rank', 'asc')->get();
        $files = $this->productFileModel::query()->get();
        $datasheetGroups = $product->productDatasheetGroups->unique();
        $solutions = $this->solutionModel::query()->get();

        return view('be.product.edit', [
            'product' => $product,
            'solutions' => $solutions,
            'datasheetGroups' => $datasheetGroups,
            'datasheets' => $datasheets,
            'properties' => $properties,
            'childrens' => $childrens,
            'tags' => $tags,
            'cats' => $cats,
            'brands' => $brands,
            'prodStatus' => $this->mainModel::STATUS,
            'prodPartNumberType' => $this->mainModel::PART_NUMBER_DISPLAY_TYPE,
            'prodType' => $this->mainModel::TYPE,
            'files' => $files,
        ]);
    }

    public function update(ProductRequest $productRequest): RedirectResponse
    {
        $product = $this->mainModel::query()->find(\request()->post('prod_id'));
        $data = $this->mainModel->getDataFields($productRequest->post(), ['views', 'user_id', 'thumbnail']);

        if (!isset($data['product_file_ids'])) {
            $data['product_file_ids'] = [];
        }

        if (!isset($data['product_tag_ids'])) {
            $data['product_tag_ids'] = [];
        }

        if (!isset($data['solution_ids'])) {
            $data['solution_ids'] = [];
        }

        $product->update($data);
        // upload thumbnail
        $this->mainModel->uploadAndSaveThumbnail('prod_thumbnail', $product);

        return redirect()->back()->with('success', config('message.update_successful'));
        //return redirect()->route('be.product.index')->with('success', config('message.update_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws Throwable
     */
    public function destroy(): RedirectResponse
    {
        $prodId = \request()->get('id', 0);
        tap($this->mainModel::parentQuery()->prodId($prodId)->first(), function ($product) {
            throw_if(! $product, new NoResultFoundException());
            $product->delete();
        });

        return redirect()->route('be.product.index')->with('success', config('message.destroy_successful'));
    }

    public function clone($id)
    {
        /**
         * @var $product Product
         */
        if (! $product = $this->mainModel::parentQuery()->prodId($id)->first()) {
            throw_if(! $product, new NoResultFoundException());
        }

        $partNumber = $product->part_number.'-CLONE';
        $folderNameByPartNumber = create_folder_name($partNumber);
        $productArr = $product->getRawOriginal();
        unset($productArr['prod_id']);
        unset($productArr['created_at']);
        unset($productArr['updated_at']);
        $productArr['part_number'] = $partNumber;
        $productArr['status'] = 'inactive';
        $productArr['views'] = 0;
        $productArr['thumbnail'] = '';
        $newProduct = $this->mainModel::query()->create($productArr);

        // copy image related
        if (is_dir($product->path_image_related)) {
            File::copyDirectory($product->path_image_related, dirname($product->path_image_related).'/'.$folderNameByPartNumber);
        }

        // copy datasheet
        if (is_dir($product->path_datasheet)) {
            File::copyDirectory($product->path_datasheet, dirname($product->path_datasheet).'/'.$folderNameByPartNumber);
        }

        // copy thumbnail
        if (is_file($product->path_thumbnail)) {
            $extension = pathinfo($product->path_thumbnail)['extension'];
            $newThumbnail = Str::slug($newProduct->name).'_product'.$newProduct->prod_id.".{$extension}";
            copy($product->path_thumbnail, dirname($product->path_thumbnail)."/{$newThumbnail}");
            $newProduct->thumbnail = $newThumbnail;
            $newProduct->save();
        }

        // copy product_property
        $productProperties = $this->productPropertyModel::query()->where('prod_id', $id)->get();
        foreach ($productProperties as $productProperty) {
            $propertyArr = $productProperty->toArray();
            unset($productArr['prppt_id']);
            $propertyArr['prod_id'] = $newProduct->prod_id;
            $this->productPropertyModel::query()->create($propertyArr);
        }

        // copy product_datasheet
        $productDatasheets = $this->productDatasheetModel::query()->where('prod_id', $id)->get();
        foreach ($productDatasheets as $productDatasheet) {
            $datasheetArr = $productDatasheet->toArray();
            unset($datasheetArr['prda_id']);
            $datasheetArr['prod_id'] = $newProduct->prod_id;
            $this->productDatasheetModel::query()->create($datasheetArr);
        }

        // copy product_children
        $productChilds = $this->productChildrenModel::query()->where('prod_id', $id)->get();
        foreach ($productChilds as $productChild) {
            $childrenArr = $productChild->toArray();
            unset($childrenArr['prchl_id']);
            $childrenArr['prod_id'] = $newProduct->prod_id;
            $this->productChildrenModel::query()->create($childrenArr);
        }

        return redirect()->route('be.product.index')->with('success', 'Clone sản phẩm thành công');
    }
}
