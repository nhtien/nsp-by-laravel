<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductDatasheetRequest;
use App\Models\Product as ProductModel;
use App\Models\ProductDatasheet;
use App\Models\ProductDatasheetGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductDatasheetController extends Controller
{
    public ProductDatasheet $mainModel;

    public ProductModel $productModel;

    public ProductDatasheetGroup $productDatasheetGroupModel;

    public function __construct(ProductDatasheet $mainModel, ProductModel $productModel, ProductDatasheetGroup $productDatasheetGroupModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
        $this->productDatasheetGroupModel = $productDatasheetGroupModel;
        $this->productModel = $productModel;
    }

    /**
     * @return Application|Factory|View
     *
     * @throws ContainerExceptionInterface
     * @throws NoResultFoundException
     * @throws NotFoundExceptionInterface
     */
    public function create()
    {
        $prodId = \request()->get('prod_id', 0);
        if (! $product = $this->productModel::parentQuery()->select(['prod_id', 'part_number', 'name'])->find($prodId)) {
            throw new NoResultFoundException();
        }

        $datasheetGroups = $this->productDatasheetGroupModel::all();

        return view('be.product-datasheet.create', [
            'datasheetGroups' => $datasheetGroups,
            'product' => $product,
            'datasheetStatus' => $this->mainModel::STATUS,
            'datasheetType' => $this->mainModel::TYPE,
        ]);
    }

    /**
     * @return RedirectResponse
     */
    public function store(ProductDatasheetRequest $productDatasheetRequest)
    {
        $data = $this->mainModel->getDataFields($productDatasheetRequest->post());
        $this->mainModel::query()->create($data);

        return redirect()->route('be.product.edit', ['id' => request()->post('prod_id')])->with('success', \config('message.store_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $sheetId = \request()->get('prod_sht_id', 0);
        $prodId = \request()->get('prod_id', 0);
        if (! $datasheet = $this->mainModel::query()->prodId($prodId)->find($sheetId)) {
            throw new NoResultFoundException();
        }

        if (! $product = $datasheet->product()->select(['prod_id', 'part_number', 'name'])->first()) {
            throw new NoResultFoundException();
        }

        $datasheetGroups = $this->productDatasheetGroupModel::all();

        return \view('be.product-datasheet.edit', [
            'datasheet' => $datasheet,
            'datasheetGroups' => $datasheetGroups,
            'product' => $product,
            'datasheetStatus' => $this->mainModel::STATUS,
            'datasheetType' => $this->mainModel::TYPE,
        ]);
    }

    public function update(ProductDatasheetRequest $productDatasheetRequest): RedirectResponse
    {
        $datasheet = $this->mainModel::query()->find(\request()->post('prod_sht_id'));
        $data = $this->mainModel->getDataFields($productDatasheetRequest->post(), ['prod_id']);
        $datasheet->update($data);

        return redirect()->route('be.product.edit', ['id' => $datasheet->prod_id])->with('success', \config('message.update_successful'));
    }

    /**
     * @throws NoResultFoundException
     */
    public function destroy(): RedirectResponse
    {
        $datasheetId = \request()->post('id', 0);
        if (! $datasheet = $this->mainModel::query()->byHashId($datasheetId)->first()) {
            throw new NoResultFoundException();
        }
        $prodId = $datasheet->prod_id;
        if ($prodId < 1) {
            throw new NoResultFoundException();
        }
        $datasheet->delete();

        return redirect()->route('be.product.edit', ['id' => $prodId])->with('success', 'Xóa datasheet thành công');
    }
}
