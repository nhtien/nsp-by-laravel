<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductDatasheetGroupRequest;
use App\Models\ProductDatasheetGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductDatasheetGroupController extends Controller
{
    public ProductDatasheetGroup $mainModel;

    public function __construct(ProductDatasheetGroup $mainModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
    }

    public function index(): Factory|View|Application
    {
        $datasheetGroups = $this->mainModel::query()->paginate();

        return view('be.product-datasheet-group.index', [
            'datasheetGroups' => $datasheetGroups,
            'grpStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.product-datasheet-group.create', [
            'grpStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(ProductDatasheetGroupRequest $productDatasheetGroupRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($productDatasheetGroupRequest->post());
        $this->mainModel::query()->create($data);

        return redirect()->route('be.productdatasheetgroup.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $datasheetGroup = $this->mainModel::query()->find($id)) {
            throw new NoResultFoundException();
        }

        return \view('be.product-datasheet-group.edit', [
            'datasheetGroup' => $datasheetGroup,
            'datasheetStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(ProductDatasheetGroupRequest $productDatasheetGroupRequest): RedirectResponse
    {
        $sliderGroup = $this->mainModel::query()->find(\request()->post('prod_sht_grp_id'));
        $data = $this->mainModel->getDataFields($productDatasheetGroupRequest->post());
        $sliderGroup->update($data);

        return redirect()->route('be.productdatasheetgroup.index')->with('success', \config('message.update_successful'));
    }

    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->mainModel::query()->find($id)->delete();

        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
