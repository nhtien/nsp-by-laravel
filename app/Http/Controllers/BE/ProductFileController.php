<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductFileRequest;
use App\Models\Product as ProductModel;
use App\Models\ProductFile;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductFileController extends Controller
{
    public ProductFile $mainModel;

    public ProductModel $productModel;

    public function index()
    {
        $files = $this->mainModel->searching();

        return view('be.product-file.index', [
            'files' => $files,
            'fileStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function __construct(ProductFile $mainModel, ProductModel $productModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
        $this->productModel = $productModel;
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function create()
    {
        return view('be.product-file.create', [
            'fileStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(ProductFileRequest $productFileRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($productFileRequest->post());
        $filename = create_filename('prod_file_name');
        upload_file('prod_file_name', base_path($this->mainModel::FILE_PATH), $filename);
        $data['file_name'] = $filename;
        $this->mainModel::query()->create($data);

        return redirect()->route('be.productfile.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $fileId = \request()->get('id', 0);
        if (! $file = $this->mainModel::query()->find($fileId)) {
            throw new NoResultFoundException();
        }

        return \view('be.product-file.edit', [
            'file' => $file,
            'fileStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(ProductFileRequest $productFileRequest): RedirectResponse
    {
        $file = $this->mainModel::query()->find(\request()->post('prod_file_id'));
        $data = $this->mainModel->getDataFields($productFileRequest->post());
        if (\request()->file('prod_file_name')) {
            $filename = create_filename('prod_file_name');
            upload_file('prod_file_name', base_path($this->mainModel::FILE_PATH), $filename);
            delete_file(base_path($this->mainModel::FILE_PATH)."/{$file->file_name}");
            $data['file_name'] = $filename;
        }
        $file->update($data);

        return redirect()->route('be.productfile.index')->with('success', \config('message.update_successful'));
    }

    /**
     * @throws NoResultFoundException
     */
    public function destroy(): RedirectResponse
    {
        $fileId = \request()->post('id', 0);
        if (! $file = $this->mainModel::query()->find($fileId)) {
            throw new NoResultFoundException();
        }

        if ($file->delete()) {
            delete_file(base_path($this->mainModel::FILE_PATH)."/{$file->file_name}");
        }

        return redirect()->route('be.productfile.index')->with('success', 'Xóa thành công');
    }
}
