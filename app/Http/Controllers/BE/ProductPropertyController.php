<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductPropertyRequest;
use App\Models\Product;
use App\Models\ProductProperty;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductPropertyController extends Controller
{
    public function __construct(
        public ProductProperty $mainModel,
        public Product $productModel
    ) {
        parent::__construct();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NoResultFoundException
     * @throws NotFoundExceptionInterface
     */
    public function create(): View|Factory|Application
    {
        $prodId = \request()->get('prod_id', 0);
        if (! $product = $this->productModel::parentQuery()->select(['prod_id', 'part_number', 'name'])->find($prodId)) {
            throw new NoResultFoundException();
        }

        return \view('be.product-property.create', [
            'product' => $product,
            'icons' => $this->mainModel->icons,
            'propertyStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(ProductPropertyRequest $productPropertyRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields(($productPropertyRequest->post()));
        $this->mainModel::parentQuery()->create($data);

        return redirect()
            ->route('be.product.edit', ['id' => request()->post('prod_id')])
            ->with('success', config('message.store_successful'));
    }

    public function show($id)
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $propertyId = \request()->get('prod_pro_id', 0);
        $prodId = \request()->get('prod_id', 0);
        if (! $property = $this->mainModel::query()->prodId($prodId)->find($propertyId)) {
            throw new NoResultFoundException();
        }
        if (! $product = $property->product()->select(['prod_id', 'part_number', 'name'])->first()) {
            throw new NoResultFoundException();
        }

        return \view('be.product-property.edit', [
            'property' => $property,
            'product' => $product,
            'propertyStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(ProductPropertyRequest $productPropertyRequest): RedirectResponse
    {
        $property = $this->mainModel::query()->find(\request()->post('prod_pro_id'));
        $data = $this->mainModel->getDataFields($productPropertyRequest->post(), ['prod_id']);
        $property->update($data);

        return redirect()->route('be.product.edit', ['id' => $property->prod_id])->with('success', \config('message.update_successful'));
    }

    /**
     * @throws NoResultFoundException
     */
    public function destroy(): RedirectResponse
    {
        $propertyId = \request()->post('id', 0);
        if (! $property = $this->mainModel::query()->byHashId($propertyId)->first()) {
            throw new NoResultFoundException();
        }
        $prodId = $property->prod_id;
        if ($prodId < 1) {
            throw new NoResultFoundException();
        }
        $property->delete();

        return redirect()->route('be.product.edit', ['id' => $prodId])->with('success', 'Xóa thuộc tính thành công');
    }
}
