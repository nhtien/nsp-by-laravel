<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\ProductTagRequest;
use App\Models\ProductTag;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProductTagController extends Controller
{
    public ProductTag $mainModel;

    public function __construct(ProductTag $mainModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
    }

    public function index(): View|Factory|Application
    {
        $tags = $this->mainModel->searching();

        return view('be.product-tag.index', [
            'tags' => $tags,
            'tagType' => $this->mainModel::TYPE,
            'tagStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        $tags = $this->mainModel->getAllItems(0);

        return view('be.product-tag.create', [
            'tagStatus' => $this->mainModel::STATUS,
            'tags' => $tags,
            'tagType' => $this->mainModel::TYPE,
        ]);
    }

    /**
     * @throws Exception
     */
    public function store(ProductTagRequest $productTagRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields(($productTagRequest->post()));
        $this->mainModel->createNode($data, $data['position']);

        return redirect()->route('be.producttag.index')->with('success', config('message.store_successful'));
    }

    public function show($id)
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|Application|RedirectResponse
    {
        $id = \request()->get('id', 0);
        if (! $tag = $this->mainModel::query()->find($id)) {
            throw new NoResultFoundException();
        }
        $tags = $this->mainModel->getAllItems($id);
        $tagAncestor = $this->mainModel::query()->orderBy('left', 'asc')->breadcrumb($tag->left, $tag->right)->first();
        $tagBranchs = $this->mainModel::query()->branch($tagAncestor->left, $tagAncestor->right)->orderBy('left', 'asc')->get();

        return \view('be.product-tag.edit', [
            'tag' => $tag,
            'tagBranchs' => $tagBranchs,
            'tags' => $tags,
            'tagStatus' => $this->mainModel::STATUS,
            'tagType' => $this->mainModel::TYPE,
        ]);
    }

    /**
     * @throws Exception
     */
    public function update(ProductTagRequest $productTagRequest): RedirectResponse
    {
        $id = \request()->post('prod_tag_id');
        $tag = $this->mainModel::query()->find($id);
        $data = $this->mainModel->getDataFields(($productTagRequest->post()));

        $positionNode = \request()->post('prod_tag_position');
        $this->mainModel->updateNode($data, $id, $positionNode);

        if (\request()->post('prod_tag_status') == 'activated') {
            // Thực hiện update dữ liệu (Danh sách node tổ tiên và bản thân node đó)
            $this->mainModel::parentQuery()->breadcrumb($tag->left, $tag->right)->update(['status' => 'activated']);
        }

        return redirect()->route('be.producttag.index')->with('success', config('message.update_successful'));
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function destroy(): RedirectResponse
    {
        $params['prtag_id'] = \request()->get('id', 0);
        $this->mainModel->deleteNode($params);

        return redirect()->route('be.producttag.index')->with('success', config('message.destroy_successful'));
    }
}
