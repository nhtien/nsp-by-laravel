<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use App\Models\SliderGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SliderController extends Controller
{
    public Slider $mainModel;

    public SliderGroup $sliderGroupModel;

    public function __construct(Slider $mainModel, SliderGroup $sliderGroupModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
        $this->sliderGroupModel = $sliderGroupModel;
    }

    public function index(): Factory|View|Application
    {
        $sliders = $this->mainModel->searching();

        return view('be.slider.index', [
            'sliders' => $sliders,
            'sldStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        $sliderGroups = $this->sliderGroupModel::parentQuery()->get();

        return view('be.slider.create', [
            'sldStatus' => $this->mainModel::STATUS,
            'sliderGroups' => $sliderGroups,
        ]);
    }

    public function store(SliderRequest $sliderRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($sliderRequest->post());
        $slider = $this->mainModel::parentQuery()->create($data);

        // upload picture
        $this->mainModel->uploadAndSavePicture('sld_picture', $slider, false);

        return redirect()->route('be.slider.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $slider = $this->mainModel::parentQuery()->find($id)) {
            throw new NoResultFoundException();
        }
        $sliderGroups = $this->sliderGroupModel::parentQuery()->get();

        return \view('be.slider.edit', [
            'slider' => $slider,
            'sldStatus' => $this->mainModel::STATUS,
            'sliderGroups' => $sliderGroups,
        ]);
    }

    public function update(SliderRequest $sliderRequest): RedirectResponse
    {
        $slider = $this->mainModel::query()->find(\request()->post('sld_id'));
        $data = $this->mainModel->getDataFields($sliderRequest->post(), ['picture']);
        $slider->update($data);

        // upload picture
        $this->mainModel->uploadAndSavePicture('sld_picture', $slider);

        return redirect()->route('be.slider.index')->with('success', \config('message.update_successful'));
    }

    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->mainModel::query()->find($id)->delete();

        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
