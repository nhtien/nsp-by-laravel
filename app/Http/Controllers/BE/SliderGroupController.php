<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\SliderGroupRequest;
use App\Models\SliderGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SliderGroupController extends Controller
{
    public SliderGroup $mainModel;

    public function __construct(SliderGroup $mainModel)
    {
        parent::__construct();
        $this->mainModel = $mainModel;
    }

    public function index(): Factory|View|Application
    {
        $sliderGroups = $this->mainModel::parentQuery()->paginate();

        return view('be.slider-group.index', [
            'sliderGroups' => $sliderGroups,
            'sldgrpStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.slider-group.create', [
            'sldgrpStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(SliderGroupRequest $sliderGroupRequest): RedirectResponse
    {
        $data = $this->mainModel->getDataFields($sliderGroupRequest->post());
        $this->mainModel::query()->create($data);

        return redirect()->route('be.slidergroup.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $sliderGroup = $this->mainModel::parentQuery()->find($id)) {
            throw new NoResultFoundException();
        }

        return \view('be.slider-group.edit', [
            'sliderGroup' => $sliderGroup,
            'sldgrpStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(SliderGroupRequest $sliderGroupRequest): RedirectResponse
    {
        $sliderGroup = $this->mainModel::query()->find(\request()->post('sld_grp_id'));
        $data = $this->mainModel->getDataFields($sliderGroupRequest->post());
        $sliderGroup->update($data);

        return redirect()->route('be.slidergroup.index')->with('success', \config('message.update_successful'));
    }

    public function destroy(): RedirectResponse
    {
        $id = \request()->post('id', 0);
        $this->mainModel::query()->find($id)->delete();

        return redirect()->back()->with('success', \config('message.destroy_successful'));
    }
}
