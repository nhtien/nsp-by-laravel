<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\SolutionRequest;
use App\Models\Solution;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class SolutionController extends Controller
{
    public function __construct(public Solution $mainModel)
    {
        parent::__construct();
    }

    public function index(): Factory|View|Application
    {
        $solutions = $this->mainModel->searching();

        return view('be.solution.index', [
            'solutions' => $solutions,
            'solutionStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.solution.create', [
            'solutionStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function store(SolutionRequest $solutionRequest): RedirectResponse
    {
        $this->mainModel::query()->create($solutionRequest->all());

        return redirect()->route('be.solution.index')->with('success', \config('message.store_successful'));
    }

    /**
     * @return false
     */
    public function show(): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $solution = $this->mainModel::query()->find($id)) {
            throw new NoResultFoundException();
        }

        return \view('be.solution.edit', [
            'solution' => $solution,
            'solutionStatus' => $this->mainModel::STATUS,
        ]);
    }

    public function update(SolutionRequest $solutionRequest): RedirectResponse
    {
        $solution = $this->mainModel::query()->find(\request()->post('id'));
        $solution->update($solutionRequest->all());

        return redirect()->route('be.solution.index')->with('success', \config('message.update_successful'));
    }
}
