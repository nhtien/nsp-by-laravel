<?php

namespace App\Http\Controllers\BE;

use App\Jobs\TestSendMail;

class TestingController extends Controller
{
    public function sendMail()
    {
        dispatch(new TestSendMail())->delay(now()->addSeconds(30));

        return 'test send email';
    }
}
