<?php

namespace App\Http\Controllers\BE;

use App\Exceptions\NoResultFoundException;
use App\Http\Requests\UserGroupRequest;
use App\Models\Menu;
use App\Models\UserGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class UserGroupController extends Controller
{
    public UserGroup $userGroupModel;

    public Menu $menuModel;

    public function __construct(UserGroup $userGroupModel, Menu $menuModel)
    {
        $this->userGroupModel = $userGroupModel;
        $this->menuModel = $menuModel;
    }

    public function index(): View|Factory|Application
    {
        $userGroups = $this->userGroupModel::query()->paginate();

        return view('be.user-group.index', ['userGroups' => $userGroups]);
    }

    public function create(): View|Factory|Application
    {
        return view('be.user-group.create', [
            'groupStatus' => $this->userGroupModel::STATUS,
        ]);
    }

    public function store(UserGroupRequest $userGroupRequest): RedirectResponse
    {
        $data = $this->userGroupModel->getDataFields($userGroupRequest->post());
        $this->userGroupModel::parentQuery()->create($data);

        return redirect()->route('be.usergroup.index')->with('success', \config('message.store_successful'));
    }

    public function show($id): bool
    {
        return false;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @throws NoResultFoundException
     */
    public function edit(): View|Factory|RedirectResponse|Application
    {
        $id = \request()->get('id', 0);
        if (! $userGroup = $this->userGroupModel::query()->find($id)) {
            throw new NoResultFoundException();
        }
        $parentMenus = $this->menuModel::parentQuery()->isParent()->get();
        $childMenus = $this->menuModel::parentQuery()->notParent()->get();
        $childMenuArr = collect($childMenus)->groupBy('parent')->toArray();
        $menuOfUserGroupArr = $userGroup->menu_ids;

        return \view('be.user-group.edit', [
            'userGroup' => $userGroup,
            'groupStatus' => $this->userGroupModel::STATUS,
            'parentMenus' => $parentMenus,
            'childMenuArr' => $childMenuArr,
            'menuOfUserGroupArr' => $menuOfUserGroupArr,
        ]);
    }

    public function update(UserGroupRequest $userGroupRequest): RedirectResponse
    {
        $userGroup = $this->userGroupModel::query()->find(\request()->post('ugrp_id'));
        $data = $this->userGroupModel->getDataFields($userGroupRequest->post());
        $userGroup->update($data);

        return redirect()->route('be.usergroup.index')->with('success', \config('message.update_successful'));
    }

    public function destroy($id)
    {
    }
}
