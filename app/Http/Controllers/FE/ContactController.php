<?php

namespace App\Http\Controllers\FE;

use App\Models\Contact;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __construct(public Contact $contactModel)
    {
        parent::__construct();
    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index()
    {
        return \view('fe.contact.index');
    }

    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'full_name' => 'required|min:6|max:255|do_not_contain_html_tag',
            'company_name' => 'nullable|min:3|max:255|do_not_contain_html_tag',
            'company_address' => 'nullable|min:6|max:255|do_not_contain_html_tag',
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',
            'email' => 'required|email',
            'subject' => 'required|min:6|max:255|do_not_contain_html_tag',
            'message' => 'required|min:6|max:1000|do_not_contain_html_tag',
        ],
            [
                'full_name.required' => 'Họ và Tên là bắt buộc',
                'full_name.min' => 'Tối thiểu 6 kí tự',
                'full_name.max' => 'Tối đa 255 kí tự',
                'full_name.do_not_contain_html_tag' => 'Không được chứa kí tự đặc biệt',
                'company_name.min' => 'Tối thiểu 6 kí tự',
                'company_name.max' => 'Tối đa 255 kí tự',
                'company_name.do_not_contain_html_tag' => 'Không được chứa kí tự đặc biệt',
                'company_address.min' => 'Tối thiểu 6 kí tự',
                'company_address.max' => 'Tối đa 255 kí tự',
                'company_address.do_not_contain_html_tag' => 'Không được chứa kí tự đặc biệt',
                'phone_number.required' => 'Số điện thoại là bắt buộc',
                'phone_number.regex' => 'Định dạng không hợp lệ',
                'subject.required' => 'Chủ đề là bắt buộc',
                'subject.min' => 'Tối thiểu 6 kí tự',
                'subject.max' => 'Tối đa 255 kí tự',
                'subject.do_not_contain_html_tag' => 'Không được chứa kí tự đặc biệt',
                'message.required' => 'Nội dung là bắt buộc',
                'message.min' => 'Tối thiểu 6 kí tự',
                'message.max' => 'Tối đa 1000 kí tự',
                'message.do_not_contain_html_tag' => 'Không được chứa kí tự đặc biệt',
            ]
        );

        $this->contactModel::query()->create($request->all());

        return redirect()->route('fe.contact.index')->with('success', 'Tin nhắn đã được gửi thành công');
    }
}
