<?php

namespace App\Http\Controllers\FE;

use App\Models\Post;
use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\Slider;
use App\Models\Solution;
use App\Models\TwentyYear;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class IndexController extends Controller
{
    public function __construct(
        public Slider $sliderModel,
        public TwentyYear $twentyYearModel,
        public ProductCategory $productCategoryModel,
        public Post $postModel,
        public ProductBrand $productBrandModel,
        public Solution $solutionModel
    ) {
        parent::__construct();
    }

    public function index(){
        return redirect('trang-chu.html');
    }

    public function homepage(): View|Factory|Application
    {
        $sliders = $this->sliderModel::parentQuery()
            ->where('status', 'activated')
            ->orderBy('position', 'asc')
            ->get();

        $twentyYears = $this->twentyYearModel::query()
            ->where('enable', 1)
            ->orderBy('ordering', 'asc')
            ->get();

        $productCategories = $this->productCategoryModel::query()
            ->where('status', 'activated')
            ->where('level', 1)
            ->orderBy('left', 'ASC')
            ->get();

        $promotion = $this->postModel::query()
            ->where('pgrp_id', 2)
            ->where('status', 'activated')
            ->orderBy('post_id', 'DESC')->first();

        $productBrands = $this->productBrandModel::query()->where('status', 'activated')->get();

        $posts = $this->postModel::query()
            ->whereIn('pgrp_id', [1, 4])
            ->limit(10)
            ->where('status', 'activated')
            ->orderBy('post_id', 'DESC')->get();

        $solutions = $this->solutionModel::query()->where('status', 'activated')->get();

        return \view('fe.index.index', [
            'sliders' => $sliders,
            'twentyYears' => $twentyYears,
            'productCategories' => $productCategories,
            'promotion' => $promotion,
            'productBrands' => $productBrands,
            'posts' => $posts,
            'solutions' => $solutions,
        ]);
    }

    public function aboutUs()
    {
        return \view('fe.index.about-us');
    }
}
