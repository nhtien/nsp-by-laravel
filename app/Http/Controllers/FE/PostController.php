<?php

namespace App\Http\Controllers\FE;

use App\Models\Post;
use App\Models\PostGroup;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class PostController extends Controller
{
    public function __construct(
        public Post $postModel,
        public PostGroup $postGroupModel
    ) {
        parent::__construct();
    }

    /**
     * @return Application|Factory|View
     */
    public function findsNews()
    {
        $category = $this->postGroupModel::query()->where('code', 'news')->first();
        if (! $category) {
            abort(404);
        }

        return \view('fe.post.news', [
            'cat' => $category,
            'title' => 'Tin tức',
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function findsKnowledge()
    {
        $category = $this->postGroupModel::query()->where('code', 'knowledge')->first();
        if (! $category) {
            abort(404);
        }

        return \view('fe.post.news', [
            'cat' => $category,
            'title' => 'Kiến thức',
        ]);
    }

    public function show($slug, $id): View|Factory|RedirectResponse|Application
    {
        $post = $this->postModel::parentQuery()->select(['post.*', 'post_group.name as cat_name', 'post_group.code as cat_code'])
            ->join('post_group', 'post_group.pgrp_id', 'post.pgrp_id')
            ->where('post.status', 'activated')
            ->find($id);

        if (! $post) {
            abort(404);
        }
        $urlCat = ($post->cat_code === 'news') ? route('fe.post.finds-news') : route('fe.post.knowledge');

        return view('fe.post.show', [
            'post' => $post,
            'urlCat' => $urlCat,
        ]);
    }
}
