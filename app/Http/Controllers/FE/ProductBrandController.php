<?php

namespace App\Http\Controllers\FE;

use App\Models\Product;
use App\Models\ProductBrand;

class ProductBrandController extends Controller
{
    public function __construct(
        public ProductBrand $productBrandModel,
        public Product $productModel
    ) {
        parent::__construct();
    }

    public function index()
    {
        $brands = $this->productBrandModel::query()->where('status', 'activated')->get();

        return view('fe.product-brand.index', ['brands' => $brands]);
    }

    public function findProducts($slug, $id)
    {
        $brand = $this->productBrandModel::query()->where('status', 'activated')->find($id);
        if (! $brand) {
            abort(404);
        }

        return view('fe.product-brand.find-products', ['brand' => $brand]);
    }

    public function detail($slug, $id)
    {
        $brand = $this->productBrandModel::query()->where('status', 'activated')->find($id);
        if (! $brand) {
            abort(404);
        }

        $products = $this->productModel::query()
            ->select(['products.*', 'product_brands.name as brand_name'])
            ->join('product_brands', 'product_brands.prodbrd_id', '=', 'products.prodbrd_id')
            ->where('products.prodbrd_id', $id)
            ->orderBy('prod_id', 'asc')
            ->limit(6)->get();

        return view('fe.product-brand.detail', [
            'brand' => $brand,
            'products' => $products,
        ]);
    }
}
