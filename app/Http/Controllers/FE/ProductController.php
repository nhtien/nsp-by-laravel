<?php

namespace App\Http\Controllers\FE;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductDatasheet;
use App\Models\ProductTag;

class ProductController extends Controller
{
    public function __construct(
        public Product $productModel,
        public ProductCategory $productCategoryModel,
        public ProductDatasheet $productDatasheetModel,
        public ProductTag $productTagModel
    ) {
        parent::__construct();
    }

    public function index()
    {
        return redirect('/');
    }

    public function getProductsByCategory($slug, $id)
    {
        $category = $this->productCategoryModel::query()->find($id);
        if (! $category) {
            abort(404);
        }

        $products = $this->productModel::query()->where('prcat_id', $id)->paginate();

        return \view('fe.product.product-by-cate', [
            'products' => $products,
            'category' => $category,
        ]);
    }

    public function detail($slug, $id)
    {
        $product = $this->productModel::query()->where('status', 'activated')->find($id);
        if (! $product) {
            abort(404);
        }

        return view('fe.product.detail');
    }

    public function test(): string
    {
        $products = $this->productModel::all();
        foreach ($products as $product) {
            dump($product->images_related);
        }

        return '';
    }

    public function findsProductCategory()
    {
        $catLevel1 = $this->productCategoryModel->findsByLevel(1, 'activated');
        $catLevel2 = $this->productCategoryModel->findsByLevel(2, 'activated');

        return view('fe.product.product-category', [
            'catLevel1' => $catLevel1,
            'catLevel2' => $catLevel2,
        ]);
    }

    public function findsProductByTag($slug, $hashId)
    {
        $tag = $this->productTagModel::query()->byHashId($hashId)->first();
        if (! $tag) {
            abort(404);
        }

        return view('fe.product.product-by-tag', [
            'tag' => $tag,
        ]);
    }

    public function document()
    {
        $search = request()->get('search');
        $datasheets = $this->productDatasheetModel::query()
            ->select(['product_datasheet.name as datasheet_name', 'product_datasheet.prda_id', 'product_datasheet.link', 'product_datasheet.file', 'products.part_number', 'product_brands.name as brand_name'])
            ->join('products', 'products.prod_id', 'product_datasheet.prod_id')
            ->join('product_brands', 'products.prodbrd_id', 'product_brands.prodbrd_id')
            ->where('product_datasheet.status', 'activated')
            ->orderBy('prda_id', 'desc')
            ->when(! empty($search), function ($q) use ($search) {
                $q->where(function ($q) use ($search) {
                    $q->orWhere('products.part_number', $search)
                        ->orWhere('product_datasheet.name', 'like', "%{$search}%");
                });
            })
            ->paginate(10);

        return view('fe.product.document', [
            'datasheets' => $datasheets,
            'search' => $search,
        ]);
    }
}
