<?php

namespace App\Http\Controllers\FE;

use App\Enums\Product;
use App\Models\ProductDatasheet;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ProductDatasheetController extends Controller
{
    public function __construct(
        public ProductDatasheet $productDatasheetModel
    ) {
        parent::__construct();
    }

    public function downloadDatasheet($hashId): BinaryFileResponse
    {
        $datasheet = $this->productDatasheetModel::query()
            ->select(['part_number', 'file'])
            ->join('products', 'products.prod_id', 'product_datasheet.prod_id')
            ->where('product_datasheet.status', 'activated')
            ->byHashId($hashId)
            ->first();

        if (! $datasheet) {
            abort(404);
        }

        $pathToFile = Product::DATASHEET_PATH->path().create_folder_name($datasheet->part_number).'/'.$datasheet->file;

        return response()->file($pathToFile);
    }
}
