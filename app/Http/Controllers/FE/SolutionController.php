<?php

namespace App\Http\Controllers\FE;

use App\Models\Solution;

class SolutionController extends Controller
{
    public function __construct(
        public Solution $solutionModel
    ) {
        parent::__construct();
    }

    public function detail($slug, $id)
    {
        $solution = $this->solutionModel::query()->where('status', 'activated')->find($id);
        if (! $solution) {
            abort(404);
        }

        return view('fe.solution.detail', ['solution' => $solution]);
    }

    public function index()
    {
        $solutions = $this->solutionModel::query()->where('status', 'activated')->get();

        return view('fe.solution.index', ['solutions' => $solutions]);
    }
}
