<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     */
    protected function redirectTo($request): ?string
    {
        if (! $request->expectsJson()) {
            return route('login');
        }
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  string[]  ...$guards
     *
     * @throws AuthenticationException
     */
    public function handle($request, Closure $next, ...$guards): mixed
    {
        $this->authenticate($request, $guards);

        /**
         * Kiểm tra quyền truy cập của người dùng
         */
        if ($this->checkAccess() === false) {
            // logout khi status là inactive
            if (\auth()->user()->status !== 'activated') {
                return redirect()->route('logout');
            }

            return abort(403);
        }

        return $next($request);
    }

    private function checkAccess(): bool
    {
        $auth = Auth::guard();
        if ($auth->check()) {
            $user = $auth->user();
            if (optional($user)->status != 'activated') {
                return false;
            } else {
                if (is_root_account() === false) {
                    if (check_access_current_action() === false) {
                        return false;
                    }
                }

                return true;
            }
        }

        return false;
    }
}
