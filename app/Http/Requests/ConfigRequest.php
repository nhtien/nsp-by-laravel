<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'cfg_name' => 'bail|required|min:3|max:255',
            'cfg_des' => 'bail|max:255',
            'cfg_value' => 'bail|required|min:1|max:255',
            'cfg_status' => 'bail|required|in:inactive,activated',
            'cfg_type' => 'bail|required|in:int,float,string,array,json',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.config.update' => array_merge($ruleDefault, ['cfg_id' => 'bail|required|exists:configs,conf_id']),
            'be.config.store' => array_merge($ruleDefault, ['cfg_key' => 'bail|required|min:1|max:255|unique:configs,conf_key']),
            default => [],
        };
    }
}
