<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'pst_grp_name' => 'bail|required|min:3|max:255',
            'pst_grp_name_en' => 'bail|required|min:3|max:255',
            'pst_grp_status' => 'bail|required|in:inactive,activated',
            'pst_grp_code' => 'bail|required|min:3|max:255',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.postgroup.update' => array_merge($ruleDefault, ['pst_grp_id' => 'bail|required|exists:post_group,pgrp_id']),
            'be.postgroup.store' => $ruleDefault,
            default => [],
        };
    }
}
