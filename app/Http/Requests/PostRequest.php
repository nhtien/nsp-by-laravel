<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'pst_author' => 'bail|nullable|min:3|max:255|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'pst_status' => 'bail|required|in:inactive,activated',
            'pst_grp_id' => 'bail|required|integer',
            'pst_thumbnail' => 'nullable|max:1024|mimetypes:image/jpeg,image/png,image/gif|dimensions:min_width=200,min_height=200',

            'pst_name' => 'bail|required|min:1|max:1000|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'pst_short_des' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_meta_des' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_meta_keyword' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_content' => 'bail|required',

            'pst_name_en' => 'bail|nullable|max:1000|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'pst_short_des_en' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_meta_des_en' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_meta_keyword_en' => 'bail|nullable|max:1000|do_not_contain_html_tag',
            'pst_content_en' => 'bail',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.post.update' => array_merge($ruleDefault, ['pst_id' => 'bail|required|exists:post,post_id']),
            'be.post.store' => array_merge($ruleDefault, ['pst_thumbnail' => 'bail|required|max:1024|mimetypes:image/jpeg,image/png,image/gif|dimensions:min_width=200,min_height=200']),
            default => [],
        };
    }
}
