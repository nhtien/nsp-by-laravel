<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_brd_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_brd_name_en' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_brd_picture' => 'bail|required|min:1|max:255|do_not_contain_multiple_whitespace|do_not_contain_html_tag',
            'prod_brd_status' => 'bail|required|min:1|max:255',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productbrand.update' => array_merge($ruleDefault, ['prod_brd_id' => 'bail|required|exists:product_brands,prodbrd_id']),
            'be.productbrand.store' => $ruleDefault,
            default => [],
        };
    }
}
