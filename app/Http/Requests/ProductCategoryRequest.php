<?php

namespace App\Http\Requests;

use App\Models\ProductTag;
use Illuminate\Foundation\Http\FormRequest;

class ProductCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_cat_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_short_name' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_short_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_keyword' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_keyword_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_description' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_cat_description_en' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_cat_thumbnail' => 'bail|nullable|max:1024|mimetypes:image/jpeg,image/png,image/gif|dimensions:min_width=300,min_height=300',
            'prod_tag_ids' => [
                'bail',
                'nullable',
                function ($attribute, $value, $fail) {
                    $ids = convert_ids_to_array($value);
                    if (count($ids) > 0) {
                        $countTag = ProductTag::query()->select('prtag_id')->whereIn('prtag_id', $ids)->count();
                        if (count($ids) != $countTag) {
                            $fail('Tags không tồn tại trong cơ sở dữ liệu.');
                        }
                    }

                    return true;
                },
            ],
            'prod_cat_icon' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_cat_display_header_menu' => 'bail|required|in:full,short',
            'prod_cat_display_sidebar' => 'bail|required|in:full,short',
            'prod_cat_type' => 'bail|required|in:new,popular,default',
            'prod_cat_status' => 'bail|required|in:inactive,activated',
        ];

        $id = request()->post('prod_cat_id');
        $status = request()->post('prod_cat_status');
        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productcategory.store' => array_merge($ruleDefault, ['prod_cat_position' => 'bail|required|in:left,right,before,after']),
            'be.productcategory.update' => array_merge($ruleDefault, [
                'prod_cat_status' => "bail|required|in:inactive,activated|check_node_status:product_category,{$id},{$status}",
                'prod_cat_id' => 'bail|required|exists:product_categories,prcat_id',
            ]),
            default => [],
        };
    }
}
