<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductChildrenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_child_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_child_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_child_rank' => 'bail|required|integer',
            'prod_child_status' => 'bail|required|in:inactive,activated',
            'prod_child_part_number' => 'bail|required|min:1|max:255|do_not_contain_html_tag|unique:product_children,part_number,'.request()->post('prod_child_id').',prchl_id',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productchildren.update' => $ruleDefault,
            'be.productchildren.store' => array_merge($ruleDefault,
                [
                    'prod_id' => 'bail|required|exists:products,prod_id',
                    'prod_child_part_number' => 'bail|required|min:1|max:255|do_not_contain_html_tag|unique:product_children,part_number',
                ]),
            default => [],
        };
    }
}
