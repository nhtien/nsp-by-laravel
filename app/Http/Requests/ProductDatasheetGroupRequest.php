<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductDatasheetGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_sht_grp_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_sht_grp_name_en' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_sht_grp_status' => 'bail|required|in:inactive,activated',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productdatasheetgroup.update' => array_merge($ruleDefault, ['prod_sht_grp_id' => 'bail|required|exists:product_datasheet_groups,prdagrp_id']),
            'be.productdatasheetgroup.store' => $ruleDefault,
            default => [],
        };
    }
}
