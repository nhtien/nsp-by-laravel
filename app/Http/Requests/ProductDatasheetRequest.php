<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductDatasheetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_sht_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_sht_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_sht_rank' => 'bail|required|integer',
            'prod_sht_file' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_sht_link' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_sht_status' => 'bail|required|in:inactive,activated',
            'prod_sht_type' => 'bail|required|in:default,primary',
            'prod_sht_grp_id' => 'bail|required|exists:product_datasheet_groups,prdagrp_id',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productdatasheet.update' => $ruleDefault,
            'be.productdatasheet.store' => array_merge($ruleDefault, ['prod_id' => 'bail|required|exists:products,prod_id']),
            default => [],
        };
    }
}
