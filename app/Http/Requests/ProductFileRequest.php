<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $routeName = $this->route()->getName();
        $fileName = $routeName == 'be.productfile.store' ? 'required' : 'nullable';

        return [
            'prod_file_title' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_file_name' => "bail|{$fileName}",
            'prod_file_status' => 'bail|required|in:inactive,activated',
            'prod_file_des' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
        ];
    }
}
