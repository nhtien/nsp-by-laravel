<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductPropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_pro_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_pro_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_pro_icon' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_pro_rank' => 'bail|required|integer',
            'prod_pro_status' => 'bail|required|in:inactive,activated',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.productproperty.update' => $ruleDefault,
            'be.productproperty.store' => array_merge($ruleDefault, ['prod_id' => 'bail|required|exists:products,prod_id']),
            default => [],
        };
    }
}
