<?php

namespace App\Http\Requests;

use App\Models\ProductTag;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $routeName = $this->route()->getName();
        $required = ($routeName == 'be.product.store') ? 'required' : 'nullable';

        $ruleDefault = [
            'prod_part_number' => 'bail|required|min:1|max:255|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'prod_part_number_old' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'prod_part_number_type' => 'bail|required|in:default,slash,viewmore,suffix-x',
            'prod_name' => 'bail|required|min:1|max:500|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'prod_name_en' => 'bail|nullable|min:1|max:500|do_not_contain_html_tag|do_not_contain_multiple_whitespace',
            'prod_short_des' => 'bail|required|min:1|max:1000',
            'prod_short_des_en' => 'bail|nullable|min:1|max:1000',
            'prod_type' => 'bail|required|in:new,hot,sale,default',
            'prod_sale_off' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_brd_id' => 'bail|required|exists:product_brands,prodbrd_id',
            'prod_cat_id' => 'bail|required|exists:product_categories,prcat_id',
            'prod_tag_ids' => [
                'bail',
                'nullable',
                function ($attribute, $value, $fail) {
                    $ids = convert_ids_to_array($value);
                    if (count($ids) > 0) {
                        $countTag = ProductTag::query()->select('prtag_id')->whereIn('prtag_id', $ids)->count();
                        if (count($ids) != $countTag) {
                            $fail('Tags không tồn tại trong cơ sở dữ liệu.');
                        }
                    }

                    return true;
                },
            ],
            'prod_meta_des' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_meta_des_en' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_meta_keyword' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_meta_keyword_en' => 'bail|nullable|min:1|max:1000|do_not_contain_html_tag',
            'prod_status' => 'bail|required|in:inactive,activated',
            'prod_thumbnail' => 'bail|'.$required.'|max:1024|mimetypes:image/jpeg,image/png,image/gif|dimensions:min_width=380,min_height=380',
        ];

        return match ($routeName) {
            'be.product.store', 'be.product.update' => $ruleDefault,
            default => [],
        };
    }
}
