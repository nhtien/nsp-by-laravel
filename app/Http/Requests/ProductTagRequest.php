<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'prod_tag_name' => 'bail|required|min:1|max:255|do_not_contain_html_tag',
            'prod_tag_name_en' => 'bail|nullable|min:1|max:255|do_not_contain_html_tag',
            'prod_tag_type' => 'bail|required|in:default,primary',
            'prod_tag_status' => 'bail|required|in:inactive,activated',
        ];

        $id = request()->post('prod_tag_id');
        $status = request()->post('prod_tag_status');
        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.producttag.store' => $ruleDefault,
            'be.producttag.update' => array_merge($ruleDefault, [
                'prod_tag_status' => "bail|required|in:inactive,activated|check_node_status:product_tag,{$id},{$status}",
                'prod_tag_id' => 'bail|required|exists:product_tags,prtag_id',
            ]),
            default => [],
        };
    }
}
