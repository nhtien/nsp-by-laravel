<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'sld_grp_name' => 'bail|required|min:1|max:255',
            'sld_grp_note' => 'bail|required|min:1|max:255',
            'sld_grp_status' => 'bail|required|in:inactive,activated',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.slidergroup.update' => array_merge($ruleDefault, ['sld_grp_id' => 'bail|required|exists:slider_groups,sldgrp_id']),
            'be.slidergroup.store' => array_merge($ruleDefault, ['sld_grp_code' => 'bail|required|min:1|max:255|unique:slider_groups,code']),
            default => [],
        };
    }
}
