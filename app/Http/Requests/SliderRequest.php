<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $ruleDefault = [
            'sld_name' => 'bail|required|min:1|max:255',
            'sld_description' => 'bail|nullable|max:10000',
            'sld_description_en' => 'bail|nullable|max:10000',
            'sld_status' => 'bail|required|in:inactive,activated',
            'sld_picture' => 'bail|nullable|max:10240|mimetypes:image/jpeg,image/png,image/gif',
            'sld_position' => 'bail|required|integer',
            'sld_grp_id' => 'bail|required|integer',
        ];

        $routeName = $this->route()->getName();

        return match ($routeName) {
            'be.slider.update' => array_merge($ruleDefault, ['sld_id' => 'bail|required|exists:sliders,sld_id']),
            'be.slider.store' => array_merge($ruleDefault, ['sld_picture' => 'bail|required|nullable|max:10240|mimetypes:image/jpeg,image/png,image/gif']),
            default => [],
        };
    }
}
