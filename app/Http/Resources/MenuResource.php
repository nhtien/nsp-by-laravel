<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'menu_id' => $this->id,
            'menu_code' => $this->code,
            'menu_name' => $this->name,
            'menu_name_en' => $this->name_en,
            'menu_link' => $this->link,
            'menu_link_en' => $this->link_en,
            'menu_special' => $this->special,
            'menu_icon' => $this->icon,
            'menu_parent' => $this->parent,
            'menu_level' => $this->level,
            'menu_left' => $this->left,
            'menu_right' => $this->right,
            'menu_status' => $this->status,

        ];

        //  Lay phan tu menu con
        if ($this->childrens) {
            $results['childrens'] = $this->childrens;
        }

        return $results;

        //return parent::toArray($request);
    }
}
