<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use JsonSerializable;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'pst_id' => $this->post_id,
            'pst_thumbnail' => $this->thumbnail,
            'pst_name' => $this->name,
            'pst_short_des' => Str::words($this->short_description, 35, '...'),
            'pst_short_des_full' => $this->short_description,
            'pst_content' => $this->content,
            'pst_status' => $this->status,
            'pst_created_at' => $this->created_at,
            'pst_uri' => $this->uri,
            'pst_slug' => Str::slug($this->name),
        ];

        return $results;
    }
}
