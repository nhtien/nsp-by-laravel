<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductBrandResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'prodbrd_id' => $this->prodbrd_id,
            'prodbrd_name' => $this->name,
            'prodbrd_name_en' => $this->name_en,
            'prodbrd_picture' => $this->picture,
            'prodbrd_picture_small' => $this->picture_small,
            'prodbrd_status' => $this->status,
            'prodbrd_uri' => $this->uri,
        ];
    }
}
