<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'prodcat_id' => $this->prcat_id,
            'prodcat_type' => $this->type,
            'prodcat_name' => $this->name,
            'prodcat_slug' => $this->slug,
            'prodcat_url' => $this->url,
            'prodcat_level' => $this->level,
            'prodcat_parent' => $this->parent,
            'prodcat_short_name' => $this->short_name,

        ];

        //  Lay phan tu menu con
        if ($this->childrens) {
            $results['childrens'] = $this->childrens;
        }

        return $results;

        //return parent::toArray($request);
    }
}
