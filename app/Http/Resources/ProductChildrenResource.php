<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductChildrenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'prodchild_id' => $this->prchl_id,
            'prodchild_part_number' => $this->part_number,
            'prodchild_name' => $this->name,
            'prodchild_status' => $this->status,
            'prod_id' => $this->prod_id,

        ];
    }
}
