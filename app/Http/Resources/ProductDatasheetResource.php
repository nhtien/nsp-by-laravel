<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductDatasheetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'prodsheet_id' => $this->prda_id,
            'prodsheet_name' => $this->name,
            'prodsheet_file' => $this->file,
            'prodsheet_link' => $this->link,
            'prodsheet_status' => $this->status,
            'prodsheet_rank' => $this->rank,
            'prod_id' => $this->prod_id,
            'prodsheet_download_url' => $this->downloadUrl,
        ];

        if ($this->prodsheetgrp_name) {
            $results['prodsheetgrp_name'] = $this->prodsheetgrp_name;
        }

        return $results;
    }
}
