<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductPropertyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'prodppt_id' => $this->prppt_id,
            'prodppt_name' => $this->name,
            'prodppt_description' => $this->description,
            'prodppt_icon' => $this->icon,
        ];

        return $results;
    }
}
