<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use JsonSerializable;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        $results = [
            'prod_id' => $this->prod_id,
            'prod_part_number' => $this->part_number,
            'prod_part_number_old' => $this->part_number_old,
            'prod_part_number_type' => $this->part_number_display_type,
            'prod_name' => $this->name,
            'prod_short_name' => Str::words($this->name, 13, '...'),
            'prod_slug' => Str::slug($this->name),
            'prod_short_description' => $this->short_description,
            'prod_thumbnail' => $this->thumbnail,
            'prod_brand_id' => $this->prodbrd_id,
            'prod_url' => $this->url,
            'prod_type' => $this->type,
        ];

        if ($this->images_related) {
            $results['images_related'] = $this->images_related;
        }

        if ($this->prodbrd_name) {
            $results['prodbrd_name'] = $this->prodbrd_name;
        }

        if ($this->product_properties) {
            $results['product_properties'] = $this->product_properties;
        }

        if ($this->product_childrens) {
            $results['product_childrens'] = $this->product_childrens;
        }

        if ($this->product_datasheets) {
            $results['product_datasheets'] = $this->product_datasheets;
        }

        if ($this->product_tags) {
            $results['product_tags'] = $this->product_tags;
        }

        return $results;
    }
}
