<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;
use JsonSerializable;

class ProductTagCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        if ($this->resource instanceof LengthAwarePaginator) {
            return [
                'product-tags' => ProductTagResource::collection($this->collection),
                'links' => [
                    'first' => $this->url(1),
                    'last' => $this->url($this->lastPage()),
                    'prev' => $this->previousPageUrl(),
                    'next' => $this->nextPageUrl(),
                ],
                'meta' => [
                    'current_page' => $this->currentPage(),
                    'from' => ($this->currentPage() - 1) * $this->count() + 1,
                    'last_page' => $this->lastPage(),
                    'path' => $this->path(),
                    'per_page' => $this->perPage(),
                    'to' => $this->currentPage() * $this->count(),
                    'total' => $this->total(),
                ],
            ];
        } else {
            return [
                'product-tags' => ProductTagResource::collection($this->collection),
            ];
        }
    }
}
