<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class ProductTagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'prodtag_id' => $this->prtag_id,
            'prodtag_name' => $this->name,
            'prodtag_name_en' => $this->name_en,
            'prodtag_status' => $this->status,
            'prodtag_parent' => $this->parent,
            'prodtag_uri' => $this->uri,
        ];
    }
}
