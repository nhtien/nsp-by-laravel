<?php

namespace App\Listeners;

use App\Enums\Product as ProductEnum;
use App\Models\Product;

class ProductFolderCreationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Folder sẽ được tạo khi chưa tồn tại. Đổi tên folder khi part-number được thay đổi
     */
    public function handle(object $event): void
    {
        $product = $event->product;
        if ($product instanceof Product) {
            $changes = $product->getChanges();
            $partNumber = $product->getRawOriginal('part_number') ? $product->getRawOriginal('part_number') : $product->part_number;
            // rename folder
            if (is_array($changes) && isset($changes['part_number']) && ! empty($changes['part_number'])) {
                $partNumberNew = $changes['part_number'];
                if (create_folder_name($partNumberNew) != create_folder_name($partNumber)) {
                    rename_folder(
                        ProductEnum::PRODUCT_RELATED_PATH->path().create_folder_name($partNumber),
                        ProductEnum::PRODUCT_RELATED_PATH->path().create_folder_name($partNumberNew)
                    );
                    rename_folder(
                        ProductEnum::DATASHEET_PATH->path().create_folder_name($partNumber),
                        ProductEnum::DATASHEET_PATH->path().create_folder_name($partNumberNew)
                    );
                    /*rename_folder(
                        ProductEnum::PRODUCT_FILE_PATH->path().create_folder_name($partNumber),
                        ProductEnum::PRODUCT_FILE_PATH->path().create_folder_name($partNumberNew)
                    );*/
                }
            }
            // create folder
            else {
                // Tạo folder khi part_number tồn tại
                if ($partNumber != '' || $partNumber != null) {
                    create_folder(ProductEnum::PRODUCT_RELATED_PATH->path(), $partNumber);
                    create_folder(ProductEnum::DATASHEET_PATH->path(), $partNumber);
                    //create_folder(ProductEnum::PRODUCT_FILE_PATH->path(), $partNumber);
                }
            }
        }
    }
}
