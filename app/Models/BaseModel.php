<?php

namespace App\Models;

use App\Events\WriteModelLogEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Traits\Tappable;

class BaseModel extends Model
{
    use Tappable;

    const GET_DATA_OF_OTHER_LANGUAGE = true;

    protected $perPage = 50;

    public static function boot(): void
    {
        parent::boot();

        static::deleted(function ($item) {
            if ($item->getTable() != 'model_logs') {
                event(new WriteModelLogEvent($item));
            }
        });
    }

    const ALIAS = []; //To convert data key and format

    public function revertAlias($data): array
    {
        $return = [];
        foreach (static::ALIAS as $field => $alias) {
            if (($value = Arr::get($data, $alias)) !== null) {
                Arr::set($return, $field, $value);
            }
        }

        return $return;
    }

    protected function getAttrByLang($attrName): mixed
    {
        if (app()->getLocale() == 'vi') {
            return $this->$attrName;
        }

        $data = $this->{$attrName.'_'.app()->getLocale()};

        // Lấy dữ liệu của ngôn ngữ khác khi dữ liệu hiện tại rỗng hoặc null
        if (self::GET_DATA_OF_OTHER_LANGUAGE) {
            return $data ? $data : $this->$attrName;
        }

        return $data;
    }
}
