<?php

namespace App\Models;

use App\Casts\DateFormat;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class Post
 *
 * @property int id
 * @property string full_name
 * @property string company_name
 * @property string company_address
 * @property string phone_number
 * @property string email
 * @property string subject
 * @property string message
 * @property string is_new
 */
class Contact extends BaseModel
{
    use HasFactory;

    protected $table = 'contacts';

    protected $primaryKey = 'id';

    public const CREATED_AT = 'created_at';

    public const UPDATED_AT = 'updated_at';

    protected $perPage = 50;

    protected $casts = [
        'created_at' => DateFormat::class,
        'updated_at' => DateFormat::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'company_name',
        'company_address',
        'phone_number',
        'email',
        'subject',
        'message',
    ];

    public function searching(): LengthAwarePaginator
    {
        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('is_view', 'is_view'),
            ])
            ->defaultSort('-id')
            ->paginate();
    }
}
