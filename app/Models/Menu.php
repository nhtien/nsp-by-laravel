<?php

namespace App\Models;

use App\Traits\Models\Attributes\MenuAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\MenuScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;

/**
 * Class Menu
 *
 * @property int menu_id
 * @property string name
 * @property string controller
 * @property string action
 * @property int parent
 * @property string status
 * @property string icon
 *
 * @method isActivated
 * @method isParent
 * @method notParent
 */
class Menu extends BaseModel
{
    use HasFactory, InputFilter, MenuScope, MenuAttribute;

    protected $table = 'menus';

    protected $primaryKey = 'menu_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $perPage = 100;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'controller',
        'action',
        'name',
        'parent',
        'status',
        'icon',
        'position',
        'http_query',
    ];

    /**
     * @return Builder[]|Collection
     */
    public function getMenuByUser(string $menuType = 'parent'): Collection|array
    {
        $conditionParent = ($menuType == 'parent') ? '=' : '>';
        if (Auth::guard()->check()) {
            $user = Auth::guard()->user();
            if (in_array($user->user_id, config('constants.account.root_user_ids')) && in_array($user->usrgroup_id, config('constants.account.root_group_ids'))) {
                return static::query()->where('parent', $conditionParent, 0)->orderBy('position')->get();
            } else {
                $adminMenuArr = [0];
                if ($user->userGroup instanceof UserGroup) {
                    if (is_array($user->userGroup->menu_ids) && count($user->userGroup->menu_ids) > 0) {
                        $adminMenuArr = $user->userGroup->menu_ids;
                    }
                }

                return static::query()->where('parent', $conditionParent, 0)->whereIn('menu_id', $adminMenuArr)->orderBy('position')->get();
            }
        } else {
            return static::query()->where('parent', 0)->where('menu_id', 0)->orderBy('position')->get();
        }
    }

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query()->isActivated();
    }

    public function getMenusByType(string $type = 'all'): mixed
    {
        if ($type == 'parent') {
            return $this::parentQuery()->isParent()->get();
        } elseif ($type == 'children') {
            return $this::parentQuery()->notParent()->get();
        } else {
            return $this::parentQuery()->get();
        }
    }

    public function getMenuDontAccess(array $menuIds): mixed
    {
        return $this::parentQuery()->whereNotIn('menu_id', $menuIds)->isActivated()->get();
    }
}
