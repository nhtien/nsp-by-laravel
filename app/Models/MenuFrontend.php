<?php

namespace App\Models;

use App\Models\Solutions\NestedSetModel;
use App\Traits\Models\Attributes\MenuFrontendAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\MenuFrontendScope;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class MenuFrontend
 *
 * @property int id
 * @property string code
 * @property string name
 * @property string name_en
 * @property string link
 * @property string link_en
 * @property string special
 * @property string icon
 * @property int parent
 * @property int level
 * @property int left
 * @property int right
 * @property string status
 *
 * @method isActivated
 * @method notRootItem
 */
class MenuFrontend extends NestedSetModel
{
    use HasFactory, InputFilter, MenuFrontendScope, MenuFrontendAttribute;

    protected $table = 'menu_frontend';

    protected $primaryKey = 'id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $perPage = 100;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const SPECIAL = [
        'default' => 'Mặc định',
        'link' => 'Liên kết',
        'popular' => 'Phổ biến',
        'new' => 'Mới',
        'hot' => 'Hot',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'name',
        'name_en',
        'link',
        'link_en',
        'special',
        'icon',
        'parent',
        'level',
        'left',
        'right',
        'status',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * Không bao giờ lấy id = 1
     */
    public static function query(): Builder
    {
        return parent::query()->notRootItem();
    }

    /**
     * @throws Exception
     */
    public function updateNode(array $params, int $nodeMoveId, string $positionNode = 'left'): bool
    {
        $item = $this::parentQuery()->find($nodeMoveId);

        // Loại bỏ những phần tử không được phép update
        $data = collect($params)->except(['parent', 'level', 'left', 'right'])->toArray();
        $item->update($data);

        // Thực hiện move node
        if (@$params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            return false;
        } elseif (@$params['parent'] != $item->id) {
            $this->moveNode(['move_item' => $item, 'partner_id' => $params['parent']], ['position' => $positionNode]);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function createNode(array $params, string $positionNode = 'left'): bool
    {
        if ($params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            $positionNode = 'left';
        }

        return $this->insertNode($params, ['position' => $positionNode]);
    }

    /**
     * @throws Exception
     */
    public function deleteNode(array $params, string $type = 'only'): bool
    {
        return $this->removeNode($params, ['type' => $type]);
    }

    public function getAllItems(int $exceptBranchId = 0): Collection|array
    {
        $query = self::parentQuery();
        // KHÔNG lấy items không thuộc nhánh
        if ($exceptBranchId > 0) {
            $item = self::parentQuery()->find($exceptBranchId);
            if ($item) {
                $query = $query->notBranch($item->left, $item->right);
            }
        }

        return $query->orderBy('left', 'asc')->get();
    }

    public function findsMenuHeader(): mixed
    {
        return $this::query()
            ->where('level', '<', 4)
            ->orderBy('left', 'ASC')
            ->status('activated')->get();
    }
}
