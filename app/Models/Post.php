<?php

namespace App\Models;

use App\Casts\DateFormat;
use App\Traits\Models\Attributes\PostAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\PostRelationship;
use App\Traits\Models\Scopes\PostScope;
use App\Traits\Models\StoreImage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class Post
 *
 * @property int post_id
 * @property string thumbnail
 * @property string name
 * @property string name_en
 * @property string short_name
 * @property string short_name_en
 * @property string content
 * @property string content_en
 * @property string short_description
 * @property string short_description_en
 * @property int pgrp_id
 * @property string status
 * @property int views
 * @property string meta_description
 * @property string meta_description_en
 * @property string meta_keyword
 * @property string meta_keyword_en
 * @property string created_at
 * @property string updated_at
 * @property string author
 * @property string uuid
 * @property int user_id
 *
 * @method isActivated
 */
class Post extends BaseModel
{
    use HasFactory, HashId, InputFilter, StoreImage, PostScope, PostAttribute, PostRelationship;

    protected $table = 'post';

    protected $primaryKey = 'post_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    protected $perPage = 50;

    protected $appends = [
        'slug',
        'uri',
        'name_trans',
        'short_name_trans',
        'content_trans',
        'short_description_trans',
        'meta_description_trans',
        'meta_keyword_trans',
        'encode_id',
        'url',
    ];

    protected $casts = [
        'created_at' => DateFormat::class,
        'updated_at' => DateFormat::class,
    ];

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const THUMBNAIL_PATH = '/public/uploads/images/thumbs/post/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thumbnail',
        'name',
        'name_en',
        'short_name',
        'short_name_en',
        'content',
        'content_en',
        'short_description',
        'short_description_en',
        'pgrp_id',
        'status',
        'views',
        'meta_description',
        'meta_description_en',
        'meta_keyword',
        'meta_keyword_en',
        'created_at',
        'updated_at',
        'author',
        'user_id',
    ];

    public function totalPosts(): int
    {
        return self::query()->select('post_id')->count();
    }

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }

    /**
     * Bài viết KHÔNG đươc lưu trong session => tăng lượt xem + 1.
     * Ngược lại (thời gian đã xem > 60 phút) sẽ xóa session này và tăng lượt xem + 1.
     *
     * @return $this
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function postViewCounter(Post $post): static
    {
        $sessionKey = 'post_view_counter_'.$post->post_id;
        if (session()->has($sessionKey)) {
            $lastTimeViewed = session()->get($sessionKey)['last_time_viewed'];
            // current time > last time viewed + 60 minutes
            if (time() >= $lastTimeViewed + 3600) {
                $post->increment('views');
            }
        } else {
            $post->increment('views');
        }
        session()->put($sessionKey, ['post_id' => $post->post_id, 'last_time_viewed' => time()]);

        return $this;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('pst_id', 'post_id'),
                AllowedFilter::exact('pst_status', 'status'),
                AllowedFilter::exact('pst_grp_id', 'pgrp_id'),
            ])
            ->with('postGroup')
            ->with('user')
            ->defaultSort('-post_id')
            ->allowedSorts([
                AllowedSort::field('pst_id', 'post_id'),
            ])
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhereFullText('name', "%$keyword%")
                        ->orWhereFullText('name_en', "%$keyword%")
                        ->orWhereFullText('content', "%$keyword%")
                        ->orWhereFullText('content_en', "%$keyword%")
                        ->orWhereFullText('short_description', "%$keyword%")
                        ->orWhereFullText('short_description_en', "%$keyword%");
                });
            })
            ->paginate();
    }
}
