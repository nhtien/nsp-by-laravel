<?php

namespace App\Models;

use App\Traits\Models\Attributes\PostGroupAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\PostGroupRelationship;
use App\Traits\Models\Scopes\PostGroupScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PostGroup
 *
 * @property int pgrp_id
 * @property string name
 * @property string name_en
 * @property string code
 * @property string status
 *
 * @method isActivated
 */
class PostGroup extends BaseModel
{
    use HasFactory, InputFilter, PostGroupScope, PostGroupRelationship, PostGroupAttribute;

    protected $table = 'post_group';

    protected $primaryKey = 'pgrp_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $perPage = 50;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'code',
        'status',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }
}
