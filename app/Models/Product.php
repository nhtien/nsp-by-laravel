<?php

namespace App\Models;

use App\Casts\DateFormat;
use App\Casts\Json;
use App\Traits\Models\Attributes\ProductAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\ProductRelationship;
use App\Traits\Models\Scopes\ProductScope;
use App\Traits\Models\StoreImage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class Product
 *
 * @property int post_id
 * @property string part_number
 * @property string part_number_old
 * @property string part_number_display_type
 * @property string name
 * @property string name_en
 * @property string short_description
 * @property string short_description_en
 * @property int views
 * @property string type
 * @property int sale_off
 * @property string thumbnail
 * @property int prodbrd_id
 * @property int product_category_id
 * @property string product_tag_ids
 * @property string created_at
 * @property string updated_at
 * @property string meta_description
 * @property string meta_description_en
 * @property string meta_keyword
 * @property string meta_keyword_en
 * @property string status
 * @property int user_id
 * @property string folder_name_image_related
 * @property string path_image_related
 * @property string path_datasheet
 *
 * @method isActivated
 */
class Product extends BaseModel
{
    use HasFactory,
        HashId,
        InputFilter,
        StoreImage,
        ProductAttribute,
        ProductRelationship,
        ProductScope;

    protected $table = 'products';

    protected $primaryKey = 'prod_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    protected $perPage = 20;

    protected $appends = [
        'folder_name_by_part_number',
        'path_image_related',
        'path_datasheet',
        'path_file',
        'path_thumbnail',
        'image_folder_ckfinder',
        'datasheet_files',
        'url',
        'images_related',
    ];

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const PART_NUMBER_DISPLAY_TYPE = [
        'default' => 'Mặc định',
        'slash' => 'Hai mã sp',
        'viewmore' => 'Xem thêm',
    ];

    const TYPE = [
        'default' => 'Mặc định',
        'hot' => 'Hot',
        'sale' => 'Sale',
        'new' => 'New',
    ];

    const THUMBNAIL_PATH = '/public/uploads/images/thumbs/product/';

    const IMAGE_RELATED_PATH = '/public/uploads/images/product--related/';

    protected $casts = [
        //'product_tag_ids'   => Json::class,
        'created_at' => DateFormat::class,
        'updated_at' => DateFormat::class,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'part_number',
        'part_number_old',
        'part_number_display_type',
        'name',
        'name_en',
        'short_description',
        'short_description_en',
        'views',
        'type',
        'sale_off',
        'thumbnail',
        'prodbrd_id',
        'prcat_id',
        'product_tag_ids',
        'solution_ids',
        'meta_description',
        'meta_description_en',
        'meta_keyword',
        'meta_keyword_en',
        'status',
        'user_id',
        'product_file_ids',
    ];

    public function totalProducts(): int
    {
        return self::query()->select('prod_id')->count();
    }

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }

    /**
     * Sản phẩm KHÔNG đươc lưu trong session => tăng lượt xem + 1.
     * Ngược lại (thời gian đã xem > 60 phút) sẽ xóa session này và tăng lượt xem + 1.
     *
     * @return $this
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function postViewCounter(Post $post): static
    {
        $sessionKey = 'product_view_counter_'.$post->post_id;
        if (session()->has($sessionKey)) {
            $lastTimeViewed = session()->get($sessionKey)['last_time_viewed'];
            // current time > last time viewed + 60 minutes
            if (time() >= $lastTimeViewed + 3600) {
                $post->increment('views');
            }
        } else {
            $post->increment('views');
        }
        session()->put($sessionKey, ['prod_id' => $post->prod_id, 'last_time_viewed' => time()]);

        return $this;
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('prod_id', 'prod_id'),
                AllowedFilter::exact('prod_status', 'status'),
                AllowedFilter::exact('prod_brd_id', 'prodbrd_id'),
                AllowedFilter::exact('prod_cat_id', 'prcat_id'),
            ])
            ->with('productBrand')
            ->with('productCategory')
            ->with('user')
            ->defaultSort('-prod_id')
            ->allowedSorts([
                AllowedSort::field('prod_id', 'post_id'),
            ])
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhereFullText('name', "%$keyword%")
                        ->orWhereFullText('name_en', "%$keyword%")
                        ->orWhereFullText('short_description', "%$keyword%")
                        ->orWhereFullText('short_description_en', "%$keyword%")
                        ->orWhere('part_number', 'LIKE', "%$keyword%")
                        ->orWhere('part_number_old', 'LIKE', "%$keyword%");
                });
            })
            ->paginate();
    }
}
