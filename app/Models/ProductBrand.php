<?php

namespace App\Models;

use App\Traits\Models\Attributes\ProductBrandAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\ProductBrandScope;
use App\Traits\Models\StoreImage;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductBrand
 *
 * @property int prodbrd_id
 * @property string name
 * @property string name_en
 * @property string picture
 * @property string status
 *
 * @method isActivated
 */
class ProductBrand extends BaseModel
{
    use HasFactory, InputFilter, StoreImage, ProductBrandScope, ProductBrandAttribute;

    protected $perPage = 50;

    protected $table = 'product_brands';

    protected $primaryKey = 'prodbrd_id';

    const PICTURE_PATH = '/public/uploads/images/product-brand/';

    const PICTURE_SMALL_PATH = '/public/uploads/images/product-brand/menu/';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'picture',
        'status',
        'description',
    ];

    protected $appends = [
        'uri',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }
}
