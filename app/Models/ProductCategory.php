<?php

namespace App\Models;

use App\Casts\Json;
use App\Models\Solutions\NestedSetModel;
use App\Traits\Models\Attributes\ProductCategoryAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\ProductCategoryScope;
use App\Traits\Models\StoreImage;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class ProductCategory
 *
 * @property int prcat_id
 * @property string name
 * @property string name_en
 * @property string short_name
 * @property string short_name_en
 * @property string keyword
 * @property string keyword_en
 * @property string description
 * @property string description_en
 * @property string thumbnail
 * @property string product_tag_ids
 * @property string display_header_menu
 * @property string display_sidebar
 * @property string type
 * @property string status
 * @property string icon
 * @property int parent
 * @property int level
 * @property int left
 * @property int right
 *
 * @method isActivated
 * @method notRootItem
 */
class ProductCategory extends NestedSetModel
{
    use HasFactory, StoreImage, ProductCategoryScope, ProductCategoryAttribute, InputFilter;

    protected $table = 'product_categories';

    protected $primaryKey = 'prcat_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $perPage = 100;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const TYPE = [
        'default' => 'Mặc định',
        'new' => 'Mới',
        'popular' => 'Phổ biến',
    ];

    const DISPLAY_SIDEBAR = [
        'full' => 'Hiển thị nhóm sp theo tên đầy đủ',
        'short' => 'Hiển thị nhóm sp theo tên rút gọn',
    ];

    const DISPLAY_HEADER_MENU = [
        'full' => 'Hiển thị nhóm sp theo tên đầy đủ',
        'short' => 'Hiển thị nhóm sp theo tên rút gọn',
    ];

    const THUMBNAIL_PATH = '/public/uploads/images/thumbs/product-category/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'short_name',
        'short_name_en',
        'keyword',
        'keyword_en',
        'description',
        'description_en',
        'thumbnail',
        'product_tag_ids',
        'display_header_menu',
        'display_sidebar',
        'type',
        'status',
        'icon',
    ];

    protected $appends = [
        'url',
        'slug',
    ];

    protected $casts = [
        'product_tag_ids' => Json::class,
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * Không bao giờ lấy id = 1
     */
    public static function query(): Builder
    {
        return parent::query()->notRootItem();
    }

    /**
     * @throws Exception
     */
    public function updateNode(array $params, int $nodeMoveId, string $positionNode = 'left'): bool
    {
        $item = $this::parentQuery()->find($nodeMoveId);

        // Loại bỏ những phần tử không được phép update
        $data = collect($params)->except(['parent', 'level', 'left', 'right'])->toArray();
        $item->update($data);

        // Thực hiện move node
        if (@$params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            return false;
        } elseif (@$params['parent'] != $item->parent) {
            $this->moveNode(['move_item' => $item, 'partner_id' => $params['parent']], ['position' => $positionNode]);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function createNode(array $params, string $positionNode = 'left'): bool
    {
        if ($params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            $positionNode = 'left';
        }

        return $this->insertNode($params, ['position' => $positionNode]);
    }

    /**
     * @throws Exception
     */
    public function deleteNode(array $params, string $type = 'only'): bool
    {
        return $this->removeNode($params, ['type' => $type]);
    }

    public function getAllItems(int $exceptBranchId = 0): Collection|array
    {
        $query = self::parentQuery();

        // KHÔNG lấy items không thuộc nhánh
        if ($exceptBranchId > 0) {
            $item = self::parentQuery()->find($exceptBranchId);
            if ($item) {
                $query = $query->notBranch($item->left, $item->right);
            }
        }

        return $query->orderBy('left', 'asc')->get();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');
        $filters = \request()->get('filter', 0);
        $branchId = @$filters['by_branch_id'];

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('prod_cat_status', 'status'),
            ])
            ->orderBy('left', 'asc')
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('name', 'LIKE', "%$keyword%")
                        ->orWhere('name_en', 'LIKE', "%$keyword%")
                        ->orWhere('short_name', 'LIKE', "%$keyword%")
                        ->orWhere('short_name_en', 'LIKE', "%$keyword%")
                        ->orWhere('prcat_id', "$keyword");
                });
            })
            ->when($branchId > 0, function ($q) use ($branchId) {
                $cat = $this::query()->find($branchId);

                return $q->branch($cat->left, $cat->right);
            })
            ->paginate(70);
    }

    public function findsHeaderMenu(): Collection|array
    {
        return $this::query()->select(['prcat_id', 'name', 'type', 'level', 'parent'])->where('status', 'activated')
            ->where('level', '<', 3)
            ->orderBy('left', 'ASC')->get();
    }

    public function findsByLevel($level, string $status = '*'): Collection|array
    {
        return $this::query()
            ->where(['level' => $level])
            ->when($status !== '*', function ($q) use ($status) {
                return $q->where('status', $status);
            })
            ->orderBy('left', 'asc')
            ->get();
    }

    public function findsChildrenByBranch($catId, string $status = '*', string $maxLevel = '*'): mixed
    {
        $cat = $this::query()->find($catId);

        return $this::query()
            ->when($status !== '*', function ($q) use ($status) {
                return $q->where('status', $status);
            })
            ->when($maxLevel !== '*', function ($q) use ($maxLevel) {
                return $q->where('level', '<=', $maxLevel);
            })
            ->where('prcat_id', '<>', $catId)
            ->branch($cat->left, $cat->right)
            ->orderBy('left', 'asc')->get();
    }

    public function findsByBranch($catId, string $status = '*', string $maxLevel = '*'): mixed
    {
        $cat = $this::query()->find($catId);

        return $this::query()
            ->when($status !== '*', function ($q) use ($status) {
                return $q->where('status', $status);
            })
            ->when($maxLevel !== '*', function ($q) use ($maxLevel) {
                return $q->where('level', '<=', $maxLevel);
            })
            ->branch($cat->left, $cat->right)
            ->orderBy('left', 'asc')->get();
    }

    public function findFirstAncestor($catId, string $status = '*')
    {
        $cat = $this::query()->find($catId);

        return $this::query()
            ->when($status !== '*', function ($q) use ($status) {
                return $q->where('status', $status);
            })
            ->where('left', '<=', $cat->left)
            ->where('right', '>=', $cat->right)
            ->orderBy('left', 'asc')->first();
    }
}
