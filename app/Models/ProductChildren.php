<?php

namespace App\Models;

use App\Traits\Models\Attributes\ProductChildrenAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\ProductChildrenRelationship;
use App\Traits\Models\Scopes\ProductChildrenScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductChildren
 *
 * @property int prchl_id
 * @property string part_number
 * @property int prod_id
 * @property string name
 * @property string name_en
 * @property string status
 * @property int rank
 *
 * @method isActivated
 */
class ProductChildren extends BaseModel
{
    use HasFactory, InputFilter, HashId, ProductChildrenAttribute, ProductChildrenRelationship, ProductChildrenScope;

    protected $perPage = 50;

    protected $table = 'product_children';

    protected $primaryKey = 'prchl_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'part_number',
        'prod_id',
        'name',
        'name_en',
        'status',
        'rank',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }
}
