<?php

namespace App\Models;

use App\Traits\Models\Attributes\ProductDatasheetAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\ProductDatasheetRelationship;
use App\Traits\Models\Scopes\ProductDatasheetScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductDatasheet
 *
 * @property int prda_id
 * @property string name
 * @property string name_en
 * @property int rank
 * @property string file
 * @property string link
 * @property string type
 * @property string status
 * @property int prod_id
 * @property int prdagrp_id
 */
class ProductDatasheet extends BaseModel
{
    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const TYPE = [
        'default' => 'File phụ',
        'primary' => 'File chính (popup xem nhanh sẽ cho download file này)',
    ];

    use HasFactory, HashId, ProductDatasheetScope, ProductDatasheetRelationship, ProductDatasheetAttribute, InputFilter;

    const DATASHEET_PATH = '/public/uploads/datasheet/';

    protected $perPage = 50;

    protected $table = 'product_datasheet';

    protected $primaryKey = 'prda_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $appends = [
        'type_convert_vietnamese',
        'download_url',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'rank',
        'file',
        'link',
        'type',
        'status',
        'prod_id',
        'prdagrp_id',
    ];

    public static function query(): Builder
    {
        return parent::query();
    }
}
