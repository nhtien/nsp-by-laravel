<?php

namespace App\Models;

use App\Traits\Models\Attributes\ProductDatasheetGroupAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\ProductDatasheetGroupScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductDatasheetGroup extends BaseModel
{
    use HasFactory, InputFilter, ProductDatasheetGroupScope, ProductDatasheetGroupAttribute;

    protected $perPage = 50;

    protected $table = 'product_datasheet_groups';

    protected $primaryKey = 'prdagrp_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prdagrp_id',
        'name',
        'name_en',
        'status',
    ];

    public static function query(): Builder
    {
        return parent::query();
    }
}
