<?php

namespace App\Models;

use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\ProductFileRelationship;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class ProductDatasheet
 *
 * @property int id
 * @property string title
 * @property string file_name
 * @property string description
 * @property string status
 */
class ProductFile extends BaseModel
{
    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    use HasFactory, HashId, InputFilter, ProductFileRelationship;

    const FILE_PATH = '/public/uploads/product--file/';

    protected $perPage = 50;

    protected $table = 'product_files';

    protected $primaryKey = 'id';

    /*    protected $appends = [
            'type_convert_vietnamese',
            'download_url',
        ];*/

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'file_name',
        'description',
        'status',
    ];

    public static function query(): Builder
    {
        return parent::query();
    }

    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('prod_file_id', 'id'),
                AllowedFilter::exact('prod_file_status', 'status'),
            ])
            ->defaultSort('-id')
            ->allowedSorts([
                AllowedSort::field('id', 'id'),
            ])
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('title', 'LIKE', "%$keyword%")
                        ->orWhere('description', 'LIKE', "%$keyword%");
                });
            })
            ->paginate();
    }
}
