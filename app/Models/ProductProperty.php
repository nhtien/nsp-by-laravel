<?php

namespace App\Models;

use App\Traits\Models\Attributes\ProductPropertyAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\ProductPropertyRelationship;
use App\Traits\Models\Scopes\ProductPropertyScope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class ProductProperty
 *
 * @property int prppt_id
 * @property string name
 * @property string name_en
 * @property string description
 * @property string description_en
 * @property string status
 * @property string icon
 * @property int rank
 * @property int prod_id
 *
 * @method isActivated
 */
class ProductProperty extends BaseModel
{
    use HasFactory, InputFilter, HashId, ProductPropertyAttribute, ProductPropertyRelationship, ProductPropertyScope;

    protected $perPage = 50;

    protected $table = 'product_property';

    protected $primaryKey = 'prppt_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    protected $appends = ['icons'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'description',
        'description_en',
        'rank',
        'prod_id',
        'icon',
        'status',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }
}
