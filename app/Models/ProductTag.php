<?php

namespace App\Models;

use App\Models\Solutions\NestedSetModel;
use App\Traits\Models\Attributes\ProductTagAttribute;
use App\Traits\Models\HashId;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Scopes\ProductTagScope;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class ProductTag
 *
 * @property int prtag_id
 * @property string name
 * @property string name_en
 * @property string type
 * @property string status
 * @property int parent
 * @property int level
 * @property int left
 * @property int right
 *
 * @method isActivated
 * @method notRootItem
 */
class ProductTag extends NestedSetModel
{
    use HasFactory;
    use HashId, InputFilter, ProductTagScope, ProductTagAttribute;

    protected $table = 'product_tags';

    protected $primaryKey = 'prtag_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    protected $perPage = 50;

    protected $appends = [
        'uri',
    ];

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const TYPE = [
        'default' => 'Mặc định',
        'primary' => 'Khóa chính',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'name_en',
        'type',
        'status',
        'parent',
        'level',
        'left',
        'right',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    /**
     * Không bao giờ lấy id = 1
     */
    public static function query(): Builder
    {
        return parent::query()->notRootItem();
    }

    /**
     * @throws Exception
     */
    public function updateNode(array $params, int $nodeMoveId, string $positionNode = 'left'): bool
    {
        $item = $this::parentQuery()->find($nodeMoveId);

        // Loại bỏ những phần tử không được phép update
        $data = collect($params)->except(['parent', 'level', 'left', 'right'])->toArray();
        $item->update($data);

        // Thực hiện move node
        if (@$params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            return false;
        } elseif (@$params['parent'] != $item->prtag_id) {
            $this->moveNode(['move_item' => $item, 'partner_id' => $params['parent']], ['position' => $positionNode]);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    public function createNode(array $params, string $positionNode = 'left'): bool
    {
        if ($params['parent'] == 1 && in_array($positionNode, ['after', 'before'])) {
            $positionNode = 'left';
        }

        return $this->insertNode($params, ['position' => $positionNode]);
    }

    /**
     * @throws Exception
     */
    public function deleteNode(array $params, string $type = 'only'): bool
    {
        return $this->removeNode($params, ['type' => $type]);
    }

    public function getAllItems(int $exceptBranchId = 0): Collection|array
    {
        $query = self::parentQuery();
        // KHÔNG lấy items không thuộc nhánh
        if ($exceptBranchId > 0) {
            $item = self::parentQuery()->find($exceptBranchId);
            if ($item) {
                $query = $query->notBranch($item->left, $item->right);
            }
        }

        return $query->orderBy('left', 'asc')->get();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('prod_tag_status', 'status'),
            ])
            ->orderBy('left', 'asc')
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('name', 'LIKE', "%$keyword%")
                        ->orWhere('name_en', 'LIKE', "%$keyword%");
                });
            })
            ->paginate();
    }
}
