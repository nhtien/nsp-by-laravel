<?php

namespace App\Models\Scopes\UserGroup;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class IgnoreRootGroup implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->whereNotIn('usrgroup_id', get_root_group_ids());
    }
}
