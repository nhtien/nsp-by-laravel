<?php

namespace App\Models;

use App\Traits\Models\Attributes\SliderAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\SliderRelationship;
use App\Traits\Models\Scopes\SliderScope;
use App\Traits\Models\StoreImage;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class Menu
 *
 * @property int sld_id
 * @property string name
 * @property string description
 * @property string description_en
 * @property int sldgrp_id
 * @property string picture
 * @property string status
 * @property int position
 *
 * @method isActivated
 */
class Slider extends BaseModel
{
    use HasFactory, InputFilter, StoreImage, SliderAttribute, SliderRelationship, SliderScope;

    protected $perPage = 50;

    protected $table = 'sliders';

    protected $primaryKey = 'sld_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    const PICTURE_PATH = '/public/uploads/images/slider/';

    // cũng chính là picture_path sử dụng cho trait
    const THUMBNAIL_PATH = '/public/uploads/images/slider/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'description_en',
        'sldgrp_id',
        'picture',
        'status',
        'name',
        'position',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact('sld_id', 'sld_id'),
                AllowedFilter::exact('sld_status', 'status'),
            ])
            ->with('sliderGroup')
            ->defaultSort('-sld_id')
            ->allowedSorts([
                AllowedSort::field('sld_id', 'sld_id'),
            ])
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('name', 'like', "%$keyword%");
                });
            })
            ->paginate();
    }
}
