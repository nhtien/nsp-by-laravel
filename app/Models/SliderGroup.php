<?php

namespace App\Models;

use App\Traits\Models\Attributes\SliderGroupAttribute;
use App\Traits\Models\InputFilter;
use App\Traits\Models\Relationships\SliderGroupRelationship;
use App\Traits\Models\Scopes\SliderGroupScope;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\AllowedSort;
use Spatie\QueryBuilder\QueryBuilder;

class SliderGroup extends BaseModel
{
    use HasFactory, InputFilter, SliderGroupAttribute, SliderGroupRelationship, SliderGroupScope;

    protected $perPage = 50;

    protected $table = 'slider_groups';

    protected $primaryKey = 'sldgrp_id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'note',
        'status',
    ];

    public static function parentQuery(): Builder
    {
        return parent::query();
    }

    public static function query(): Builder
    {
        return parent::query();
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function searching(): LengthAwarePaginator
    {
        $keyword = \request()->get('keyword', '');

        return QueryBuilder::for($this)
            ->allowedFilters([
                AllowedFilter::exact(config('aliases-model.configs.conf_id'), 'conf_id'),
                AllowedFilter::exact(config('aliases-model.configs.conf_status'), 'conf_status'),
            ])
            ->defaultSort('-conf_id')
            ->allowedSorts([
                AllowedSort::field(config('aliases-model.configs.conf_id'), 'conf_id'),
            ])
            ->when(! empty($keyword), function ($q) use ($keyword) {
                $q->where(function ($q) use ($keyword) {
                    $q->orWhere('conf_name', 'like', "%$keyword%")
                        ->orWhere('conf_description', 'like', "%$keyword%")
                        ->orWhere('conf_key', 'like', "%$keyword%")
                        ->orWhere('conf_value', 'like', "%$keyword%");
                });
            })
            ->paginate();
    }
}
