<?php

namespace App\Models;

use App\Traits\Models\Attributes\SolutionAttribute;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class Menu
 *
 * @property int sld_id
 * @property string title
 * @property string status
 *
 * @method isActivated
 */
class Solution extends BaseModel
{
    use HasFactory,
        SolutionAttribute;

    protected $perPage = 50;

    protected $table = 'solutions';

    protected $primaryKey = 'id';

    const STATUS = [
        'activated' => 'Activated',
        'inactive' => 'Inactive',
    ];

    protected $appends = [
        'url',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'title',
        'status',
    ];

    public function searching(): LengthAwarePaginator
    {
        return QueryBuilder::for($this)->paginate();
    }
}
