<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TwentyYear extends BaseModel
{
    use HasFactory;

    protected $perPage = 50;

    protected $table = '20years';

    protected $primaryKey = 'id';

    const CREATED_AT = null;

    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company',
        'regency',
        'comment',
        'enable',
        'ordering',
    ];

    public static function query(): Builder
    {
        return parent::query();
    }
}
