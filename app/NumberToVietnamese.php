<?php

namespace App;

class NumberToVietnamese
{
    private $number = '';

    public function numberToString(int $number)
    {
        if (! is_int($number) || strlen($number) > 9 || $number < 0) {
            throw new Exception('Must be an integer and less than or equal to 9 digits');
        }

        $this->number = $number;

        $output = '';
        if ($number === 0) {
            return 'không';
        } else {
            $integer = str_pad($number, 9, '0', STR_PAD_LEFT);
            $group = rtrim(chunk_split($integer, 3, ' '), ' ');
            $groups = explode(' ', $group);
            $groups2 = [];
            foreach ($groups as $key => $g) {
                $groups2[] = $this->convertThreeDigit($g[0], $g[1], $g[2]);
            }
            foreach ($groups2 as $key => $value) {
                if ($value != '') {
                    $output .= $groups2[$key].$this->convertGroup($key).' ';
                }
            }
        }

        return $output;
    }

    private function units()
    {
        return [
            0 => 'triệu',
            1 => 'nghìn',
            2 => '',
        ];
    }

    private function convertGroup($index)
    {
        $units = $this->units();
        if (array_key_exists($index, $units)) {
            return " {$units[$index]} ";
        }

        return '';
    }

    private function convertThreeDigit($digit1, $digit2, $digit3)
    {
        $buffer = '';

        if ($digit1 == '0' && $digit2 == '0' && $digit3 == '0') {
            return '';
        }

        if ($digit1 != '0') {
            $buffer .= $this->convertDigit($digit1).' trăm';
            if ($digit2 != '0' || $digit3 != '0') {
                $buffer .= ' ';
            }
        }

        if ($digit2 != '0') {
            $buffer .= $this->convertTwoDigit($digit2, $digit3);
        } elseif ($digit2 == '0' and $digit1 != '0' and $digit3 != '0') {
            $buffer .= ' linh ';
            $buffer .= $this->convertDigit($digit3);
        } elseif ($digit3 != '0') {
            $buffer .= $this->convertDigit($digit3);
        }

        return $buffer;
    }

    private function convertTwoDigit($digit1, $digit2)
    {
        if ($digit2 == '0') {
            switch ($digit1) {
                case '1':
                    return 'mười';
                case '2':
                    return 'hai mươi';
                case '3':
                    return 'ba mươi';
                case '4':
                    return 'bốn mươi';
                case '5':
                    return 'năm mươi';
                case '6':
                    return 'sáu mươi';
                case '7':
                    return 'bảy mươi';
                case '8':
                    return 'tám mươi';
                case '9':
                    return 'chín mươi';
            }
        } elseif ($digit1 == '1') {
            switch ($digit2) {
                case '1':
                    return 'mười một';
                case '2':
                    return 'mười hai';
                case '3':
                    return 'mười ba';
                case '4':
                    return 'mười bốn';
                case '5':
                    return 'mười lăm';
                case '6':
                    return 'mười sáu';
                case '7':
                    return 'mười bảy';
                case '8':
                    return 'mười tám';
                case '9':
                    return 'mười chín';
            }
        } else {
            $temp = $this->convertDigit($digit2);
            if ($temp == 'năm') {
                $temp = 'lăm';
            }
            if ($temp == 'một') {
                $temp = 'mốt';
            }
            switch ($digit1) {
                case '2':
                    return "hai mươi $temp";
                case '3':
                    return "ba mươi $temp";
                case '4':
                    return "bốn mươi $temp";
                case '5':
                    return "năm mươi $temp";
                case '6':
                    return "sáu mươi $temp";
                case '7':
                    return "bảy mươi $temp";
                case '8':
                    return "tám mươi $temp";
                case '9':
                    return "chín mươi $temp";
            }
        }
    }

    private function convertDigit($digit)
    {
        $number4 = 'tư';
        if ($this->number < 10 || $this->number >= 10000000) {
            $number4 = 'bốn';
        }
        switch ($digit) {
            case '0':
                return 'không';
            case '1':
                return 'một';
            case '2':
                return 'hai';
            case '3':
                return 'ba';
            case '4':
                return $number4;
            case '5':
                return 'năm';
            case '6':
                return 'sáu';
            case '7':
                return 'bảy';
            case '8':
                return 'tám';
            case '9':
                return 'chín';
        }
    }
}
