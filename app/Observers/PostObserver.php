<?php

namespace App\Observers;

use App\Models\Post;
use Illuminate\Support\Str;

class PostObserver
{
    public function creating(Post $post): void
    {
        $post->uuid = Str::orderedUuid()->toString();
    }

    public function updating(Post $post): void
    {
        if (empty($post->uuid) || $post->uuid == '') {
            $post->uuid = Str::orderedUuid()->toString();
        }
    }
}
