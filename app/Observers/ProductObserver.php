<?php

namespace App\Observers;

use App\Events\FolderCreatedByProductEvent;
use App\Models\Product;
use App\Models\ProductChildren;
use App\Models\ProductDatasheet;
use App\Models\ProductProperty;
use Illuminate\Support\Facades\File;

class ProductObserver
{
    public function __construct(
        protected ProductDatasheet $productDatasheetModel,
        protected ProductChildren $productChildrenModel,
        protected ProductProperty $productPropertyModel
    ) {
    }

    public function deleted(Product $product): void
    {
        if ($product->prod_id > 0) {
            /*
            |--------------------------------------------------------------------------
            | XÓA FILES
            |--------------------------------------------------------------------------
            */
            File::delete(base_path($product->thumbnail));
            File::deleteDirectory($product->path_image_related);
            File::deleteDirectory($product->path_datasheet);

            /*
            |--------------------------------------------------------------------------
            | XÓA DATASHEET
            | Khi xóa từng bản ghi thì Observer mới hoạt động.
            | When executing a mass delete statement via Eloquent, the deleting and deleted model events will not be fired for the deleted models.
            | This is because the models are never actually retrieved when executing the delete statement.
            |--------------------------------------------------------------------------
            */
            $datasheets = $this->productDatasheetModel::query()->where('prod_id', $product->prod_id)->get();
            foreach ($datasheets as $datasheet) {
                $datasheet->delete();
            }

            /*
            |--------------------------------------------------------------------------
            | XÓA PROPERTY
            |--------------------------------------------------------------------------
            */
            $properties = $this->productPropertyModel::query()->where('prod_id', $product->prod_id)->get();
            foreach ($properties as $property) {
                $property->delete();
            }

            /*
            |--------------------------------------------------------------------------
            | XÓA PRODUCT-CHILDREN
            |--------------------------------------------------------------------------
            */
            $childrens = $this->productChildrenModel::query()->where('prod_id', $product->prod_id)->get();
            foreach ($childrens as $children) {
                $children->delete();
            }
        }
    }

    public function updated(Product $product): void
    {
        event(new FolderCreatedByProductEvent($product));
    }

    public function created(Product $product): void
    {
        event(new FolderCreatedByProductEvent($product));
    }
}
