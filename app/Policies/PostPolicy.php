<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine whether the user is admin for all authorization
     *
     * @return null
     */
    public function before(User $user)
    {
        return null;
    }

    public function update(User $user, Post $post): bool
    {
        return $user->user_id === $post->user_id;
    }
}
