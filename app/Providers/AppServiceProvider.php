<?php

namespace App\Providers;

use App\Models\Menu;
use App\Models\Product;
use App\Models\User;
use App\Rules\CheckNodeStatus;
use Detection\MobileDetect;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    protected array $rules = [
        \App\Rules\Username::class,
        \App\Rules\Password::class,
        \App\Rules\DoNotContainHtmlTag::class,
        \App\Rules\DoNotContainMultipleWhitespace::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('UserModel', User::class);
        $this->app->bind('MenuModel', Menu::class);
        $this->app->bind('ProductModel', Product::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        /**
         * Register rule
         */
        $this->registerValidationRules();
        $this->registerValidationRulesWithParameters();

        /**
         * share view
         */
        $this->shareView();

        /**
         * Create throttle middleware
         */
        RateLimiter::for('uploads', function (Request $request) {
            return $request->input('allow')
                ? Limit::none()
                : Limit::perMinute(1)->by($request->ip());
        });
    }

    private function registerValidationRulesWithParameters(): void
    {
        Validator::extend('check_node_status', function ($attribute, $value, $parameters, $validator) {
            [$modelName, $nodeId, $status] = $parameters;

            return (new CheckNodeStatus($modelName, $nodeId, $status))->passes($attribute, $value);
        });
    }

    private function registerValidationRules(): void
    {
        foreach ($this->rules as $class) {
            $alias = (new $class)->__toString();
            if ($alias) {
                Validator::extend($alias, $class.'@passes');
            }
        }
    }

    public function shareView(): void
    {
        $detect = new MobileDetect;

        View::share('showMenuBackend', true);
        view()->share('isMobile', $detect->isMobile());
    }
}
