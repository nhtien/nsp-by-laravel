<?php

namespace App\Providers;

use App\Blade\Directives;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * https://github.com/appstract/laravel-blade-directives
     * https://devdojo.com/tnylea/custom-laravel-blade-directive
     *
     * @return void
     */
    public function boot()
    {
        $this->registerDirectives();
    }

    public function registerDirectives()
    {
        $directiveObj = new Directives();
        $directives = $directiveObj->register();

        collect($directives)->each(function ($item, $key) {
            Blade::if($key, $item);
        });
    }
}
