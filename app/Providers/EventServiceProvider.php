<?php

namespace App\Providers;

use App\Events\FolderCreatedByProductEvent;
use App\Events\NewCustomerHasRegisteredEvent;
use App\Events\WriteModelLogEvent;
use App\Listeners\ProductFolderCreationListener;
use App\Listeners\WelcomeNewCustomerListener;
use App\Listeners\WriteModelLogListener;
use App\Models\Post;
use App\Models\Product;
use App\Observers\PostObserver;
use App\Observers\ProductObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        NewCustomerHasRegisteredEvent::class => [
            WelcomeNewCustomerListener::class,
        ],
        WriteModelLogEvent::class => [
            WriteModelLogListener::class,
        ],
        FolderCreatedByProductEvent::class => [
            ProductFolderCreationListener::class,
        ],
    ];

    /**
     * @var string[][]
     */
    protected $observers = [
        Product::class => [ProductObserver::class],
        Post::class => [PostObserver::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
