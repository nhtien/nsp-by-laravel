<?php

namespace App\Rules;

use App\Models\MenuFrontend;
use App\Models\ProductCategory;
use App\Models\ProductTag;
use Illuminate\Contracts\Validation\Rule;

/**
 * Khi status chuyển thành inactive, kiểm tra tất cả node con - nếu có 1 node con (status = activated) sẽ thông báo lỗi.
 * + Nếu status từ inactive => activated thì không cần kiểm tra (chỉ cần activated node đó và node tổ tiên)
 * + Ngược lại (activated => inactive) thì cần kiểm tra tất cả node con, nếu có 1 node con là activated thì ko cho node cha inactive.
 */
class CheckNodeStatus implements Rule
{
    protected ProductTag|ProductCategory|MenuFrontend|null  $mainModel;

    /**
     * Id của item
     */
    protected int $nodeId;

    /**
     * Trạng thái mà item sẽ được cập nhật
     */
    protected string $status;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($modelName, $nodeId, $status)
    {
        $this->mainModel = match ($modelName) {
            'product_category' => new ProductCategory(),
            'product_tag' => new ProductTag(),
            'menu_frontend' => new MenuFrontend(),
            default => null,
        };
        $this->nodeId = $nodeId;
        $this->status = $status;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        if ($this->mainModel === null) {
            return false;
        }

        $item = $this->mainModel::parentQuery()->find($this->nodeId);
        if (! $item) {
            return false;
        }

        if ($item->status != $this->status) {
            if ($this->status == 'inactive' &&
                $this->mainModel::parentQuery()
                    ->branch($item->left, $item->right)
                    ->where('status', 'activated')
                    ->notEqualToId($item->id_value)
                    ->first()
            ) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return __('validation.check_node_status');
    }
}
