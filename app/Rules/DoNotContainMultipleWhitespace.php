<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class DoNotContainMultipleWhitespace implements Rule
{
    protected string $alias = 'do_not_contain_multiple_whitespace';

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->alias;
    }

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     */
    public function passes($attribute, $value): bool
    {
        return strlen($value) == strlen(Str::squish($value));
        //return $value == preg_replace('/\s+/', ' ', $value);;
    }

    /**
     * Get the validation error message.
     */
    public function message(): string
    {
        return __('validation.do_not_contain_multiple_whitespace');
    }
}
