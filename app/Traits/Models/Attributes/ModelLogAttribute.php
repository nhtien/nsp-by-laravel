<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait ModelLogAttribute
{
    public function tableName(): Attribute
    {
        return new Attribute(
            get: fn ($value) => strtoupper($value),
            set: fn ($value) => strip_tags($value),
        );
    }

    public function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) => strtoupper($value),
            set: fn ($value) => in_array($value, ['deleted', 'updated']) ? strtolower($value) : 'deleted',
        );
    }
}
