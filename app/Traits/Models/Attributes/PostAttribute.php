<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait PostAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortName(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortNameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function content(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function contentEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function shortDescription(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function shortDescriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function metaDescription(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    public function metaDescriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    public function metaKeyword(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    public function metaKeywordEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    public function thumbnail(): Attribute
    {
        return new Attribute(
            get: fn ($value) => self::THUMBNAIL_PATH.$value,
        );
    }

    public function uri(): Attribute
    {
        return new Attribute(
            get: fn () => (route('fe.post.show', ['id' => $this->post_id, 'slug' => Str::slug($this->name)])),
        );
    }

    public function slug(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->post_id.'-'.Str::slug($this->name))
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    public function url(): Attribute
    {
        return new Attribute(
            get: fn () => route('fe.post.show', ['slug' => Str::slug($this->name), 'id' => $this->post_id])
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }

    public function shortNameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('short_name'),
        );
    }

    public function contentTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('content'),
        );
    }

    public function shortDescriptionTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('short_description'),
        );
    }

    public function metaDescriptionTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('meta_description'),
        );
    }

    public function metaKeywordTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('meta_keyword'),
        );
    }
    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
