<?php

namespace App\Traits\Models\Attributes;

use App\Enums\Product;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;
use JsonException;

trait ProductAttribute
{
    public function partNumber(): Attribute
    {
        return new Attribute(
            set: fn ($value) => strip_tags($value),
        );
    }

    public function partNumberOld(): Attribute
    {
        return new Attribute(
            set: fn ($value) => strip_tags($value),
        );
    }

    /**
     * @throws JsonException
     */
    public function getProductTagIdsAttribute(mixed $value): array
    {
        if (is_json($value)) {
            $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);

            return @$value['tag_ids'] ? $value['tag_ids'] : [];
        }

        return [];
    }

    public function setProductTagIdsAttribute(mixed $value): void
    {
        if (is_json($value)) {
            $this->attributes['product_tag_ids'] = $value;
        } elseif (is_string($value)) {
            $this->attributes['product_tag_ids'] = json_encode(['tag_ids' => explode('||', $value)]);
        } else {
            $this->attributes['product_tag_ids'] = json_encode(['tag_ids' => $value]);
        }
    }


    public function setProductFileIdsAttribute(mixed $value): void
    {
        if (is_json($value)) {
            $this->attributes['product_file_ids'] = $value;
        } elseif (is_string($value)) {
            $this->attributes['product_file_ids'] = json_encode(['file_ids' => explode('||', $value)]);
        } else {
            $this->attributes['product_file_ids'] = json_encode(['file_ids' => $value]);
        }
    }

    /**
     * @throws JsonException
     */
    public function getProductFileIdsAttribute(mixed $value): array
    {
        if (is_json($value)) {
            $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);

            return @$value['file_ids'] ? $value['file_ids'] : [];
        }

        return [];
    }

    public function setSolutionIdsAttribute(mixed $value): void
    {
        if (is_json($value)) {
            $this->attributes['solution_ids'] = $value;
        } elseif (is_string($value)) {
            $this->attributes['solution_ids'] = json_encode(['solution_ids' => explode('||', $value)]);
        } else {
            $this->attributes['solution_ids'] = json_encode(['solution_ids' => $value]);
        }
    }

    public function getSolutionIdsAttribute(mixed $value): array
    {
        if (is_json($value)) {
            $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);

            return @$value['solution_ids'] ? $value['solution_ids'] : [];
        }

        return [];
    }

    public function partNumberDisplayType(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['default', 'slash', 'viewmore', 'suffix-x']) ? $value : 'default',
        );
    }

    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortDescription(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortDescriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['new', 'hot', 'sale', 'default']) ? $value : 'default',
        );
    }

    public function saleOff(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function thumbnail(): Attribute
    {
        return new Attribute(
            get: fn ($value) => asset(self::THUMBNAIL_PATH.$value),
        );
    }

    public function getImagesRelatedAttribute(): array
    {
        $imagesRelated = [];
        $images = get_file_name_in_folder($this->path_image_related);
        foreach ($images as $image) {
            $imagesRelated[] = asset(self::IMAGE_RELATED_PATH.$this->folder_name_by_part_number.'/'.$image);
        }

        return $imagesRelated;
    }

    public function metaDescription(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function metaDescriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function metaKeyword(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function metaKeywordEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    /*
    |--------------------------------------------------------------------------
    | START - Thuộc tính liên quan (Không thuộc db)
    |--------------------------------------------------------------------------
    */

    public function folderNameByPartNumber(): Attribute
    {
        return new Attribute(
            get: fn ($value) => create_folder_name($this->part_number),
        );
    }

    public function pathImageRelated(): Attribute
    {
        return new Attribute(
            get: fn ($value) => Product::PRODUCT_RELATED_PATH->path().create_folder_name($this->part_number),
        );
    }

    public function pathThumbnail(): Attribute
    {
        return new Attribute(
            get: fn ($value) => Product::PRODUCT_THUMBNAIL_PATH->path().$this->getRawOriginal('thumbnail'),
        );
    }

    public function pathDatasheet(): Attribute
    {
        return new Attribute(
            get: fn ($value) => Product::DATASHEET_PATH->path().create_folder_name($this->part_number),
        );
    }

    public function pathFile(): Attribute
    {
        return new Attribute(
            get: fn ($value) => Product::PRODUCT_FILE_PATH->path().create_folder_name($this->part_number),
        );
    }

    public function imageFolderCkfinder(): Attribute
    {
        return new Attribute(
            get: fn ($value) => 'product--related/'.create_folder_name($this->part_number),
        );
    }

    public function datasheetFiles(): Attribute
    {
        return new Attribute(
            get: fn () => get_file_name_in_folder($this->path_datasheet, 'pdf'),
        );
    }

    public function files(): Attribute
    {
        return new Attribute(
            get: fn () => get_file_name_in_folder($this->path_file),
        );
    }

    public function url(): Attribute
    {
        return new Attribute(
            get: fn () => route('fe.product.detail', ['slug' => Str::slug($this->name), 'id' => $this->prod_id]),
        );
    }

    /*
    |--------------------------------------------------------------------------
    | START - Get attribute by language
    |--------------------------------------------------------------------------
    */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }

    public function shortDescriptionTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('short_description'),
        );
    }

    public function metaDescriptionTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('meta_description'),
        );
    }

    public function metaKeywordTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('meta_keyword'),
        );
    }

    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
