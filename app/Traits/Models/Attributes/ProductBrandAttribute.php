<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait ProductBrandAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function picture(): Attribute
    {
        return new Attribute(
            get: fn ($value) => self::PICTURE_PATH.$value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function pictureSmall(): Attribute
    {
        return new Attribute(
            get: fn ($value) => self::PICTURE_SMALL_PATH.$this->getRawOriginal('picture')
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    public function uri(): Attribute
    {
        return new Attribute(
            get: fn () => (route('fe.product-brand.detail', ['id' => $this->prodbrd_id, 'slug' => Str::slug($this->name)])),
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }
    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
