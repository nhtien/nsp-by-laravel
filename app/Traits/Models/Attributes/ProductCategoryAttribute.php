<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait ProductCategoryAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortName(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function shortNameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function keyword(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    public function keywordEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags(strtolower($value)),
        );
    }

    public function description(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    public function descriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    public function thumbnail(): Attribute
    {
        return new Attribute(
            get: fn ($value) => self::THUMBNAIL_PATH.$value,
        );
    }

    public function displayHeaderMenu(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['full', 'short']) ? $value : 'full',
        );
    }

    public function displaySidebar(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['full', 'short']) ? $value : 'full',
        );
    }

    public function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['new', 'popular', 'default']) ? $value : 'default',
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    public function icon(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strtolower(strip_tags($value)),
        );
    }

    public function url(): Attribute
    {
        return new Attribute(
            get: fn () => route('fe.product.get-product-by-category', ['slug' => Str::slug($this->name), 'id' => $this->prcat_id])
        );
    }

    public function slug(): Attribute
    {
        return new Attribute(
            get: fn () => Str::slug($this->name)
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }

    public function shortNameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('short_name'),
        );
    }
    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
