<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait ProductDatasheetAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function file(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function link(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    public function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['default', 'primary']) ? $value : 'default',
        );
    }

    /*
    |--------------------------------------------------------------------------
    | Attributes mở rộng
    |--------------------------------------------------------------------------
    */

    /**
     * route để tải datasheet
     */
    public function downloadUrl(): Attribute
    {
        return new Attribute(
            get: fn () => route('fe.product-datasheet.download', ['hashId' => $this->encode_Id]),
        );
    }

    public function typeConvertVietnamese(): Attribute
    {
        return new Attribute(
            get: fn () => self::TYPE[$this->type],
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }
    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
