<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait ProductTagAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function nameEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['primary', 'default']) ? $value : 'default',
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    public function uri(): Attribute
    {
        return new Attribute(
            get: fn () => (route('fe.product.finds-product-by-tag', ['slug' => Str::slug($this->name), 'hashId' => encode_hash_id($this->prtag_id)])),
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function nameTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }
    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
