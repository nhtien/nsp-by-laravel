<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;

trait SliderAttribute
{
    public function name(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => strip_tags($value),
        );
    }

    public function description(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function descriptionEn(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => $value,
        );
    }

    public function picture(): Attribute
    {
        return new Attribute(
            get: fn ($value) => self::PICTURE_PATH.$value,
        );
    }

    public function status(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value,
            set: fn ($value) => in_array($value, ['activated', 'inactive']) ? $value : 'inactive',
        );
    }

    /*
   |--------------------------------------------------------------------------
   | START - Get attribute by language
   |--------------------------------------------------------------------------
   */

    public function descriptionTrans(): Attribute
    {
        return new Attribute(
            get: fn () => $this->getAttrByLang('name'),
        );
    }

    /*
    |--------------------------------------------------------------------------
    | END - Get attribute by language
    |--------------------------------------------------------------------------
    */
}
