<?php

namespace App\Traits\Models\Attributes;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Str;

trait SolutionAttribute
{
    public function url(): Attribute
    {
        return new Attribute(
            get: fn () => route('fe.solution.detail', ['slug' => Str::slug($this->title), 'id' => $this->id]),
        );
    }
}
