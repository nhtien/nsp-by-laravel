<?php

namespace App\Traits\Models;

trait InputFilter
{
    protected array $tables = [
        'post' => 'posts',
        'configs' => 'configs',
        'menus' => 'menus',
        'menu_frontend' => 'menu_frontends',
        'post_group' => 'post_groups',
        'products' => 'products',
        'product_brands' => 'product_brands',
        'product_categories' => 'product_categories',
        'product_datasheet_groups' => 'product_datasheet_groups',
        'product_tags' => 'product_tags',
        'sliders' => 'sliders',
        'slider_groups' => 'slider_groups',
        'users' => 'users',
        'user_groups' => 'user_groups',
        'product_datasheet' => 'product_datasheets',
        'product_children' => 'product_childrens',
        'product_property' => 'product_properties',
        'product_files' => 'product_files',
    ];

    /**
     * Lấy fields được phép update/insert
     */
    public function getDataFields(array $data, array $except = []): array
    {
        $data = get_column_matches($this->tables[$this->table], $data);

        return collect($data)->except($except)->toArray();
    }
}
