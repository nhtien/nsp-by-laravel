<?php

namespace App\Traits\Models\Relationships;

use App\Models\Post;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait PostGroupRelationship
{
    public function posts(): HasMany
    {
        return $this->hasMany(Post::class, 'pgrp_id', 'pgrp_id');
    }
}
