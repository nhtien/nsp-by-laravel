<?php

namespace App\Traits\Models\Relationships;

use App\Models\PostGroup;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait PostRelationship
{
    public function postGroup(): BelongsTo
    {
        return $this->belongsTo(PostGroup::class, 'pgrp_id', 'pgrp_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }
}
