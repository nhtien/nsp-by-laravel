<?php

namespace App\Traits\Models\Relationships;

use App\Models\Product;
use App\Models\ProductDatasheetGroup;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait ProductDatasheetRelationship
{
    public function productDatasheetGroup(): BelongsTo
    {
        return $this->belongsTo(ProductDatasheetGroup::class, 'prdagrp_id', 'prdagrp_id');
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'prod_id', 'prod_id');
    }
}
