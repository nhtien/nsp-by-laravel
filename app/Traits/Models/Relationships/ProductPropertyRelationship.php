<?php

namespace App\Traits\Models\Relationships;

use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait ProductPropertyRelationship
{
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'prod_id', 'prod_id');
    }
}
