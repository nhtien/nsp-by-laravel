<?php

namespace App\Traits\Models\Relationships;

use App\Models\ProductBrand;
use App\Models\ProductCategory;
use App\Models\ProductChildren;
use App\Models\ProductDatasheet;
use App\Models\ProductDatasheetGroup;
use App\Models\ProductFile;
use App\Models\ProductProperty;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait ProductRelationship
{
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id');
    }

    public function productBrand(): BelongsTo
    {
        return $this->belongsTo(ProductBrand::class, 'prodbrd_id', 'prodbrd_id');
    }

    public function productCategory(): BelongsTo
    {
        return $this->belongsTo(ProductCategory::class, 'prcat_id', 'prcat_id');
    }

    public function productDatasheets(): HasMany
    {
        return $this->hasMany(ProductDatasheet::class, 'prod_id', 'prod_id');
    }

    public function productChildrens(): HasMany
    {
        return $this->hasMany(ProductChildren::class, 'prod_id', 'prod_id');
    }

    public function productProperties(): HasMany
    {
        return $this->hasMany(ProductProperty::class, 'prod_id', 'prod_id');
    }

    public function productFiles(): HasMany
    {
        return $this->hasMany(ProductFile::class, 'prod_id', 'prod_id');
    }

    public function productDatasheetGroups(): BelongsToMany
    {
        return $this->belongsToMany(
            ProductDatasheetGroup::class,
            ProductDatasheet::class,
            'prod_id',
            'prdagrp_id',
            'prod_id',
            'prdagrp_id'
        );
    }
}
