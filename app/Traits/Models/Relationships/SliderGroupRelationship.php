<?php

namespace App\Traits\Models\Relationships;

use App\Models\Slider;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait SliderGroupRelationship
{
    public function sliders(): HasMany
    {
        return $this->hasMany(Slider::class, 'sldgrp_id', 'sldgrp_id');
    }
}
