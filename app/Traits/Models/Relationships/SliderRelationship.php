<?php

namespace App\Traits\Models\Relationships;

use App\Models\SliderGroup;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait SliderRelationship
{
    public function sliderGroup(): BelongsTo
    {
        return $this->belongsTo(SliderGroup::class, 'sldgrp_id', 'sldgrp_id');
    }
}
