<?php

namespace App\Traits\Models\Relationships;

use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait UserGroupRelationship
{
    public function user(): HasMany
    {
        return $this->hasMany(User::class, 'usrgroup_id', 'usrgroup_id');
    }
}
