<?php

namespace App\Traits\Models\Relationships;

use App\Models\UserGroup;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait UserRelationship
{
    public function userGroup(): BelongsTo
    {
        return $this->belongsTo(UserGroup::class, 'usrgroup_id', 'usrgroup_id');
    }
}
