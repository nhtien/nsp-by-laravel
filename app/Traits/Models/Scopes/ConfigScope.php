<?php

namespace App\Traits\Models\Scopes;

trait ConfigScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('conf_status', 'activated');
    }

    public function scopeStatus($query, $status): mixed
    {
        return $query->where('conf_status', $status);
    }

    public function scopeType($query, $type): mixed
    {
        return $query->where('conf_type', $type);
    }

    public function scopeKey($query, $key): mixed
    {
        return $query->where('conf_key', $key);
    }
}
