<?php

namespace App\Traits\Models\Scopes;

trait MenuFrontendScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeNotRootItem($query): mixed
    {
        return $query->where('id', '<>', 1);
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }

    public function scopeSpecial($query, $value): mixed
    {
        return $query->where('special', $value);
    }
}
