<?php

namespace App\Traits\Models\Scopes;

trait MenuScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeIsParent($query): mixed
    {
        return $query->where('parent', 0);
    }

    public function scopeNotParent($query): mixed
    {
        return $query->where('parent', '<>', 0);
    }

    public function scopeParent($query, $value): mixed
    {
        return $query->where('parent', $value);
    }

    public function scopeController($query, $value): mixed
    {
        return $query->where('controller', $value);
    }

    public function scopeAction($query, $value): mixed
    {
        return $query->where('action', $value);
    }
}
