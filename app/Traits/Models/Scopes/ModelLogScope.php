<?php

namespace App\Traits\Models\Scopes;

trait ModelLogScope
{
    public function scopeType($query, $type): mixed
    {
        return $query->where('type', $type);
    }
}
