<?php

namespace App\Traits\Models\Scopes;

trait PostGroupScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }

    public function scopeCode($query, $value): mixed
    {
        return $query->where('code', $value);
    }
}
