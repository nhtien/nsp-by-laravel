<?php

namespace App\Traits\Models\Scopes;

trait PostScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeStatus($query, $status): mixed
    {
        return $query->where('conf_status', $status);
    }

    public function scopeGroup($query, $value): mixed
    {
        return $query->where('pgrp_id', $value);
    }
}
