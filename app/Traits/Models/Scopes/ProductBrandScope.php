<?php

namespace App\Traits\Models\Scopes;

trait ProductBrandScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }
}
