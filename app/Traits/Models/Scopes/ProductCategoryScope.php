<?php

namespace App\Traits\Models\Scopes;

trait ProductCategoryScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }

    public function scopeType($query, $value): mixed
    {
        return $query->where('type', $value);
    }

    public function scopeNotRootItem($query): mixed
    {
        return $query->where('prcat_id', '<>', 1);
    }

    public function scopeTagId($query, $id): mixed
    {
        return $query->whereJsonContains("product_tag_ids->$id", "$id");
    }
}
