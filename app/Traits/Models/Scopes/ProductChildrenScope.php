<?php

namespace App\Traits\Models\Scopes;

trait ProductChildrenScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopePartNumber($query, $value): mixed
    {
        return $query->where('part_number', $value);
    }

    public function scopeProdId($query, $value): mixed
    {
        return $query->where('prod_id', $value);
    }
}
