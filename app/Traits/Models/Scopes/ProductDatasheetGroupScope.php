<?php

namespace App\Traits\Models\Scopes;

trait ProductDatasheetGroupScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }
}
