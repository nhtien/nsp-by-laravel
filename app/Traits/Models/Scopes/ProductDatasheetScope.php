<?php

namespace App\Traits\Models\Scopes;

trait ProductDatasheetScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }

    public function scopeType($query, $value): mixed
    {
        return $query->where('type', $value);
    }

    public function scopeProdId($query, $value): mixed
    {
        return $query->where('prod_id', $value);
    }
}
