<?php

namespace App\Traits\Models\Scopes;

trait ProductPropertyScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeProdId($query, $value): mixed
    {
        return $query->where('prod_id', $value);
    }
}
