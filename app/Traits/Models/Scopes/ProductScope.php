<?php

namespace App\Traits\Models\Scopes;

trait ProductScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopePartNumber($query, $value): mixed
    {
        return $query->where('part_number', $value);
    }

    public function scopePartNumberOld($query, $value): mixed
    {
        return $query->where('part_number_old', $value);
    }

    public function scopeType($query, $value): mixed
    {
        return $query->where('type', $value);
    }

    public function scopeStatus($query, $value): mixed
    {
        return $query->where('status', $value);
    }

    public function scopeProdId($query, $value): mixed
    {
        return $query->where('prod_id', $value);
    }
}
