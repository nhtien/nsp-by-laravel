<?php

namespace App\Traits\Models\Scopes;

trait ProductTagScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }

    public function scopeNotRootItem($query): mixed
    {
        return $query->where('prtag_id', '<>', 1);
    }

    public function scopeType($query, $value): mixed
    {
        return $query->where('type', $value);
    }
}
