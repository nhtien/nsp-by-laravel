<?php

namespace App\Traits\Models\Scopes;

trait SliderGroupScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }
}
