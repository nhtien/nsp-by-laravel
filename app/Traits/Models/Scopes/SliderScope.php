<?php

namespace App\Traits\Models\Scopes;

trait SliderScope
{
    public function scopeIsActivated($query): mixed
    {
        return $query->where('status', 'activated');
    }
}
