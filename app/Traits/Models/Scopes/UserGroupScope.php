<?php

namespace App\Traits\Models\Scopes;

trait UserGroupScope
{
    public function scopeNotRootUserGroup($query): mixed
    {
        return $query->whereNotIn('usrgroup_id', config('constants.account.root_group_ids'));
    }
}
