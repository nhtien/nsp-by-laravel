<?php

namespace App\Traits\Models\Scopes;

trait UserScope
{
    public function scopeNotRootUser($query): mixed
    {
        return $query->whereNotIn('usrgroup_id', config('constants.account.root_user_ids'));
    }
}
