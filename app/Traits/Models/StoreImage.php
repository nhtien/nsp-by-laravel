<?php

namespace App\Traits\Models;

use JetBrains\PhpStorm\ArrayShape;

trait StoreImage
{
    public function uploadAndSaveThumbnail(string $file, mixed $model, bool $isDeleteThumbnail = true): bool
    {
        $result = $this->uploadThumbnail($file);
        if ($result['status'] === true) {
            if ($isDeleteThumbnail === true) {
                delete_file(base_path($model->getOriginal('thumbnail')));
            }
            $model->thumbnail = $result['filename'];
            $model->save();

            return true;
        }

        return false;
    }

    /**
     * Sử dụng cho bảng slider
     */
    public function uploadAndSavePicture(string $file, mixed $model, bool $isDeletePicture = true): bool
    {
        $result = $this->uploadThumbnail($file);
        if ($result['status'] === true) {
            if ($isDeletePicture === true) {
                delete_file(base_path($model->getOriginal('picture')));
            }
            $model->picture = $result['filename'];
            $model->save();

            return true;
        }

        return false;
    }

   #[ArrayShape(['status' => 'bool', 'filename' => 'string'])]
 public function uploadThumbnail(string $file): array
 {
     $fileName = create_filename($file);
     $result = ['status' => false, 'filename' => $fileName];
     if (upload_file($file, base_path(static::THUMBNAIL_PATH), $fileName) === true) {
         $result['status'] = true;
     }

     return $result;
 }
}
