<?php

namespace App\View\Components\BE\Elements;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PopupCkfinder extends Component
{
    public string $type;

    public string $folder;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($type, $folder)
    {
        $this->type = $type;
        $this->folder = $folder;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|string|Closure
    {
        return view('components.be.elements.popup-ckfinder');
    }
}
