<?php

namespace App\View\Components\BE;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Menu extends Component
{
    public array $parentMenus;

    public array $childrenMenus;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        /**
         * @var \App\Models\Menu $MenuModel
         */
        $menuModel = app('MenuModel');
        $this->parentMenus = $menuModel->getMenuByUser('parent')->toArray();
        $this->childrenMenus = collect($menuModel->getMenuByUser('children')->toArray())->groupBy('parent')->toArray();
    }

    public function render(): View|string|Closure
    {
        return view('components.be.menu');
    }
}
