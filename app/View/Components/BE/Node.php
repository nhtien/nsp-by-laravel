<?php

namespace App\View\Components\BE;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Node extends Component
{
    public string $id;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|string|Closure
    {
        return view('components.be.node');
    }
}
