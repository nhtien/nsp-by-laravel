<?php

namespace App\View\Components\BE\Searching;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ActionBox extends Component
{
    public string $refreshUrl;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($refreshUrl)
    {
        $this->refreshUrl = $refreshUrl;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|string|Closure
    {
        return view('components.be.searching.action-box');
    }
}
