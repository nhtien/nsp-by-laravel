<?php

namespace App\View\Components\FE\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use function view;

class Input extends Component
{
    /**
     * @var array|string[]
     */
    public array $class = [
        'checkbox' => 'form-check-input',
        'text' => 'form-control',
    ];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.input');
    }
}
