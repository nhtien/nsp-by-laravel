<?php

namespace App\View\Components\FE\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use function view;

class Option extends Component
{
    public string $selected = '';

    public string $value = '';

    public string $label = '';

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($value, $label)
    {
        $this->value = $value;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.option');
    }

    /**
     * Determine if the given option is the currently selected option.
     */
    public function isSelected(string $option): bool
    {
        return $option === $this->selected;
    }
}
