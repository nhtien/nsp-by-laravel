<?php

return [
    'route' => [
        'backend' => 'be',
        'frontend' => 'fe',
    ],
    'account' => [
        'root_user_ids' => [1],
        'root_group_ids' => [1],
    ],
    'cache_css' => 1,
    'cache_js' => 1,
    'schedule_send_email' => ['huutiendh11th@gmail.com'],
];
