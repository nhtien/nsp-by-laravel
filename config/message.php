<?php

return [
    'store_successful' => 'Tạo mới thành công',
    'update_successful' => 'Cập nhật thành công',
    'destroy_successful' => 'Xóa thành công',
];
