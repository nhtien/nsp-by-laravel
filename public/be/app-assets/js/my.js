var mainForm = 'form#main-form';
var checkboxItem = 'table.main-table input[name="cid[]"]';

/**
 * multiple post data
 */
$('.js-post-default').click(function () {
    var url = $(this).data('url');
    $(mainForm).attr('action', url);
    $(mainForm).submit();
});

$('.js-edit').click(function () {
    var url = $(this).data('url');
    var httpQuery = $(this).data('http-query');
    httpQuery = (httpQuery) ? httpQuery : '';
    var items = $(checkboxItem);
    $.each(items, function( index, item ) {
        var isCheck = $(this).is(':checked');
        if (isCheck == true) {
            window.location.replace(url + '?id=' + $(this).val() + httpQuery);
            return false;
        }
    });
});

$('.js-get').click(function () {
    var url = $(this).data('url');
    var httpQuery = $(this).data('http-query');
    httpQuery = (httpQuery) ? httpQuery : '';
    var items = $(checkboxItem);

    let listIds = items.map(function() {
        if($(this).is(':checked')){
            return this.value;
        }else{
            return 0;
        }
    }).get();

    listIds = listIds.filter(function(id){
        return id != 0;
    });

    window.location.replace(url + '?ids=' + listIds.join() + '&' + httpQuery);
});

/**
 * Delete trang list
 */
$('.js-delete').click(function () {
    if(confirm('Are you sure to delete this record?')){
        var items = $(checkboxItem + ':checked');
        if (items.length < 1) return false;

        var url = $(this).data('url');
        items.each(function(index, item){
            $('#main-form').append('<input type="hidden" name="_method" value="DELETE">');
            $('#main-form').append('<input type="hidden" name="id" value="'+$(item).val()+'">');
            $('#main-form').attr('action', url).submit();
            return false;
        });
        return false;
    }
});

/**
 * Delete item ngoài trang list
 */
$('.js-delete-item').click(function () {
    var message = 'Are you sure to delete this record?';
    if ($(this).data('message') != undefined) {
        message = $(this).data('message');
    }
    if(confirm(message)){
        var url = $(this).data('url');
        var id  = $(this).data('id');
        $('#delete-form').append('<input type="hidden" name="id" value="'+ id +'">');
        $('#delete-form').attr('action', url).submit();
        return false;
    }
});
/*$('.js-deletes').click(function () {
   if(confirm('Are you sure to delete this record?')){
       var url = $(this).data('url');
       var httpQuery = $(this).data('http-query');
       httpQuery = (httpQuery) ? httpQuery : '';
       var items = $(checkboxItem);

       let listIds = items.map(function() {
           if($(this).is(':checked')){
               return this.value;
           }else{
               return 0;
           }
       }).get();
       listIds = listIds.filter(function(id){
           return id != 0;
       });
       window.location.replace(url + '?ids=' + listIds.join() + httpQuery);
   }
});*/

/**
 * post edit by action type
 */
$('.js-post-edit').click(function () {
    var url = $(this).data('url');
    var actionType = $(this).data('action-type');

    // get multiple select
    var selects = $(mainForm + ' select.js-select-multiple');
    selects.each(function(index, value) {
        var selectedItems = $(value).find(':selected');
        var name = $(value).data('name');
        $("input[name="+name+"]").remove();
        if (selectedItems.length > 0) {
            var ids = [];
            selectedItems.each(function (index, item) {
                ids.push($(item).val());
            });
            var idsValue = ids.join('||');
            // insert input ids
            $( "<input type='hidden' name='"+name+"' value='" + idsValue + "'/>" ).insertBefore( $(this));
        }
    });

    $(mainForm).attr('action', url);
    $(mainForm + " input[name='action_type']").attr('value', actionType);

    setTimeout(function(){
        $(mainForm).submit();
    }, 700);
});

$('body').on('change','.city-select',function(e){
    $.ajax({
        method:'GET',
        url:getListDistrictByCityLink.replace('/0', "/" + $(this).val()),
        dataType: "json"
    }).done(function(res){
        if(res.status){
            $('.district-select').html(res.html);
        }
    })
});

$('body').on('change','.district-select',function(e){
    $.ajax({
        method:'GET',
        url:getListWardByDistrictLink.replace('/0', "/" + $(this).val()),
        dataType: "json"
    }).done(function(res){
        if(res.status){
            $('.ward-select').html(res.html);
        }
    })
});

$('#exportExcel').click(function (){
    $('input[name="isExport"]').val(1);
    $('#' + $(this).data('formid')).submit();
});


$('#btnSearch').click(function (e){
    e.preventDefault();
    $('input[name="isExport"]').val(0);
    $('#' + $(this).data('formid')).submit();
});


function getUrlVars(url)
{
    var vars = [], hash;
    var hashes = url.slice(url.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
