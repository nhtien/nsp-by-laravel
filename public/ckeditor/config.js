/**
 * @license Copyright (c) 2003-2022, CKSource Holding sp. z o.o. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
    config.filebrowserBrowseUrl = '/ckfinder-show.html?type=Images&folder=__detail';
    //config.filebrowserUploadUrl = '/ckfinder-upload.html?command=QuickUpload&type=Images';
/*    config.language  = 'vi';
    config.uiColor   = '#E8E8E8';*/
    config.entities = false;
    config.allowedContent = true;
    config.autoParagraph = false;
    /*
        Mặc định ckeditor sẽ xóa những thẻ không có nội dung
        https://stackoverflow.com/questions/18250404/ckeditor-strips-i-tag
    */
    config.protectedSource.push(/<i[^>]*><\/i>/g);
};
