/*
* Lọc dữ liệu tại category list
*  * lọc theo tag và brand
* */



/********** FILTER PRODUCT **************/
/*
$('ul.js-tag-submit a').click(function() {
      $('form#nsp-form input#product-tag').val($(this).data('tag'));
      $('form#nsp-form').submit();
});
$('form#nsp-form select#product-brand').change(function() {
      $('form#nsp-form').submit();
});
*/

$('ul.js-tag-submit a, select#product-brand').on('click change', function(event) {
    /*** ELEMENTS ***/
    var loadMoreUrlElement    = $('.js-load-more-url');
    var categoryFilterElement = $('.js-category-filter');
    var productNotFoundMgsElement = $('.js-product-not-found-mgs');
    var productListElement    = $('.js-product-list');
    var tagElement   		  = $('form#nsp-form input[name=\'product-tag\']');
    var brandElement 	      = $('form#nsp-form select[name=\'product-brand\']');
    var loadMoreElement       = $('.js-load-more');
    var itemCountPerPageElement = $('.js-item-count-per-page');


    /* Values */
    var filterType = $(this).data('filter');


    /* set a value to tag and disable brand when is click event*/
    if (filterType == 'tag')
        tagElement.val($(this).data('tag'));
    else if (filterType == 'brand') {
        if (event.type == 'click') return false;
    }

    /* Clear and set product list content*/
    productListElement.html(' ');
    productListElement.html('\<p\ class=\"text-center nsp-color\">Loading...\</p\>');

    /* hide load more*/
    loadMoreElement.css('display', 'none');

    /* set paged number*/
    loadMoreElement.data('paged', 1);


    /* active tag*/
    $('ul.js-tag-submit li').removeClass('active');
    $('ul.js-tag-submit a[data-tag=\"' + tagElement.val() + '\"]').parent().addClass('active');

    $.ajax({
        method: 'POST',
        url: loadMoreUrlElement.val().toString(),
        data: {paged: 1, tagID: tagElement.val(), brandID: brandElement.val(), categoryChild: categoryFilterElement.val().toString()}
    })
    .fail(function () {
    })
    .always(function () {
    })
    .done(function (data) {
        if (data == false) {
            productListElement.html('<h4 class="nsp-color text-center" style="min-height: 50px">'+productNotFoundMgsElement.val()+'</h4>');
        } else {
            productListElement.html(data);
            $('div.js-grid-item-new').slideUp(500).delay(400).fadeIn(500);

            /***** remove js-grid-item-new class ******/
            $('div.grid-item').removeClass('js-grid-item-new');

            /* display load-more? */
            setTimeout(function () {
                var productItems = $('div.js-grid-item');
                if (productItems.length == itemCountPerPageElement.val())
                    loadMoreElement.css('display', 'inline-block');
            }, 500);

            /** lightbox ajax **/
            lightboxAjax();
        }
    });
});


/*************** ĐÁNH DẤU CATEGORY ĐƯỢC CHỌN BÊN LEFT MENU ********/
$(document).ready(function(){
    var catID = $('.js-category-current').val().toString();
    var categorySelected =  $('.js-product-category-active a[data-cat-id='+catID+']');
    var categoryParentID = categorySelected.data('parent-id');
    if(typeof categoryParentID != 'undefined'){
        /***** div tag active *****/
        $('.js-product-category-active a[data-cat-id='+categoryParentID+']').parent().parent().addClass('ac-active');

        /********** current category: color :#C3393D **********/
        $('.js-product-category-active a[data-cat-id='+catID+']').parent().parent().removeAttr('style').css('font-size', '13px');
        $('.js-product-category-active a[data-cat-id='+catID+']').css('color', '#C3393D');
    }else{
        $('.js-product-category-active a[data-cat-id='+catID+']').addClass('nsp-color');
    }
});