/*
* Mở oppup của nhưng sp cần báo giá
*
* */
function openRequestQuotePopup(value, url){
    $.ajax({
        method: 'POST',
        url: url,
        data: { productItems: value.toString()}
    })
    .fail(function() {
    })
    .always(function() {})
    .done(function( data ) {
        $('.js-lightbox-open').attr('href', url);
        $('.js-lightbox-open').trigger('click');
    });
}