$( document ).ready(function() {


    var lazyElements = $('img.js-lazy');
    var lazylength   = lazyElements.length;
    var i  = 0;
    for (i = 0; i < lazylength; i++){
        var element    = lazyElements[i];
        var dataSrcVal =  $(element).data('src');
        $(element).attr( "src", dataSrcVal );
        $(element).removeAttr( "data-src" );
    }


    // Background image
    var lazyBgElements = $('.js-lazy-bg');
    var lazyBglength   = lazyBgElements.length;
    var y  = 0;
    for (y = 0; y < lazyBglength; y++){
        var element    = lazyBgElements[y];
        var dataSrcVal =  $(element).data('src');

        $(element).css( "background-image", 'url(' + dataSrcVal + ')'  );
        $(element).removeAttr( "data-src" );
    }
});