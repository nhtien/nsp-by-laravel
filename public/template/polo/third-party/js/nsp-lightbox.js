function lightboxAjax(){
    var lightboxOpenElement = $('.js-lightbox-open');
    var lightboxElement     = $('.js-lightbox');
    lightboxElement.click(function (event) {
        lightboxOpenElement.attr('href', $(this).attr('href'));
        lightboxOpenElement.trigger('click');
        return false;
    });
}