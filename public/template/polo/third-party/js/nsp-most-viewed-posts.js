/*
* bài viết xem nhiều nhất
*
* */

$(document).ready(function(){
    var link = $('input.js-most-viewed-posts-link');
    $.ajax({
        method: 'POST',
        url: link.val().toString(),
        data: { }
    }).fail(function() {

    }).always(function() {

    }).done(function( data ) {
        if(data){
            $('header#header .js-most-viewed-posts-box').html(data);
            //console.log(data);
        }
    });

});
