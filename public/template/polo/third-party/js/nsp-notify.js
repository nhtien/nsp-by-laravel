/*
* Khi thông báo được người dùng đánh dấu không nhắc lại sẽ tự động ẩn thông báo đó đi
*
* */

$( document ).ready(function() {
    /*
    * Hide notifications when cookies exist
    * */
    var elements = $('.js-nsp-notify');
    $.each(elements, function( index, value ) {
        var cookieName  = $(value).data('cookie-name');
        var cookieValue = $.cookie(cookieName);
        if (cookieValue == 'true'){
            $('div#' + cookieName).css('display', 'none'); // hidden notify
            //$('.js-nsp-notify-inactive').css('transform', 'translate3d(0px, -200px, 0px)');
        }
    });


    /*
    * Create cookie when click to "no repeat" button
    * */
    $('.js-nsp-modal-confirm ').click(function () {
        var cookieName = $(this).data('cookie-name');
        var cookieExpireValue = $(this).data('expire');
        // set cookie
        $.cookie(cookieName , 'true', { expires : cookieExpireValue, path: '/' });
        $('div#' + cookieName).css('display', 'none'); // hidden notify
    });
});