/*Khi click "Xem thêm sản phẩm" thì tab "Thông tin đặt hàng" sẽ được mở*/
function openOrderInfo() {
    /*active tab*/
    $('.js-myTab3 a.nav-link').removeClass('show active');
    $('.js-myTab3 a.js-order-info').addClass('show active');

    /*active content tab*/
    $('.js-myTabContent3 div.tab-pane').removeClass('show active');
    $('.js-myTabContent3 div.js-order-info-content').addClass('show active');

}
