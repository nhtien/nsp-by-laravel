/*
* Xử lý tất cả các xự kiện khi product popup được mở.
* Như lưu cookie, search, submit
*
* */

/******************* REMOVE SELECTED PRODUCT ITEM AND PRODUCT COOKIE ****************/
function removeSelectedProductItem() {
    $('.js-remove-selected-product-item').click(function(event){
        var productID 	= $(this).data('id');
        var productType = $(this).data('type');

        /* remove cookie*/
        $.removeCookie('request_quote_product_id_' + productID + '_' + productType, { path: '/' });

        /* remove quote info*/
        $('.js-selected-product-item-' + productID + '-' + productType).remove();
    });
}

/********* REMOVE ALL SELECTED PRODUCT AND COOKIE PRODUCT ******/
function removeAllSelectedProduct(){
    $('.js-remove-all-selected-product-items').click(function(event){
        var quoteCookies = getCookieKey('request_quote_product_id_', 'cookie-start-with');

        $.each(quoteCookies, function( index, value ) {
            $.removeCookie(value, { path: '/' });
            $('.js_' + value).remove();
        });
    });
}

/************ GET ELEMENTS ***************/
function getElementForm(){
    var customerNameElement = $('.js-request-quote-form input[name=customer-name]');
    var customerTypeElement = $('.js-request-quote-form select[name=customer-type] option:selected');
    var companyElement      = $('.js-request-quote-form input[name=company]');
    var emailElement 		= $('.js-request-quote-form input[name=email]');
    var phoneNumberElement  = $('.js-request-quote-form input[name=phone-number]');
    var areaElement  		= $('.js-request-quote-form input[name=area]:checked');

    var obj 	= [];
    obj.area  	= areaElement;
    obj.phone_number 	= phoneNumberElement;
    obj.email 			= emailElement;
    obj.company 		= companyElement;
    obj.customer_type 	= customerTypeElement;
    obj.customer_name 	= customerNameElement;

    return obj;
}


/*********** CLEAR VALUES AND COOKIE OF FORM ***********/
$('.js-clear-form').click(function(event){
    var elements = getElementForm();

    elements.area.val('');
    elements.customer_type.val('');
    elements.customer_name.val('');
    elements.company.val('');
    elements.email.val('');
    elements.phone_number.val('');

    /********** clear cookie *********/
    $.removeCookie('request_quote_area', { path: '/' });
    $.removeCookie('request_quote_customer_type', { path: '/' });
    $.removeCookie('request_quote_customer_name', { path: '/' });
    $.removeCookie('request_quote_company', { path: '/' });
    $.removeCookie('request_quote_email', { path: '/' });
    $.removeCookie('request_quote_phone_number', { path: '/' });
});

/**************** UPDATE COOKIE AND CLOSE POPUP *************/
$('.js-save-form').click(function(event){
    var cookieName 		= 'request_quote_';
    var cookieExpire 	= 1;
    var elements 		= getElementForm();


    $.cookie(cookieName + 'area' , elements.area.val(), { expires : cookieExpire , path: '/' });
    $.cookie(cookieName + 'customer_type' , elements.customer_type.val(), { expires : cookieExpire , path: '/' });
    $.cookie(cookieName + 'customer_name' , elements.customer_name.val(), { expires : cookieExpire , path: '/' });
    $.cookie(cookieName + 'company' , elements.company.val(), { expires : cookieExpire , path: '/' });
    $.cookie(cookieName + 'email' , elements.email.val(), { expires : cookieExpire , path: '/' });
    $.cookie(cookieName + 'phone_number' , elements.phone_number.val(), { expires : cookieExpire , path: '/' });

    /*********** UPDATE PRODUCT COOKIE ***********/
    var values = $( "form" ).serializeArray();
    var productIdsObj  = [];
    var productTypeObj = [];
    var qtyObj 	    = [];
    $.each(values, function( index, val ) {
        if(val.name == 'product-id[]') productIdsObj.push(val.value);
        else if(val.name == 'product-type[]') productTypeObj.push(val.value);
        else if(val.name == 'qty[]') qtyObj.push(val.value);
    });

    $.each(productIdsObj, function( index, val ) {
        var number 		= (qtyObj[index] > 0) ? qtyObj[index] : 1 ;
        var productType = ( productTypeObj[index] == 'main' || productTypeObj[index] == 'child') ? productTypeObj[index] : 'child';
        var cookieVal   = val + '___hcm___end_user___' + number + '___' + productType;

        $.cookie('request_quote_product_id_' + val + '_' + productType, cookieVal, { expires : cookieExpire , path: '/' });

    });


    $('.mfp-ready').trigger('click');
});



/*************** SUBMIT REQUEST QUOTE ****************/
$('.js-request-quote-form .js-request-quote--submit').click(function(event){
    var values = $( "form" ).serializeArray();

    /***** elements ******/
    var submitSendingElement 	 = $('.js-request-quote--submit-sending');
    var submitSuccessElement 	 = $('.js-request-quote--submit-success')
    var errorMgsElement 	 	 = $('.js-error');
    var productNotFoundElement 	 = $('.js-product-not-found')

    /***************/
    submitSendingElement.css('display', 'block');

    $.ajax({
        method: 'POST',
        url: 'https://zend.nsp.com.vn/vi/nsp/product/request-quote-submit',
        data: { info: JSON.stringify(values)}
    })
        .fail(function() {})
        .always(function() {})
        .done(function( data ) {
            submitSendingElement.css('display', 'none');

            if(data.status == 'product-not-found')
                productNotFoundElement.show().delay(5000).hide(1000);
            else if(data.status == 'element-error'){
                errorMgsElement.addClass('nsp-color').css('font-weight', 'bold');
            }else if(data.status == 'validate-successful'){
                submitSuccessElement.show().delay(5000).hide(1000);

                /******** CLEAR ALL PRODUCT COOKIE **********/
                $('.js-remove-all-selected-product-items').trigger('click');
                /********** CLEAR VALUE OF FORM **********/
                $('.js-clear-form').trigger('click');
            }

        });

});