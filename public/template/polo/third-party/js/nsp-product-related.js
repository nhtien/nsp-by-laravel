/*
* tự động hiển thị sp liên quan
* */

$.ajax({
    method: 'POST',
    url: urlProductRelated,
    data: { product_not_id: productID, tag_ids: tagIDS}
}).fail(function() {

}).always(function() {

}).done(function( data ) {
    $('.js-product-related').html(data);
});
