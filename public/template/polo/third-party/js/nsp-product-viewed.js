/*
* Tự động hiển thị sp đã xem
*
* */

$(document).ready(function(){
    //$.cookie('product_detail_id_' + productID , date, { expires : lifetimeProductCookie , path: '/' });
    var productIDS  = [];

    document.cookie.split(/; */).forEach(function(cookieraw){
        /*var match = cookieraw.match(new RegExp('(^| )product_detail_id_17=([^;]+)'));
        https://stackoverflow.com/questions/10730362/get-cookie-by-name*/

        var match = cookieraw.match(new RegExp('(^| )product_detail_id'));
        if(match){
            var cookieVal    = match.input.replace('product_detail_id_', '');
            var cookieDetail = cookieVal.split('=');
            productIDS.push(cookieDetail[0]);
        }
    });
    // Neu san pham da xem ton tại thì lấy nó ra
    if(productIDS){
        $.ajax({
            method: 'POST',
            url: urlProductViewed,
            data: { product_ids: productIDS.join(), product_not_id: productID}
        }).fail(function() {

        }).always(function() {

        }).done(function( data ) {
            if(data){
                $('.js-product-viewed').html(data);
                $('.js-product-viewed').css('display', 'block');
            }
        });
    }
});
