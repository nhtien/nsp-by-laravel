/*
* Kiểm tra dữ liệu khi người dùng đăng ký nhận báo giá
* 
*
* */

$('.js-price-list__submit').click(function() {
    var sendingElement = $('.js-price-list__mgs-sending');
    var successElement = $('.js-price-list__mgs-success');
    var failElement    = $('.js-price-list__mgs-fail');
    var emailElement   = $('.js-price-list__email');
    var registerPriceListUrlElement   = $('.js-register-price-list-url');

    /*Không có phép click submit form nhiều lần. Khi chưa nhận dc phản hồi từ máy chủ*/
    if($(this).hasClass('js-price-list__submit-disable') == true) return false;
    $(this).addClass('js-price-list__submit-disable');

    sendingElement.css('display', 'block');
    var email = emailElement.val();
    $.ajax({
        method: 'POST',
        url: registerPriceListUrlElement.val().toString(),
        data: {  email: email}
    }).fail(function() {

    }).always(function() {
        //sendingElement.css('display', 'inline-block');
    }).done(function( data ) {
        $('.js-price-list__submit').removeClass('js-price-list__submit-disable');
        sendingElement.delay(700).fadeOut(300);
        if(data.status == 'email-is-correct'){
            emailElement.val(' ');
            successElement.delay(1000).fadeIn(300);
            successElement.delay(5000).fadeOut(300);
        }else{
            failElement.delay(1000).fadeIn(300);
            failElement.delay(5000).fadeOut(300);
        }
    });
});