// https://stackoverflow.com/questions/9642205/how-to-force-a-script-reload-and-re-execute

var jqueryJS   = $('script[id="polo-jquery"]');
var jquerySrc  = jqueryJS.attr('src');

var pluginsJS   = $('script[id="polo-plugins"]');
var pluginsSrc  = pluginsJS.attr('src');

var functionsJS = $('script[id="polo-functions"]');
var functionsSrc= functionsJS.attr('src');

pluginsJS.remove();
jqueryJS.remove();
functionsJS.remove();

$('<script type="text/javascript">').attr('src', jquerySrc).appendTo('head');
$('<script type="text/javascript">').attr('src', pluginsSrc).appendTo('head');
$('<script type="text/javascript">').attr('src', functionsSrc).appendTo('head');
