/*
* Lưu sp khi người dùng yêu cầu báo giá 1 hay nhiều sp
*
* */

$('.js-request-quote--submit').click(function(event){
    /********** Elements **********/
    var productChildrenElement 	= $('.js-product-children:radio:checked');
    var productChildrenNotFound = $('.js-not-found-product-child--error');
    var customerElement  		= $('.js-customer');
    var qtyElement  	 		= $('.js-qty');
    var areaElement      		= $('.js-area[name="area"]:checked');
    var submitSendingElement = $('.js-request-quote--submit-sending');
    var submitErrorElement   = $('.js-request-quote--submit-error');
    var requestQuoteElement  = $('.js-request-quote--submit');

    var currentUrlElement    = $('input.js-request-quote-url');
    var productIDElement     = $('input.js-product-id');

    /********** Values ***********/
    var productChildrenObj = [];
    var productChildrenVal = '';
    productChildrenElement.each(function(i){
        productChildrenObj[i] = $(this).val();
    });
    productChildrenVal = productChildrenObj.join('_');

    var currentUrlVal  = currentUrlElement.val();
    var productIdlVal  = productIDElement.val();
    var customerVal    = customerElement.val();
    var qtyVal         = qtyElement.val();
    var areaVal        = areaElement.val();

    var cookieExpire   = 1; /* thoi gian ton tai cua cookie la 1 ngay */


    /************** Flags ************/
    var flagOpenRequestQuote = true;

    /***************/
    submitSendingElement.css('display', 'block');

    $.ajax({
        method: 'POST',
        url: currentUrlVal,
        data: {customer	: customerVal, qty: qtyVal, area: areaVal, product_children: productChildrenVal}
    }).fail(function() {

    })
    .always(function() {

    }).done(function( data ) {
        submitSendingElement.css('display', 'none');
        if(data.status == 'product-exist'){
            /*
            * 1. Nếu product không có sp con lưu cookie sp cha
            * 2. Product có sp con => lưu cookie của những sp con
            * */
            if(data.type == 'product-no-product-children'){
                /****** cookie number ******/
                var cookieNumber = getCookieLenght('request_quote_product_id', 'cookie-start-with');
                /******* set cookie ******/
                var setCookieVal = productIdlVal + '___' + data.area + '___' + data.customer + '___' + data.qty + '___main';
                /******* cookie name ******/
                var setCookieName = 'request_quote_product_id_' + productIdlVal + '_main';

                if(cookieNumber < 20)
                    $.cookie(setCookieName , setCookieVal, { expires : cookieExpire , path: '/' });
                else if ( cookieNumber >= 20 && $.cookie(setCookieName) )
                    $.cookie(setCookieName , setCookieVal, { expires : cookieExpire , path: '/' });

            }else if(data.type == 'product-have-product-children'){
                /*** Thiet lap cookie cho sp con *******/
                $.each( data.product_children_choosed, function( i, val ) {
                    /****** cookie number ******/
                    var cookieNumber = getCookieLenght('request_quote_product_id', 'cookie-start-with');
                    /******* set cookie ******/
                    var setCookieVal = val + '___' + data.area + '___' + data.customer + '___' + data.qty + '___child';
                    /******* cookie name ******/
                    var setCookieName = 'request_quote_product_id_' + val + '_child';

                    if(cookieNumber < 20)
                        $.cookie(setCookieName , setCookieVal, { expires : cookieExpire , path: '/' });
                    else if (cookieNumber >= 20 && $.cookie(setCookieName))
                        $.cookie(setCookieName , setCookieVal, { expires : cookieExpire , path: '/' });
                });

            }else if(data.type == 'product-not-found-product-children'){
                flagOpenRequestQuote = false;
                productChildrenNotFound.show(0).delay(5000).hide(1000);
            }

            /******* get cookies ******/
            var cookiesVal   = getCookieValue('request_quote_product_id', 'cookie-start-with');
            var areaCookieVal           = ($.cookie('request_quote_area') ) ? $.cookie("request_quote_area") : '';
            var customerTypeCookieVal   = ($.cookie('request_quote_customer_type') ) ? $.cookie("request_quote_customer_type") : '';
            var customerNameCookieVal   = ($.cookie('request_quote_customer_name') ) ? $.cookie("request_quote_customer_name") : '';
            var companyCookieVal        = ($.cookie('request_quote_company') ) ? $.cookie("request_quote_company") : '';
            var emailCookieVal          = ($.cookie('request_quote_email') ) ? $.cookie("request_quote_email") : '';
            var phoneNumberCookieVal    = ($.cookie('request_quote_phone_number') ) ? $.cookie("request_quote_phone_number") : '';
            var urlOpenPopup = requestQuoteElement.attr('href') + '?info=' + cookiesVal + '&area=' + areaCookieVal +
                '&customer_type=' + customerTypeCookieVal + '&customer_name=' + customerNameCookieVal + '&company=' + companyCookieVal +
                '&email=' +emailCookieVal + '&phone_number=' + phoneNumberCookieVal;

            /****** open request quote ****/
            if(flagOpenRequestQuote == true)
                openRequestQuotePopup(cookiesVal, urlOpenPopup.toString());

        }
        else if(data.status == 'element-error'){
            if(data.area == 'area-error') $('.js-area--error').show(0).delay(5000).hide(1000);
            if(data.customer == 'customer-error') $('.js-customer--error').show(0).delay(5000).hide(1000);
            if(data.qty == 'qty-error') $('.js-qty--error').show(0).delay(5000).hide(1000);
        }
        else{
            submitErrorElement.show(0).delay(5000).hide(1000);
        }
    });


    /**************** OPEN POPUP *****************/
/*
    function openRequestQuotePopup(value, url){
        $.ajax({
            method: 'POST',
            url: url,
            data: { productItems: value.toString()}
        })
        .fail(function() {
        })
        .always(function() {})
        .done(function( data ) {
            $('.js-lightbox-open').attr('href', url);
            $('.js-lightbox-open').trigger('click');
        });
    }
*/

    return false;

});
