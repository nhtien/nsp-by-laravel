require('./bootstrap');
/*document.addEventListener("DOMContentLoaded", function(event) {
    setTimeout(function () {
        require('../../public/template/polo/js/functions');
    }, 500);
});*/

import {createApp} from 'vue';
import Notifications from '@kyvg/vue3-notification'
import router from './router';
import components from "./components/index.js";
import store from './store';
import kits from './plugins/kits';

// my plugins
import RepositoryFactory from "./repository/RepositoryFactory";

createApp({
    components: components
})
    .use(kits)
    .use(store)
    .use(RepositoryFactory)
    .use(router)
    .use(Notifications)
    .mount('#app');
