import ProductByCate from './products/ProductByCate';
import HomepageIndex from './homepage/HomepageIndex';
import MenuHeader from './menus/MenuHeader';
import MenuFooter from './menus/MenuFooter';
import Notification from './notify/Notification';
const components = {
    HomepageIndex,
    MenuHeader,
    ProductByCate,
    MenuFooter,
    Notification
};
export default components;
