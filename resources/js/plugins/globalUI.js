import BaseLoading from "../components/UI/BaseLoading.vue";

const globalUI = {
    install(Vue) {
        Vue.component('base-loading', BaseLoading);
    }
};

export default globalUI;
