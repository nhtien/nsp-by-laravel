import globalUI from "./globalUI";

export default {
    install(Vue) {
        Vue.use(globalUI);
    }
};
