import Repository from './Repository';
const resource = '/menus';

export default {
    getHeaderMenus() {
        return Repository.get(`${resource}`);
    },
}
