import Repository from './Repository';
const resource = '/posts';

export default {
    findsByCategory(catId) {
        return Repository.get(`${resource}/by-category/${catId}`);
    },
}
