import Repository from './Repository';
const resource = '/product-brands';

export default {
    getAll() {
        return Repository.get(`${resource}`);
    },
}
