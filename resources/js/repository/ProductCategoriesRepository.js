import Repository from './Repository';
const resource = '/product-categories';

export default {
    getHeaderMenus() {
        return Repository.get(`${resource}/header-menu`);
    },
    findCategoriesByLevel(level){
        return Repository.get(`${resource}/level/${level}`);
    },
    findChildrenCategoriesByBranch(branchId){
        return Repository.get(`${resource}/children-branch/${branchId}`);
    },
    findCategoriesByBranch(branchId){
        return Repository.get(`${resource}/branch/${branchId}`);
    },
    find(id) {
        return Repository.get(`${resource}/${id}`);
    },
    findFirstAncestor(id){
        return Repository.get(`${resource}/first-ancestor/${id}`);
    }
}
