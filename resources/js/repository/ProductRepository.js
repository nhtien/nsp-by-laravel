import Repository from './Repository';
const resource = '/products';

export default {
    findsByBrand(brandId) {
        return Repository.get(`${resource}/by-brand/${brandId}`);
    },
    findsBySolution(solutionId) {
        return Repository.get(`${resource}/by-solution/${solutionId}`);
    },
    findsByBranch(catId) {
        return Repository.get(`${resource}/by-branch/${catId}`);
    },
    findsByTag(tagId) {
        return Repository.get(`${resource}/by-tag/${tagId}`);
    },
    find(prodId) {
        return Repository.get(`${resource}/${prodId}`);
    },
    findsRelated(prodId) {
        return Repository.get(`${resource}/related/${prodId}`);
    },
}
