import Repository from './Repository';
const resource = '/product-tags';

export default {
    findsByBranch(branchId) {
        return Repository.get(`${resource}/by-branch/${branchId}`);
    },
}
