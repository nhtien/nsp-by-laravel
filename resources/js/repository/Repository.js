import axios from 'axios';

const baseDomain = '';
const baseUrl = `${baseDomain}/api`;

// https://topdev.vn/blog/cach-call-api-trong-vuejs/
/*axios.defaults.baseURL = baseUrl;
axios.defaults.headers.post['Content-Type'] = 'application/json';*/
export default axios.create({
    baseURL: baseUrl,
    headers:{
        'Content-Type': 'application/json'
    }
});
