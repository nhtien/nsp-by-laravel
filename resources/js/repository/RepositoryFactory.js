import MenusRepository from './MenusRepository';
import ProductCategoriesRepository from './ProductCategoriesRepository';
import ProductBrandsRepository from './ProductBrandsRepository';
import SlidersRepository from './SlidersRepository';
import ProductRepository from './ProductRepository';
import ProductTagsRepository from './ProductTagsRepository';
import PostRepository from './PostRepository';
import SolutionRepository from './SolutionRepository';

const repositories = {
    sliders: SlidersRepository,
    menus: MenusRepository,
    products: ProductRepository,
    'product-categories': ProductCategoriesRepository,
    'product-brands': ProductBrandsRepository,
    'product-tags': ProductTagsRepository,
    'posts': PostRepository,
    'solutions': SolutionRepository,
}

export default {
    install (Vue, options) {
        Vue.mixin({
            methods: {
                $_getRepositoryFactory (name) {
                    return repositories[name];
                },
            }
        })
    }
}
