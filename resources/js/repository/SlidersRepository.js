import Repository from './Repository';
const resource = '/sliders';

export default {
    getAll() {
        return Repository.get(`${resource}`);
    },
}
