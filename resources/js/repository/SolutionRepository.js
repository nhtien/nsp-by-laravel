import Repository from './Repository';
const resource = '/solutions';

export default {
    getAll() {
        return Repository.get(`${resource}`);
    },
}
