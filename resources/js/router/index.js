import { createRouter, createWebHistory } from 'vue-router'
import ProductByCate from '../components/products/ProductByCate';
import ProductDetail from '../components/products/ProductDetail';
import ProductByTag from '../components/products/ProductByTag';
import SolutionDetail from '../components/solution/SolutionDetail.vue';
import BrandList from '../components/brand/BrandList.vue';
import News from '../components/post/News';
const routes = [
    {
        path: '/nhom-san-pham/:slug-:id(\\d+).html',
        name: 'product.by-cat',
        component: ProductByCate
    },
    {
        path: '/chi-tiet-san-pham/:slug-:id(\\d+).html',
        name: 'product.detail',
        component: ProductDetail
    },
    {
        path: '/giai-phap/:slug-:id(\\d+).html',
        name: 'solution.detail',
        component: SolutionDetail
    },
    {
        path: '/thuong-hieu/san-pham/:slug-:id(\\d+).html',
        name: 'brand.list',
        component: BrandList
    },
    {
        path: '/bai-viet/:slug.html',
        name: 'post.news',
        component: News
    },
    {
        path: '/tags/:slug-:hashId.html',
        name: 'product.by-tag',
        component: ProductByTag
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router
