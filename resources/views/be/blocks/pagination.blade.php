@php
/**
 * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
 */
if ($paginator->count() < 1) {
    return false;
}
$from = $paginator->currentPage() == 1 ? 1 :  ($paginator->currentPage() - 1) * $paginator->perPage() + 1;
$to   = $from + count($paginator->items());
$to   = $to - 1;

// http query
$allParams = request()->all();
unset($allParams['page']);
$httpQuery = url()->current() .'?'. http_build_query($allParams);

$previous = $httpQuery . '&page='. ($paginator->currentPage() - 1 > 0 ? $paginator->currentPage() - 1 : 1);
$next = $httpQuery . '&page='. ($paginator->lastPage() == $paginator->currentPage() ? $paginator->lastPage() : $paginator->currentPage() + 1);
@endphp

@if ($paginator->hasPages())
<section id="options" style="padding: 15px; background-color: #fff">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing {{$from}} to {{$to}} of {{$paginator->total()}} entries</div>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="dataTables_paginate paging_simple_numbers" style="float: right">
                <ul class="pagination">
                    <li class="paginate_button page-item previous">
                        <a href="{{$previous}}" class="page-link">Previous</a>
                    </li>
                    @foreach ($elements as $element)
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="paginate_button page-item active"><a href="{{$httpQuery . '&page=' . $page}}" class="page-link">{{$page}}</a></li>
                                @elseif (($page == $paginator->currentPage() + 1 || $page == $paginator->currentPage() + 2) || $page == $paginator->lastPage())
                                    <li class="paginate_button page-item"><a href="{{$httpQuery . '&page=' . $page}}" class="page-link">{{$page}}</a></li>
                                @elseif ($page == $paginator->lastPage() - 1)
                                    <li class="paginate_button page-item disabled"><a href="#" class="page-link">...</a></li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <li class="paginate_button page-item next">
                        <a href="{{$next}}" class="page-link">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
@endif
