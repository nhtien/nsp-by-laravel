@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.config.index')}}">
    <x-BE.Searching.SearchBox placeholder="Tất cả"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[usr_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[cfg_status]" id="filter[cfg_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($confStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["cfg_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.config.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
