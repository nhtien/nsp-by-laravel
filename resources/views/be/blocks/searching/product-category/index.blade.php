@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.productcategory.index')}}">
    <x-BE.Searching.SearchBox placeholder="Tên nhóm, ID"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[prod_cat_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_cat_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($catStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["prod_cat_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[by_branch_id]">Danh mục</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[by_branch_id]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($catsLevel1 as $key => $item)
                    <option value="{{$item->prcat_id}}" @selected($item->prcat_id == @$filters["by_branch_id"]) >{{notation_by_level($item->level)}} {{$item->name}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.productcategory.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
