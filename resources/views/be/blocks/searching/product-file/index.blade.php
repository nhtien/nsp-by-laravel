@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.productfile.index')}}">
    <x-BE.Searching.SearchBox placeholder="Tiêu đề, mô tả"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-4">
        <label for="filter[prod_file_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_file_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($fileStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["prod_file_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.productfile.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
