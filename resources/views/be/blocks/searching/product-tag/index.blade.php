@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.producttag.index')}}">
    <x-BE.Searching.SearchBox placeholder="Tên"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[prod_tag_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_tag_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($tagStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["prod_tag_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.producttag.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
