@php
    $filters = request()->get('filter', []);
@endphp
<x-BE.Searching title="Tìm kiếm" form-action="{{route('be.product.index')}}">
    <x-BE.Searching.SearchBox placeholder="Partnumber, tên sp, mô tả ngắn"></x-BE.Searching.SearchBox>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[prod_brd_id]">Hãng sp</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_brd_id]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($brands as $key => $item)
                    <option value="{{$item->prodbrd_id}}" @selected($item->prodbrd_id == @$filters["prod_brd_id"]) >{{$item->name}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[prod_cat_id]">Nhóm sp</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_cat_id]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($cats as $key => $item)
                    <option value="{{$item->prcat_id}}" @selected($item->prcat_id == @$filters["prod_cat_id"]) >{{notation_by_level($item->level)}} {{$item->name}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <div class="col-12 col-sm-6 col-lg-3">
        <label for="filter[prod_status]">Trạng thái</label>
        <fieldset class="form-group">
            <select class="form-control" name="filter[prod_status]">
                <option value="">-- Hiển thị tất cả --</option>
                @foreach($prodStatus as $key => $item)
                    <option value="{{$key}}" @selected($key == @$filters["prod_status"]) >{{$item}}</option>
                @endforeach
            </select>
        </fieldset>
    </div>
    <x-BE.Searching.ActionBox refresh-url="{{route('be.product.index')}}"></x-BE.Searching.ActionBox>
</x-BE.Searching>
