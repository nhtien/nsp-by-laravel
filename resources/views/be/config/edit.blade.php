@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Cấu hình [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.config.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.config.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.config.update')}}">
            @method('PUT')
            <input type="hidden" name="cfg_id" value="{{$config->conf_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="cfg_name" value="{{old('cfg_name') ?? $config->conf_name}}"/>
                    <x-BE.Elements.Input id="cfg_key" value="{{old('cfg_key') ?? $config->conf_key}}" disabled/>
                    <x-BE.Elements.Textarea content="{{old('cfg_value') ?? $config->conf_value}}" rows="4" id="cfg_value"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Textarea content="{{old('cfg_des') ?? $config->conf_description}}" rows="4" id="cfg_des"/>
                    <div class="form-group">
                        <label for="cfg_status">Trạng thái</label>
                        <select class="form-control" name="cfg_status" id="cfg_status">
                            @foreach($confStatus as $key => $item)
                                <option value="{{$key}}" @selected($config->conf_status == $key || $key == old('cfg_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cfg_type">Định dạng dữ liệu</label>
                        <select class="form-control" name="cfg_type" id="cfg_type">
                            @foreach($confType as $key => $item)
                                <option value="{{$key}}" @selected($config->conf_type == $key || $key == old('cfg_type'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
