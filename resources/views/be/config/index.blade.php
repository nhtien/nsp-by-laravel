@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Cấu hình">
        <x-BE.Elements.Button type="create" href="{{route('be.config.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.config.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.config.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    @include('be.blocks.searching.config.index')
    <x-BE.Tables.Form>
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Tên</th>
                <th>Khóa</th>
                <th>Giá trị</th>
                <th>Mô tả</th>
                <th>Trạng thái</th>
                <th>Kiểu dữ liệu</th>
            </tr>
        </thead>
        <tbody>
            @foreach($configs as $config)
                <tr>
                    <x-BE.Tables.CheckboxID>
                        {{$config->conf_id}}
                    </x-BE.Tables.CheckboxID>
                    <td>{{$config->conf_id}}</td>
                    <td><a class="underline-dashed" href="{{route('be.config.edit', ['id' => $config->conf_id])}}">{{$config->conf_name}}</a></td>
                    <td>{{$config->conf_key}}</td>
                    <td>{{$config->conf_value}}</td>
                    <td>{{$config->conf_description}}</td>
                    <x-BE.Tables.Status status="{{$config->conf_status}}"></x-BE.Tables.Status>
                    <td>{{$config->conf_type}}</td>
                </tr>
            @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $configs->links('be.blocks.pagination') }}
@endsection
