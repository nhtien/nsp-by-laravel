@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Liên Hệ">
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Chủ đề</th>
                <th>Họ và Tên</th>
                <th>Công ty</th>
                {{--<th>Địa chỉ công ty</th>--}}
                <th>SDT</th>
                <th>Email</th>
                {{--<th>Lời nhắn</th>--}}
                <th>Trạng thái</th>
                <th>Ngày tạo</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contacts as $contact)
                <tr>
                    <x-BE.Tables.CheckboxID>
                        {{$contact->id}}
                    </x-BE.Tables.CheckboxID>
                    <td>{{$contact->id}}</td>
                    <td><a class="underline-dashed" href="{{route('be.contact.view', ['id' => $contact->id])}}">{{$contact->subject}}</a></td>
                    <td>{{$contact->full_name}}</td>
                    <td>{{$contact->company_name}}</td>
                    {{--<td>{{$contact->company_address}}</td>--}}
                    <td>{{$contact->phone_number}}</td>
                    <td>{{$contact->email}}</td>
                    {{--<td>{{$contact->message}}</td>--}}
                    @if($contact->is_new === 1)
                        <td style="color: #0ac7e6; font-weight: bold">Mới</td>
                    @else
                        <td style="color: #0b0b0b; font-weight: bold">Đã xem</td>
                    @endif

                    <td>{{$contact->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $contacts->links('be.blocks.pagination') }}
@endsection
