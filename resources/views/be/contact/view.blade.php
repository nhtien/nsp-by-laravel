@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Chi tiết Liên hệ">
        <x-BE.Elements.Button type="go-back" href="{{route('be.contact.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <section>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Chủ đề</th>
                                <td>{{$contact->subject}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Lời nhắn</th>
                                <td>{{$contact->message}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Công ty</th>
                                <td>{{$contact->company_name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Địa chỉ</th>
                                <td>{{$contact->company_address}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Họ và Tên</th>
                                <td>{{$contact->full_name}}</td>
                            </tr>
                            <tr>
                                <th scope="row">Số điện thoại</th>
                                <td><a href="tel:{{$contact->phone_number}}" style="color: rgb(0, 123, 255);">{{$contact->phone_number}} </a></td>
                            </tr>
                            <tr>
                                <th scope="row">Email</th>
                                <td><a href="mailto:{{$contact->email}}" style="color: rgb(0, 123, 255);">{{$contact->email}}</a></td>
                            </tr>
                            <tr>
                                <th scope="row">Ngày tạo</th>
                                <td>{{$contact->created_at}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
