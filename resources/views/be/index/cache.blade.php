@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách các loại cache">
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>STT</th>
            <th>Mô tả</th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <x-BE.Tables.CheckboxID>1</x-BE.Tables.CheckboxID>
                <td>1</td>
                <td>Route cache cleared</td>
                <td><a class="underline-dashed" href="/systems/route-clear">Xóa cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>2</x-BE.Tables.CheckboxID>
                <td>2</td>
                <td>View cache cleared</td>
                <td><a class="underline-dashed" href="/systems/view-clear">Xóa cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>3</x-BE.Tables.CheckboxID>
                <td>3</td>
                <td>Cache facade value cleared</td>
                <td><a class="underline-dashed" href="/systems/clear-cache">Xóa cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>4</x-BE.Tables.CheckboxID>
                <td>4</td>
                <td>Configuration cache cleared!</td>
                <td><a class="underline-dashed" href="/systems/config-clear">Xóa cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>5</x-BE.Tables.CheckboxID>
                <td>5</td>
                <td>Optimize cache cleared</td>
                <td><a class="underline-dashed" href="/systems/optimize-clear">Xóa cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>6</x-BE.Tables.CheckboxID>
                <td>6</td>
                <td>Config cached</td>
                <td><a class="underline-dashed" href="/systems/config-cache">Lưu cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>7</x-BE.Tables.CheckboxID>
                <td>7</td>
                <td>Route cached</td>
                <td><a class="underline-dashed" href="/systems/route-cache">Lưu cache</a></td>
            </tr>
            <tr>
                <x-BE.Tables.CheckboxID>8</x-BE.Tables.CheckboxID>
                <td>8</td>
                <td>Reoptimized class loader</td>
                <td><a class="underline-dashed" href="/systems/optimize-cache">Lưu cache</a></td>
            </tr>
        </tbody>
    </x-BE.Tables.Form>
@endsection
