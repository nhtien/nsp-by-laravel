@extends('be.layouts.main')
@section('content')
    <section id="dashboard-ecommerce">
        <div class="row">
            <div class="col-xl-12 col-12 dashboard-users">
                <div class="row  ">
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-4 col-12 dashboard-users-success">
                                <div class="card text-center">
                                    <div class="card-body py-1">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                            <i class="bx bxl-product-hunt font-medium-5"></i>
                                        </div>
                                        <div class="text-muted line-ellipsis">Sản phẩm</div>
                                        <h3 class="mb-0">{{number_format($totalProducts)}}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12 dashboard-users-danger">
                                <div class="card text-center">
                                    <div class="card-body py-1">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                            <i class="bx bx-user font-medium-5"></i>
                                        </div>
                                        <div class="text-muted line-ellipsis">Nhân viên</div>
                                        <h3 class="mb-0">{{number_format($totalUsers)}}</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-12 dashboard-users-danger">
                                <div class="card text-center">
                                    <div class="card-body py-1">
                                        <div class="badge-circle badge-circle-lg badge-circle-light-success mx-auto mb-50">
                                            <i class="bx bxs-news font-medium-5"></i>
                                        </div>
                                        <div class="text-muted line-ellipsis">Bài viết</div>
                                        <h3 class="mb-0">{{number_format($totalPosts)}}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
