@section('css_tag')
@php
    $queryString = '?t=' . config('constants.cache_css');
@endphp
<!-- BEGIN: Vendor CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/vendors.min.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/charts/apexcharts.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/pickers/daterange/daterangepicker.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/pickers/pickadate/pickadate.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/vendors/css/forms/select/select2.min.css' . $queryString)}}">
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/bootstrap.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/bootstrap-extended.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/colors.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/components.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/themes/dark-layout.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/themes/semi-dark-layout.css' . $queryString)}}">
<!-- END: Theme CSS-->

<!-- BEGIN: Page CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/core/menu/menu-types/vertical-menu.css' . $queryString)}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/plugins/file-uploaders/dropzone.css' . $queryString)}}">
<!-- END: Page CSS-->

<!-- BEGIN: Custom CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/my-style.css' . $queryString)}}">
<!-- END: Custom CSS-->

<!-- BEGIN: overlay CSS-->
<link rel="stylesheet" type="text/css" href="{{asset('public/be/app-assets/css/overlay.css' . $queryString)}}">
<!-- END: overlay CSS-->

{{--https://trentrichardson.com/examples/timepicker/--}}
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.css" >
@show
