@section('javascript_tag')
@php
    $queryString = '?t=' . config('constants.cache_js');
@endphp
<!-- BEGIN: Vendor JS-->
<script src="{{asset('public/be/app-assets/vendors/js/vendors.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js' . $queryString)}}"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('public/be/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/charts/apexcharts.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/pickers/pickadate/picker.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/pickers/pickadate/picker.date.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/pickers/pickadate/picker.time.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/pickers/pickadate/legacy.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/extensions/moment.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/pickers/daterange/daterangepicker.js' . $queryString)}}"></script>

<script src="{{asset('public/be/app-assets/vendors/js/forms/select/select2.full.min.js' . $queryString)}}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{asset('public/be/app-assets/vendors/js/forms/repeater/jquery.repeater.min.js' . $queryString)}}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{asset('public/be/app-assets/js/core/app-menu.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/js/core/app.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/js/scripts/components.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/js/scripts/footer.js' . $queryString)}}"></script>
<!-- END: Theme JS-->
<script src="{{asset('public/be/app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js' . $queryString)}}"></script>
<!-- BEGIN: Page JS-->
<script src="{{asset('public/be/app-assets/js/scripts/forms/select/form-select2.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/vendors/js/file-uploaders/dropzone.min.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/js/scripts/extensions/dropzone.js' . $queryString)}}"></script>
<!-- END: Page JS-->
<script src="{{asset('public/be/app-assets/js/scripts/pages/table-extended.js' . $queryString)}}"></script>
<!-- BEGIN: Page JS-->
<script src="{{asset('public/be/app-assets/js/scripts/forms/form-repeater.js' . $queryString)}}"></script>
<!-- END: Page JS-->
<script src="{{asset('public/be/app-assets/js/my.js' . $queryString)}}"></script>

{{--https://trentrichardson.com/examples/timepicker/--}}
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.6.3/jquery-ui-timepicker-addon.min.js"></script>

<script src="{{asset('public/ckeditor/ckeditor.js' . $queryString)}}"></script>
<script src="{{asset('public/be/app-assets/js/ckeditor.js' . $queryString)}}"></script>
<script>
    jQuery(function($) {
        $("#datepicker").datetimepicker({
            dateFormat: 'dd/mm/yy',
            timeFormat: 'HH:mm',
        });
    });

    $( document ).ready(function() {
        $(".js-overlay").click(function(){
            $("div.spanner").addClass("show");
            $("div.overlay").addClass("show");
        });

        $('img.js-lazy-loading').each(function( index ) {
            var dataSrc = $(this).data('src');
            if (dataSrc) {
                $(this).attr('src', dataSrc);
            }
        });
    });


    // open popup ckfinder
    $('.js-open-popup-ckfinder').click(function () {
        var popupWindowOptions = [
            'location=no',
            'menubar=no',
            'toolbar=yes',
            'dependent=yes',
            'minimizable=no',
            'modal=yes',
            'alwaysRaised=yes',
            'resizable=yes',
            'scrollbars=yes',
            'width=700',
            'height=500',
            'top=50',
            'left=100'
        ].join( ',' );
        var url = $(this).data("url");
        window.open( url, "_blank", popupWindowOptions);
    });
</script>
@show
