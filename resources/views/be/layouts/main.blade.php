<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>NSP</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.ico')}}" rel="shortcut icon">
    <link href="{{asset('/public/uploads/images/favicon/favicon_192x192.png')}}" rel="icon" sizes="192x192" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_96x96.png')}}" rel="icon" sizes="96x96" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.png')}}" rel="icon" sizes="48x48" type="image/png">
    @include('be.layouts.blocks.css')
</head>
<!-- END: Head-->
<!-- BEGIN: Body-->
<body style="position: relative" class="vertical-layout vertical-menu-modern boxicon-layout no-card-shadow 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    {{--overlay--}}
    <div class="overlay"></div>
    <div class="spanner">
        <div class="loader"></div>
        <p>Đang được xử lý, vui lòng đợi ...</p>
    </div>
    <form id="delete-form" action="" method="post">
        @csrf @method('DELETE')
    </form>
    @showMenuBackend($showMenuBackend)
        <x-BE.Menu></x-BE.Menu>
    @endif
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                @include('be.layouts.blocks.notify')
                @yield('content')
            </div>
        </div>
    </div>
    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix mb-0"><span class="float-left d-inline-block">2022 &copy; NSP</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="https://nsp.com.vn/" target="_blank">NSP</a></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->
    @include('be.layouts.blocks.javascript')
</body>
<!-- END: Body-->
</html>
