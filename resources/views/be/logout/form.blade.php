@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Đăng xuất tất cả thiết bị khác">
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{asset('/logout-other-devices.html')}}" method="post">
            <div class="row">
                <div class="col-md-6 offset-md-3 col-sm-12">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend"><span class="input-group-text">Nhập mật khẩu</span></div>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="align-right">
                        <button type="submit" class="btn btn-outline-primary mr-1 mb-1">
                            <i class="bx bxs-log-out-circle"></i> Xác nhận
                        </button>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
