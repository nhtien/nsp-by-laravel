@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Menu [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.menufrontend.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.menufrontend.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.menufrontend.update')}}">
            @method('PUT')
            <input type="hidden" name="menu_fe_id" value="{{$menu->id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="menu_fe_name" value="{{old('menu_fe_name') ?? $menu->name}}"/>
                    <x-BE.Elements.Input id="menu_fe_name_en" value="{{old('menu_fe_name_en') ?? $menu->name_en}}"/>
                    <x-BE.Elements.Input id="menu_fe_link" value="{{old('menu_fe_link') ?? $menu->link}}"/>
                    <x-BE.Elements.Input id="menu_fe_link_en" value="{{old('menu_fe_link_en') ?? $menu->link_en}}"/>
                    <x-BE.Elements.Input id="menu_fe_icon" value="{{old('menu_fe_icon') ?? $menu->icon}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="menu_fe_status">{{__('menu_fe_status')}}</label>
                        <select class="form-control" name="menu_fe_status" id="menu_fe_status">
                            @foreach($menuStatus as $key => $item)
                                <option value="{{$key}}" @selected($menu->status == $key || $key == old('menu_fe_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="menu_fe_special">{{__('menu_fe_special')}}</label>
                        <select class="form-control" name="menu_fe_special" id="menu_fe_special">
                            @foreach($menuSpecial as $key => $item)
                                <option value="{{$key}}" @selected($menu->special == $key || $key == old('menu_fe_special'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="menu_fe_parent">Thuộc menu (node)</label>
                        <select class="form-control" name="menu_fe_parent" id="menu_fe_parent">
                            @foreach($menus as $key => $item)
                                <option value="{{$item->id}}" @selected($item->id == old('menu_fe_parent') || $menu->parent == $item->id)>{{notation_by_level($item->level)}} {{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <x-BE.Node id="menu_fe_position"></x-BE.Node>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
