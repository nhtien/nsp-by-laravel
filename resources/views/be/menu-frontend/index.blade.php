@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Menu">
        <x-BE.Elements.Button type="create" href="{{route('be.menufrontend.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.menufrontend.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.menufrontend.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th>Tên</th>
            <th>Tên (EN)</th>
            <th>URL</th>
            <th>URL (EN)</th>
            <th>Loại URL</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($menus as $menu)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$menu->id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$menu->left}}</td>
                <td><a class="underline-dashed" href="{{route('be.menufrontend.edit', ['id' => $menu->id])}}"> {{notation_by_level($menu->level - 1)}} {{$menu->name}}</a></td>
                <td>@if($menu->name_en != ''){{notation_by_level($menu->level - 1)}} @endif {{$menu->name_en}}</td>
                <td>{{$menu->link}}</td>
                <td>{{$menu->link_en}}</td>
                <td>{{$menuSpecial[$menu->special]}}</td>
                <x-BE.Tables.Status status="{{$menu->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $menus->links('be.blocks.pagination') }}
@endsection
