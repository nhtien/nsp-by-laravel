@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Menu [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.menu.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.menu.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.menu.update')}}">
            @method('PUT')
            <input type="hidden" name="menu_id" value="{{$menu->menu_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="menu_name" value="{{old('menu_name') ?? $menu->name}}"/>
                    <x-BE.Elements.Input id="menu_controller"     value="{{old('menu_controller') ?? $menu->controller}}"/>
                    <x-BE.Elements.Input id="menu_action"     value="{{old('menu_action') ?? $menu->action}}"/>
                    <x-BE.Elements.Input id="menu_icon"     value="{{old('menu_icon') ?? $menu->icon}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="menu_parent" value="{{old('menu_parent') ?? $menu->parent}}"/>
                    <x-BE.Elements.Input id="menu_position"     value="{{old('menu_position') ?? $menu->position}}"/>
                    <div class="form-group">
                        <label for="menu_status">Trạng thái</label>
                        <select class="form-control" name="menu_status" id="menu_status">
                            @foreach($menuStatus as $key => $item)
                                <option value="{{$key}}" @selected($menu->status == $key || $key == old('menu_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
