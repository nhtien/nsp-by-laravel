@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Menu">
        <x-BE.Elements.Button type="create" href="{{route('be.menu.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.menu.edit')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên</th>
            <th>Controller</th>
            <th>Action</th>
            <th>Loại Menu</th>
            <th>Trạng thái</th>
            <th>Vị trí</th>
            <th>Icon</th>
            <th>ID của menu cha</th>
        </tr>
        </thead>
        <tbody>
        @foreach($menus as $menu)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$menu->menu_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$menu->menu_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.menu.edit', ['id' => $menu->menu_id])}}">@if($menu->parent > 1) {{'|--->'}} @endif{{$menu->name}}</a></td>
                <td>{{$menu->controller}}</td>
                <td>{{$menu->action}}</td>
                <td>{!! $menu->parent == 0 ? '<span class="text-success">CHA</span>' : '<span class="text-danger">CON</span>' !!}</td>
                <x-BE.Tables.Status status="{{$menu->status}}"></x-BE.Tables.Status>
                <td>{{$menu->position}}</td>
                <td><i class="{{$menu->icon}}"></i></td>
                <td>{{$menu->parent == 0 ? '' : $menu->parent}}</td>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $menus->links('be.blocks.pagination') }}
@endsection
