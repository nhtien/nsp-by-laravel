@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Log">
        <x-BE.Elements.Button type="detail" data-url="{{route('be.modellog.show')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Bảng</th>
            <th>Hành động</th>
            <th>Nội dung</th>
            <th>Nhân viên</th>
            <th>Ngày tạo</th>
        </tr>
        </thead>
        <tbody>
        @foreach($modelLogs as $modelLog)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$modelLog->id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$modelLog->id}}</td>
                <td><a class="underline-dashed" href="{{route('be.modellog.show', ['id' => $modelLog->id])}}">{{$modelLog->table_name}}</a></td>
                <td>{{$modelLog->type}}</td>
                <td>{{\Illuminate\Support\Str::limit($modelLog->getRawOriginal('content'), 100)}}</td>
                <td>{{optional($modelLog->user)->name}}</td>
                <td>{{$modelLog->created_at}}</td>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $modelLogs->links('be.blocks.pagination') }}
@endsection
