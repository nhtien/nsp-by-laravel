@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Log [Chi tiết]">
        <x-BE.Elements.Button type="go-back" href="{{route('be.modellog.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <div class="row">
            <div class="col-12 col-sm-6">
                <x-BE.Elements.Input id="Bảng" value="{{$modelLog->table_name}}" disabled/>
                <x-BE.Elements.Input id="Hành động" value="{{$modelLog->type}}" disabled/>
            </div>
            <div class="col-12 col-sm-6">
                <x-BE.Elements.Input id="Ngày tạo" value="{{$modelLog->created_at}}" disabled/>
                <x-BE.Elements.Input id="Nhân viên" value="{{optional($modelLog->user)->name}}" disabled/>
            </div>
            <div class="col-12 col-sm-12">
                <x-BE.Elements.Textarea content="{{collect($modelLog->content)->toJson()}}" id="Nội dung"/>
            </div>
        </div>
    </x-BE.Sections.Card>
@endsection
