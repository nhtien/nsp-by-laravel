@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm bài viết [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.postgroup.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.postgroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.postgroup.update')}}">
            @method('PUT')
            <input type="hidden" name="pst_grp_id" value="{{$postGroup->pgrp_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="pst_grp_name" value="{{old('pst_grp_name') ?? $postGroup->name}}"/>
                    <x-BE.Elements.Input id="pst_grp_name_en" value="{{old('pst_grp_name_en') ?? $postGroup->name_en}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="pst_grp_code" value="{{old('pst_grp_code') ?? $postGroup->code}}"/>
                    <div class="form-group">
                        <label for="pst_grp_status">{{__('pst_grp_status')}}</label>
                        <select class="form-control" name="pst_grp_status" id="pst_grp_status">
                            @foreach($groupStatus as $key => $item)
                                <option value="{{$key}}" @selected($postGroup->status == $key || $key == old('pst_grp_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
