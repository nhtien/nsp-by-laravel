@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm bài viết">
        <x-BE.Elements.Button type="create" href="{{route('be.postgroup.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.postgroup.edit')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên</th>
            <th>Tên (EN)</th>
            <th>Mã</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($postGroups as $postGroup)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$postGroup->pgrp_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$postGroup->pgrp_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.postgroup.edit', ['id' => $postGroup->pgrp_id])}}">{{$postGroup->name}}</a></td>
                <td>{{$postGroup->name_en}}</td>
                <td>{{$postGroup->code}}</td>
                <x-BE.Tables.Status status="{{$postGroup->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $postGroups->links('be.blocks.pagination') }}
@endsection
