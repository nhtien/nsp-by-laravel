@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Bài Viết [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.post.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.post.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Tab title="">
        <x-BE.Elements.Form action="{{route('be.post.update')}}">
            @method('PUT')
            <input type="hidden" name="pst_id" value="{{$post->post_id}}">
            <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <div>
                                    <x-BE.Elements.Image width="322" src="{{asset($post->thumbnail)}}" alt="{{$post->name}}"/>
                                </div>
                            </div>
                            <x-BE.Elements.Input id="pst_thumbnail" type="file">
                                <x-slot name="warning">Hình được phép: png, jpg, jpeg, gif. Kích thước > (200x200) và dung lượng < 1Mb.</x-slot>
                            </x-BE.Elements.Input>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="pst_views" value="{{old('pst_views') ?? $post->views}}" disabled=""/>
                            <x-BE.Elements.Input id="pst_author" value="{{old('pst_author') ?? $post->author}}"/>
                            <div class="form-group">
                                <label for="pst_status">{{__('pst_status')}}</label>
                                <select class="form-control" name="pst_status" id="pst_status">
                                    @foreach($pstStatus as $key => $item)
                                        <option value="{{$key}}" @selected($post->status == $key || $key == old('pst_status'))>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pst_grp_id">{{__('pst_grp_id')}}</label>
                                <select class="form-control" name="pst_grp_id" id="pst_grp_id">
                                    @foreach($postGroups as $postGroup)
                                        <option value="{{$postGroup->pgrp_id}}" @selected($postGroup->pgrp_id == $post->pgrp_id || $postGroup->pgrp_id == old('pst_grp_id'))>{{$postGroup->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="vi-just" role="tabpanel" aria-labelledby="vi-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="pst_name" value="{{old('pst_name') ?? $post->name}}"/>
                            <x-BE.Elements.Textarea content="{{old('pst_short_des') ?? $post->short_description}}" rows="9" id="pst_short_des"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('pst_meta_des') ?? $post->meta_description}}" rows="4" id="pst_meta_des">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('pst_meta_keyword') ?? $post->meta_keyword}}" rows="4" id="pst_meta_keyword">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <x-BE.Elements.Textarea class="js-editor" content="{{old('pst_content') ?? $post->content}}" rows="15" id="pst_content"/>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="en-just" role="tabpanel" aria-labelledby="en-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="pst_name_en" value="{{old('pst_name_en') ?? $post->name_en}}"/>
                            <x-BE.Elements.Textarea content="{{old('pst_short_des_en') ?? $post->short_description_en}}" rows="9" id="pst_short_des_en"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('pst_meta_des_en') ?? $post->meta_description_en}}" rows="4" id="pst_meta_des_en">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('pst_meta_keyword_en') ?? $post->meta_keyword_en}}" rows="4" id="pst_meta_keyword_en">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <x-BE.Elements.Textarea class="js-editor" content="{{old('pst_content_en') ?? $post->content_en}}" rows="15" id="pst_content_en"/>
                        </div>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Tab>
@endsection
