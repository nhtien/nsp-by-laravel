@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Bài Viết">
        <x-BE.Elements.Button type="create" href="{{route('be.post.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.post.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.post.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    @include('be.blocks.searching.post.index')
    <x-BE.Tables.Form>
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Ảnh đại diện</th>
                <th>Tiêu đề</th>
                <th>Tiêu đề (Tiếng Anh)</th>
                <th>Nhóm</th>
                <th>Ngày tạo</th>
                <th>Ngày cập nhật</th>
                <th>Được tạo bởi</th>
                <th>Trạng thái</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
                <tr>
                    <x-BE.Tables.CheckboxID>
                        {{$post->post_id}}
                    </x-BE.Tables.CheckboxID>
                    <td>{{$post->post_id}}</td>
                    <td>
                        <x-BE.Elements.Image src="{{asset($post->thumbnail)}}" alt="{{$post->name}}"/>
                    </td>
                    <td><a class="underline-dashed" href="{{route('be.post.edit', ['id' => $post->post_id])}}">{{$post->name}}</a></td>
                    <td>{{$post->name_en}}</td>
                    <td>{{$post->postGroup->name}}</td>
                    <td>{{$post->created_at}}</td>
                    <td>{{$post->updated_at}}</td>
                    <td>{{optional($post->user)->name}}</td>
                    <x-BE.Tables.Status status="{{$post->status}}"></x-BE.Tables.Status>
                </tr>
            @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $posts->links('be.blocks.pagination') }}
@endsection
