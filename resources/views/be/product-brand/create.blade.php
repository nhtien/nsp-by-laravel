@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Hãng [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productbrand.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productbrand.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productbrand.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_brd_name" value="{{old('prod_brd_name')}}"/>
                    <x-BE.Elements.Input id="prod_brd_name_en" value="{{old('prod_brd_name_en')}}"/>
                    <div class="form-group">
                        <label for="prod_brd_status">{{__('prod_brd_status')}}</label>
                        <select class="form-control" name="prod_brd_status" id="prod_brd_status">
                            @foreach($brandStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_brd_status') || $key == 'activated')>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_brd_picture" value="{{old('prod_brd_picture')}}">
                        <x-slot name="warning">Nhập tên file hình vào đây</x-slot>
                    </x-BE.Elements.Input>
                    <button type="button" class="btn btn-primary glow mr-1 mb-1 js-open-popup-ckfinder" data-url="/ckfinder-show.html?type=Images&folder=product-brand">Kho ảnh</button>
                    <x-BE.Elements.Warning>
                        + Ảnh có định dạng 'png'. <br/>
                        + Kích thước: 411x211 (width: 411; height: 221)
                        {{--+ Ảnh tại Menu - tải vào thư mục 'menu' (width: 111; height:51).<br/>
                        + Ảnh tại trang Home (width: 411; height: 221).<br/>--}}
                    </x-BE.Elements.Warning>
                </div>
                <div class="col-12 col-sm-12">
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_brd_des')}}" rows="15" id="prod_brd_des"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
