@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Hãng [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productbrand.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productbrand.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productbrand.update')}}">
            <input type="hidden" name="prod_brd_id" value="{{$brand->prodbrd_id}}">
            @method('PUT')
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_brd_name" value="{{old('prod_brd_name') ?? $brand->name}}"/>
                    <x-BE.Elements.Input id="prod_brd_name_en" value="{{old('prod_brd_name_en') ?? $brand->name_en}}"/>
                    <div class="form-group">
                        <label for="prod_brd_status">{{__('prod_brd_status')}}</label>
                        <select class="form-control" name="prod_brd_status" id="prod_brd_status">
                            @foreach($brandStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_brd_status') || $key == $brand->status)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_brd_picture" value="{{old('prod_brd_picture') ?? $brand->getRawOriginal('picture')}}">
                        <x-slot name="warning">Nhập tên file hình vào đây</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.elements.popup-ckfinder type="Images" folder="product-brand"/>
                    <x-BE.Elements.Warning>
                        + Ảnh có định dạng 'png'. <br/>
                        + Kích thước: 411x211 (width: 411; height: 221)
                        {{--+ Ảnh tại Menu - tải vào thư mục 'menu' (width: 111; height:51).<br/>
                        + Ảnh tại trang Home (width: 411; height: 221).<br/>--}}
                    </x-BE.Elements.Warning>
                </div>
                <div class="col-12 col-sm-12">
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_brd_des') ?? $brand->description}}" rows="15" id="prod_brd_des"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
