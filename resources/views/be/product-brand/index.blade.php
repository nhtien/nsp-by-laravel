@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Hãng">
        <x-BE.Elements.Button type="create" href="{{route('be.productbrand.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.productbrand.edit')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Ảnh đại diện</th>
            <th>Tên hãng</th>
            <th>Tên hãng (EN)</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($productBrands as $item)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$item->prodbrd_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$item->prodbrd_id}}</td>
                <td><x-BE.Elements.Image src="{{asset($item->picture)}}" alt="{{$item->name}}"/></td>
                <td><a class="underline-dashed" href="{{route('be.productbrand.edit', ['id' => $item->prodbrd_id])}}">{{$item->name}}</a></td>
                <td>{{$item->name_en}}</td>
                <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $productBrands->links('be.blocks.pagination') }}
@endsection
