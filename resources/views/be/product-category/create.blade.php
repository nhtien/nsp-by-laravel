@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm sản phẩm [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productcategory.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productcategory.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Tab title="">
        <x-BE.Elements.Form action="{{route('be.productcategory.store')}}">
            <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_cat_thumbnail" type="file">
                                <x-slot name="warning">Hình được phép: png, jpg, jpeg, gif. Kích thước > (300x300) và dung lượng < 1Mb.</x-slot>
                            </x-BE.Elements.Input>
                            <x-BE.Elements.Input id="prod_cat_icon" value="{{old('prod_cat_icon')}}"/>
                            <div class="form-group">
                                <label for="prod_tag_ids">{{__('prod_tag_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_tag_ids" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->prtag_id}}" @selected(in_array($tag->prtag_id, explode('||', old('prod_tag_ids')))) >{{notation_by_level($tag->level - 1)}} {{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="prod_cat_display_header_menu">{{__('prod_cat_display_header_menu')}}</label>
                                <select class="form-control" name="prod_cat_display_header_menu" id="prod_cat_display_header_menu">
                                    @foreach($catDisplayHeaderMenu as $key => $item)
                                        <option value="{{$key}}" @selected($key == old('prod_cat_display_header_menu'))>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_display_sidebar">{{__('prod_cat_display_sidebar')}}</label>
                                <select class="form-control" name="prod_cat_display_sidebar" id="prod_cat_display_sidebar">
                                    @foreach($catDisplaySidebar as $key => $item)
                                        <option value="{{$key}}" @selected($key == old('prod_cat_display_sidebar'))>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_status">{{__('prod_cat_status')}}</label>
                                <select class="form-control" name="prod_cat_status" id="prod_cat_status">
                                    @foreach($catStatus as $key => $item)
                                        <option value="{{$key}}" @selected($key == old('prod_cat_status'))>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_type">{{__('prod_cat_type')}}</label>
                                <select class="form-control" name="prod_cat_type" id="prod_cat_type">
                                    @foreach($catType as $key => $item)
                                        <option value="{{$key}}" @selected($key == old('prod_cat_type'))>{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_parent">Thuộc nhóm sp (node)</label>
                                <select class="form-control" name="prod_cat_parent" id="prod_cat_parent">
                                    @foreach($cats as $key => $item)
                                        <option value="{{$item->prcat_id}}" @selected($item->prcat_id == old('prod_cat_parent'))>{{notation_by_level($item->level)}} {{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <x-BE.Node id="prod_cat_position"></x-BE.Node>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="vi-just" role="tabpanel" aria-labelledby="vi-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_cat_name" value="{{old('prod_cat_name')}}"/>
                            <x-BE.Elements.Input id="prod_cat_short_name" value="{{old('prod_cat_short_name')}}"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_cat_description')}}" rows="4" id="prod_cat_description">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_cat_keyword')}}" rows="4" id="prod_cat_keyword">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="en-just" role="tabpanel" aria-labelledby="en-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_cat_name_en" value="{{old('prod_cat_name_en')}}"/>
                            <x-BE.Elements.Input id="prod_cat_short_name_en" value="{{old('prod_cat_short_name_en')}}"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_cat_description_en')}}" rows="4" id="prod_cat_description_en">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_cat_keyword_en')}}" rows="4" id="prod_cat_keyword_en">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Tab>
@endsection
