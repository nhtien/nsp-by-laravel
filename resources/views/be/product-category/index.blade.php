@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm sản phẩm">
        <x-BE.Elements.Button type="create" href="{{route('be.productcategory.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.productcategory.edit')}}"></x-BE.Elements.Button>
        {{--<x-BE.Elements.Button type="destroy" data-url="{{route('be.productcategory.destroy')}}"></x-BE.Elements.Button>--}}
    </x-BE.Action>
    @include('be.blocks.searching.product-category.index')
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>Sắp xếp theo danh mục</th>
            <th>Ảnh đại diện</th>
            <th>Tên</th>
            <th>Tên (EN)</th>
            <th>Nhóm SP cha</th>
            <th>Loại</th>
            <th>Trạng thái</th>
            <th>ID</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cats as $item)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$item->prcat_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$item->left}}</td>
                <td>
                    @if($item->getRawOriginal('thumbnail'))
                        <x-BE.Elements.Image src="{{asset( $item->thumbnail)}}" alt="{{$item->name}}"/>
                    @else
                        <x-BE.Elements.Image src="{{asset('/public/uploads/no-image/380x380.png')}}" alt="{{$item->name}}"/>
                    @endif
                </td>
                <td><a class="underline-dashed" href="{{route('be.productcategory.edit', ['id' => $item->prcat_id ])}}"> {{notation_by_level($item->level - 1)}} {{$item->name}}</a></td>
                <td>@if($item->name_en != ''){{notation_by_level($item->level - 1)}} @endif {{$item->name_en}}</td>
                <td>{{notation_by_level($item->level - 2)}} {{$parentItems->get($item->parent)}}</td>
                <td>{{$catType[$item->type]}}</td>
                <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
                <td>{{$item->prcat_id}}</td>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $cats->links('be.blocks.pagination') }}
@endsection
