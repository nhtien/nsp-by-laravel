@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Thông tin đặt hàng [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productchildren.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.edit', ['id' => $product->prod_id])}}"></x-BE.Elements.Button>
        <p class="text-left">
            <span>Sản phẩm: <a class="underline-dashed" href="{{route('be.product.edit', ['id' => $product->prod_id])}}">{{$product->name}}</a></span>
        </p>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productchildren.store')}}">
            <input type="hidden" name="prod_id" value="{{$product->prod_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_child_part_number" value="{{old('prod_child_part_number')}}"/>
                    <x-BE.Elements.Input id="prod_child_name" value="{{old('prod_child_name')}}"/>
                    <x-BE.Elements.Input id="prod_child_name_en" value="{{old('prod_child_name_en')}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input type="number" id="prod_child_rank" value="{{old('prod_child_rank')}}"/>
                    <div class="form-group">
                        <label for="prod_child_status">{{__('prod_child_status')}}</label>
                        <select class="form-control" name="prod_child_status" id="prod_child_status">
                            @foreach($childrenStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_child_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
