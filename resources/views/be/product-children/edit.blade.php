@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Thông tin đặt hàng [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productchildren.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.edit', ['id' => $product->prod_id])}}"></x-BE.Elements.Button>
        <p class="text-left">
            <span>Sản phẩm: <a class="underline-dashed" href="{{route('be.product.edit', ['id' => $product->prod_id])}}">{{$product->name}}</a></span>
        </p>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productchildren.update')}}">
            <input type="hidden" name="prod_child_id" value="{{$children->prchl_id}}">
            @method('PUT')
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_child_part_number" value="{{old('prod_child_part_number') ?? $children->part_number}}"/>
                    <x-BE.Elements.Input id="prod_child_name" value="{{old('prod_child_name') ?? $children->name}}"/>
                    <x-BE.Elements.Input id="prod_child_name_en" value="{{old('prod_child_name_en') ?? $children->name_en}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_child_rank" type="number" value="{{old('prod_child_rank') ?? $children->rank}}"/>
                    <div class="form-group">
                        <label for="prod_child_status">{{__('prod_child_status')}}</label>
                        <select class="form-control" name="prod_child_status" id="prod_child_status">
                            @foreach($childrenStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_child_status') || $key == $children->status)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
