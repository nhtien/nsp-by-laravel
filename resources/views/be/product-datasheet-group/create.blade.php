@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm datasheet [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productdatasheetgroup.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productdatasheetgroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productdatasheetgroup.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_sht_grp_name" value="{{old('prod_sht_grp_name')}}"/>
                    <x-BE.Elements.Input id="prod_sht_grp_name_en" value="{{old('prod_sht_grp_name_en')}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_sht_grp_status">{{__('prod_sht_grp_status')}}</label>
                        <select class="form-control" name="prod_sht_grp_status" id="prod_sht_grp_status">
                            @foreach($grpStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_sht_grp_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
