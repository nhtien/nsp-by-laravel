@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm datasheet [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productdatasheetgroup.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productdatasheetgroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productdatasheetgroup.update')}}">
            <input type="hidden" name="prod_sht_grp_id" value="{{$datasheetGroup->prdagrp_id}}">
            @method('PUT')
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_sht_grp_name" value="{{old('prod_sht_grp_name') ?? $datasheetGroup->name}}"/>
                    <x-BE.Elements.Input id="prod_sht_grp_name_en" value="{{old('prod_sht_grp_name_en') ?? $datasheetGroup->name_en}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_sht_grp_status">{{__('prod_sht_grp_status')}}</label>
                        <select class="form-control" name="prod_sht_grp_status" id="prod_sht_grp_status">
                            @foreach($datasheetStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_sht_grp_status') || $key == $datasheetGroup->status)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
