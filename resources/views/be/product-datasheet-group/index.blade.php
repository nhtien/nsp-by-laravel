@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm datasheet">
        <x-BE.Elements.Button type="create" href="{{route('be.productdatasheetgroup.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.productdatasheetgroup.edit')}}"></x-BE.Elements.Button>
        {{--<x-BE.Elements.Button type="destroy" data-url="{{route('be.productdatasheetgroup.destroy')}}"></x-BE.Elements.Button>--}}
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên</th>
            <th>Tên (EN)</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datasheetGroups as $item)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$item->prdagrp_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$item->prdagrp_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.productdatasheetgroup.edit', ['id' => $item->prdagrp_id])}}">{{$item->name}}</a></td>
                <td>{{$item->name_en}}</td>
                <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $datasheetGroups->links('be.blocks.pagination') }}
@endsection
