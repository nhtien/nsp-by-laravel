@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Datasheet [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productdatasheet.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.edit', ['id' => $product->prod_id])}}"></x-BE.Elements.Button>
        <p class="text-left">
            <span>Sản phẩm: <a class="underline-dashed" href="{{route('be.product.edit', ['id' => $product->prod_id])}}">{{$product->name}}</a></span>
        </p>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productdatasheet.store')}}">
            <input type="hidden" value="{{$product->prod_id}}" name="prod_id">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_sht_name" value="{{old('prod_sht_name')}}"/>
                    <x-BE.Elements.Input id="prod_sht_name_en" value="{{old('prod_sht_name_en')}}"/>
                    <x-BE.Elements.Input id="prod_sht_rank" type="number" value="{{old('prod_sht_rank')}}"/>
                    <x-BE.Elements.Input id="prod_sht_link" value="{{old('prod_sht_link')}}">
                        <x-slot name="warning">Nhập đường dẫn file bên ngoài website</x-slot>
                    </x-BE.Elements.Input>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_sht_file">{{__('prod_sht_file')}}</label>
                        <select class="form-control" name="prod_sht_file" id="prod_sht_file">
                            <option value="">-- Chọn file -- </option>
                            @foreach($product->datasheet_files as $key => $item)
                                <option value="{{$item}}" @selected($item == old('prod_sht_file'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_sht_type">{{__('prod_sht_type')}}</label>
                        <select class="form-control" name="prod_sht_type" id="prod_sht_type">
                            @foreach($datasheetType as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_sht_type'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_sht_status">{{__('prod_sht_status')}}</label>
                        <select class="form-control" name="prod_sht_status" id="prod_sht_status">
                            @foreach($datasheetStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_sht_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_sht_grp_id">{{__('prod_sht_grp_id')}}</label>
                        <select class="form-control" name="prod_sht_grp_id" id="prod_sht_grp_id">
                            @foreach($datasheetGroups as $key => $item)
                                <option value="{{$item->prdagrp_id}}" @selected($item->prdagrp_id == old('prod_sht_grp_id'))>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
