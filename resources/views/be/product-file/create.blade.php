@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="File [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productfile.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.productfile.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productfile.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_file_title" value="{{old('prod_file_title')}}"/>
                    <x-BE.Elements.Input id="prod_file_name" type="file">
                    </x-BE.Elements.Input>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_file_status">{{__('prod_file_status')}}</label>
                        <select class="form-control" name="prod_file_status" id="prod_file_status">
                            @foreach($fileStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_file_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <x-BE.Elements.Textarea content="{{old('prod_file_des')}}" rows="6" id="prod_file_des"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
