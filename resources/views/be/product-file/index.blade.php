@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách File">
        <x-BE.Elements.Button type="create" href="{{route('be.productfile.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.productfile.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.productfile.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    @include('be.blocks.searching.product-file.index')
    <x-BE.Tables.Form>
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Tiêu đề</th>
                <th>File</th>
                <th>Mô tả</th>
                <th>Ngày tạo</th>
                <th>Ngày cập nhật</th>
                <th>Trạng thái</th>
            </tr>
        </thead>
        <tbody>
            @foreach($files as $file)
                <tr>
                    <x-BE.Tables.CheckboxID>
                        {{$file->id}}
                    </x-BE.Tables.CheckboxID>
                    <td>{{$file->id}}</td>
                    <td><a class="underline-dashed" href="{{route('be.productfile.edit', ['id' => $file->id])}}">{{$file->title}}</a></td>
                    <td>{{$file->file_name}}</td>
                    <td>{{$file->description}}</td>
                    <td>{{$file->created_at}}</td>
                    <td>{{$file->updated_at}}</td>
                    <x-BE.Tables.Status status="{{$file->status}}"></x-BE.Tables.Status>
                </tr>
            @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $files->links('be.blocks.pagination') }}
@endsection
