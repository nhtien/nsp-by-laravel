@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Thuộc tính [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productproperty.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.edit', ['id' => $product->prod_id])}}"></x-BE.Elements.Button>
        <p class="text-left">
            <span>Sản phẩm: <a class="underline-dashed" href="{{route('be.product.edit', ['id' => $product->prod_id])}}">{{$product->name}}</a></span>
        </p>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productproperty.store')}}">
            <input type="hidden" name="prod_id" value="{{$product->prod_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_pro_name" value="{{old('prod_pro_name')}}"/>
                    <x-BE.Elements.Input id="prod_pro_name_en" value="{{old('prod_pro_name_en')}}"/>
                    <x-BE.Elements.Input id="prod_pro_rank" type="number" value="{{old('prod_pro_rank')}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_pro_icon">{{__('prod_pro_icon')}}</label>
                        <select class="form-control" name="prod_pro_icon" id="prod_pro_icon">
                            @foreach($icons as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_pro_icon'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_pro_status">{{__('prod_pro_status')}}</label>
                        <select class="form-control" name="prod_pro_status" id="prod_pro_status">
                            @foreach($propertyStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_pro_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_pro_des')}}" rows="15" id="prod_pro_des"/>
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_pro_des_en')}}" rows="15" id="prod_pro_des_en"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
