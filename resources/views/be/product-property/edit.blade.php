@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Thuộc tính [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.productproperty.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.edit', ['id' => $product->prod_id])}}"></x-BE.Elements.Button>
        <p class="text-left">
            <span>Sản phẩm: <a class="underline-dashed" href="{{route('be.product.edit', ['id' => $product->prod_id])}}">{{$product->name}}</a></span>
        </p>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.productproperty.update')}}">
            <input type="hidden" name="prod_pro_id" value="{{$property->prppt_id}}">
            @method('PUT')
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_pro_name" value="{{old('prod_pro_name') ?? $property->name}}"/>
                    <x-BE.Elements.Input id="prod_pro_name_en" value="{{old('prod_pro_name_en') ?? $property->name_en}}"/>
                    <x-BE.Elements.Input id="prod_pro_rank" type="number" value="{{old('prod_pro_rank') ?? $property->rank}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_pro_icon">{{__('prod_pro_icon')}}</label>
                        <select class="form-control" name="prod_pro_icon" id="prod_pro_icon">
                            @foreach($property->icons as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_pro_icon') || $key == $property->icon)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_pro_status">{{__('prod_pro_status')}}</label>
                        <select class="form-control" name="prod_pro_status" id="prod_pro_status">
                            @foreach($propertyStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_pro_status') || $key == $property->status)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_pro_des') ?? $property->description}}" rows="15" id="prod_pro_des"/>
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('prod_pro_des_en') ?? $property->description_en}}" rows="15" id="prod_pro_des_en"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
