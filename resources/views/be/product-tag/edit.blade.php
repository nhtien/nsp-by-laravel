@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Tag [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.producttag.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.producttag.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.producttag.update')}}">
            @method('PUT')
            <input type="hidden" name="prod_tag_id" value="{{$tag->prtag_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="prod_tag_name" value="{{old('prod_tag_name') ?? $tag->name}}"/>
                    <x-BE.Elements.Input id="prod_tag_name_en" value="{{old('prod_tag_name_en') ?? $tag->name_en}}"/>
                    <div class="form-group">
                        <label for="prod_tag_status">{{__('prod_tag_status')}}</label>
                        <select class="form-control" name="prod_tag_status" id="prod_tag_status">
                            @foreach($tagStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_tag_status') || $key == $tag->status)>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="prod_tag_type">{{__('prod_tag_type')}}</label>
                        <select class="form-control" name="prod_tag_type" id="prod_tag_type">
                            @foreach($tagType as $key => $item)
                                <option value="{{$key}}" @selected($key == old('prod_tag_type') || $key == $tag->type )>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Cấu trúc Nhánh hiện tại:</label>
                        <br>
                        <div style="padding-left: 25px">
                            @foreach($tagBranchs as $item)
                                <span @class(['text-primary' => $item->prtag_id == $tag->prtag_id])>{{notation_by_level($item->level - 1)}} {{$item->name}}</span> <br>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="prod_tag_parent">Thuộc tag (node)</label>
                        <select class="form-control" name="prod_tag_parent" id="prod_tag_parent">
                            @foreach($tags as $key => $item)
                                <option value="{{$item->prtag_id}}" @selected($item->prtag_id == old('prod_tag_parent') || $item->prtag_id == $tag->parent)>{{notation_by_level($item->level)}} {{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <x-BE.Node id="prod_tag_position"></x-BE.Node>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
