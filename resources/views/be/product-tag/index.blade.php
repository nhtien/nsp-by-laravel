@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Tag">
        <x-BE.Elements.Button type="create" href="{{route('be.producttag.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.producttag.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.producttag.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    @include('be.blocks.searching.product-tag.index')
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th></th>
            <th>Tên</th>
            <th>Tên (EN)</th>
            <th>Loại</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $item)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$item->prtag_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$item->left}}</td>
                <td><a class="underline-dashed" href="{{route('be.producttag.edit', ['id' => $item->prtag_id])}}"> {{notation_by_level($item->level - 1)}} {{$item->name}}</a></td>
                <td>@if($item->name_en != ''){{notation_by_level($item->level - 1)}} @endif {{$item->name_en}}</td>
                <td>{{$tagType[$item->type]}}</td>
                <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $tags->links('be.blocks.pagination') }}
@endsection
