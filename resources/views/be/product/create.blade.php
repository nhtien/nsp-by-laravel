@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Sản phẩm [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.product.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.index')}}"></x-BE.Elements.Button>
        <p class="text-left">
            <strong>LƯU</strong> để có thể thêm 'Datasheet', 'Thông tin đặt hàng', 'Thuộc tính'
        </p>
    </x-BE.Action>
    <x-BE.Sections.Tab title="">
        <x-BE.Elements.Form action="{{route('be.product.store')}}">
            <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_thumbnail" type="file">
                                <x-slot name="warning">Hình được phép: png, jpg, jpeg, gif. Kích thước > (380x380) và dung lượng < 1Mb.</x-slot>
                            </x-BE.Elements.Input>
                            <div class="form-group">
                                <label for="prod_tag_ids">{{__('prod_tag_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_tag_ids" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->prtag_id}}" @selected(in_array($tag->prtag_id, convert_ids_to_array(old('prod_tag_ids')) ) ) >{{notation_by_level($tag->level - 1)}} {{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_solution_ids">{{__('prod_solution_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_solution_ids" multiple="multiple">
                                    @foreach($solutions as $solution)
                                        <option value="{{$solution->id}}" @selected(in_array($solution->id, convert_ids_to_array(old('prod_solution_ids')) ) ) >{{$solution->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_file_ids">{{__('prod_file_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_file_ids" multiple="multiple">
                                    @foreach($files as $file)
                                        <option value="{{$file->id}}" @selected(in_array($file->id, convert_ids_to_array(old('prod_file_ids')) ) ) >{{$file->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_part_number" value="{{old('prod_part_number')}}"></x-BE.Elements.Input>
                            <x-BE.Elements.Input id="prod_part_number_old" value="{{old('prod_part_number_old')}}"></x-BE.Elements.Input>
                            <div class="form-group">
                                <label for="prod_part_number_type">{{__('prod_part_number_type')}}</label>
                                <select class="form-control" name="prod_part_number_type" id="prod_part_number_type">
                                    @foreach($prodPartNumberType as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_part_number_type'))>{{$value}}</option>
                                    @endforeach
                                </select>
                                <x-BE.Elements.Warning>
                                    + Mặc đinh: ACB.<br/>
                                    + Hai mã sp: (ex: ABC/DEF).<br/>
                                    + Xem thêm: ABC (xem thêm).<br/>
                                    *** ABC, DEF là partnumer.
                                </x-BE.Elements.Warning>
                            </div>
                            <x-BE.Elements.Input id="prod_sale_off" value="{{old('prod_sale_off')}}">
                                <x-BE.Elements.Warning>
                                    ex: -30%, -15$
                                </x-BE.Elements.Warning>
                            </x-BE.Elements.Input>
                            <div class="form-group">
                                <label for="prod_type">{{__('prod_type')}}</label>
                                <select class="form-control" name="prod_type" id="prod_type">
                                    @foreach($prodType as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_type'))>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_status">{{__('prod_status')}}</label>
                                <select class="form-control" name="prod_status" id="prod_status">
                                    @foreach($prodStatus as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_status'))>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_brd_id">{{__('prod_brd_id')}}</label>
                                <select class="form-control" name="prod_brd_id" id="prod_brd_id">
                                    @foreach($brands as $key => $item)
                                        <option value="{{$item->prodbrd_id}}" @selected($item->prodbrd_id == old('prod_brd_id'))>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_id">{{__('prod_cat_id')}}</label>
                                <select class="form-control" name="prod_cat_id" id="prod_cat_id">
                                    @foreach($cats as $key => $item)
                                        @if($item->level != 0)
                                            <option value="{{$item->prcat_id}}" @selected($item->prcat_id == old('prod_cat_id'))>{{notation_by_level($item->level - 1)}} {{$item->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="vi-just" role="tabpanel" aria-labelledby="vi-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_name" value="{{old('prod_name')}}"/>
                            <x-BE.Elements.Textarea content="{{old('prod_short_des')}}" rows="9" id="prod_short_des"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_meta_des')}}" rows="4" id="prod_meta_des">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_meta_keyword')}}" rows="4" id="prod_meta_keyword">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="en-just" role="tabpanel" aria-labelledby="en-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_name_en" value="{{old('prod_name_en')}}"/>
                            <x-BE.Elements.Textarea content="{{old('prod_short_des_en')}}" rows="9" id="prod_short_des_en"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_meta_des_en')}}" rows="4" id="prod_meta_des_en">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_meta_keyword_en')}}" rows="4" id="prod_meta_keyword_en">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Tab>
@endsection
