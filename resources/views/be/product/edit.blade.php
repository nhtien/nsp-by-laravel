@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Sản phẩm [Sửa]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.product.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.product.index')}}"></x-BE.Elements.Button>
        <a type="button" class="btn btn-secondary mr-1 mb-1" href="{{route('be.productdatasheet.create', ['prod_id' => $product->prod_id])}}">
            <i class="bx bxs-add-to-queue"></i> Thêm datasheet
        </a>
        <a type="button" class="btn btn-bitbucket mr-1 mb-1" href="{{route('be.productchildren.create', ['prod_id' => $product->prod_id])}}">
            <i class="bx bxs-add-to-queue"></i> Thêm thông tin đặt hàng
        </a>
        <a type="button" class="btn btn-dropbox mr-1 mb-1" href="{{route('be.productproperty.create', ['prod_id' => $product->prod_id])}}">
            <i class="bx bxs-add-to-queue"></i> Thêm thuộc tính
        </a>
    </x-BE.Action>
    <x-BE.Sections.Tab title="" add-more-tab="yes">
        <x-slot name="addMoreTab">
            <li class="nav-item">
                <a class="nav-link" id="datasheet-tab-justified" data-toggle="tab" href="#datasheet-just" role="tab" aria-controls="datasheet-just" aria-selected="false">
                    Datasheet
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="children-tab-justified" data-toggle="tab" href="#children-just" role="tab" aria-controls="children-just" aria-selected="false">
                    Thông tin đặt hàng
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="property-tab-justified" data-toggle="tab" href="#property-just" role="tab" aria-controls="property-just" aria-selected="false">
                    Thuộc tính
                </a>
            </li>
        </x-slot>
        <x-BE.Elements.Form action="{{route('be.product.update')}}">
            @method('PUT')
            <input type="hidden" name="prod_id" value="{{$product->prod_id}}">
            <div class="tab-content pt-1">
                <div class="tab-pane active" id="home-just" role="tabpanel" aria-labelledby="home-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <div>
                                    @if($product->getRawOriginal('thumbnail'))
                                    <x-BE.Elements.Image width="322" src="{{asset($product->thumbnail)}}" alt=""/>
                                    @endif
                                </div>
                            </div>
                            <x-BE.Elements.Input id="prod_thumbnail" type="file">
                                <x-slot name="warning">Hình được phép: png, jpg, jpeg, gif. Kích thước > (380x380) và dung lượng < 1Mb.</x-slot>
                            </x-BE.Elements.Input>
                            <x-BE.Elements.Input id="prod_views" value="{{old('prod_views') ?? $product->views}}" disabled=""/>
                            <div class="form-group">
                                <label for="prod_tag_ids">{{__('prod_tag_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_tag_ids" multiple="multiple">
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->prtag_id}}" @selected(in_array($tag->prtag_id, $product->product_tag_ids) || in_array($tag->prtag_id, convert_ids_to_array(old('prod_tag_ids')) ) ) >{{notation_by_level($tag->level - 1)}} {{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_solution_ids">{{__('prod_solution_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_solution_ids" multiple="multiple">
                                    @foreach($solutions as $solution)
                                        <option value="{{$solution->id}}" @selected(in_array($solution->id, $product->solution_ids) || in_array($solution->id, convert_ids_to_array(old('prod_solution_ids')) ) ) >{{$solution->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_file_ids">{{__('prod_file_ids')}}</label>
                                <select class="select2 form-control js-select-multiple" data-name="prod_file_ids" multiple="multiple">
                                    @foreach($files as $file)
                                        <option value="{{$file->id}}" @selected(in_array($file->id, $product->product_file_ids) || in_array($file->id, convert_ids_to_array(old('prod_file_ids')) ) ) >{{$file->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if(file_exists( $product->path_image_related ) && is_dir( $product->path_image_related ))
                            <div class="form-group">
                                <label>Hình ảnh liên quan</label>
                                <div>
                                    <x-BE.elements.popup-ckfinder type="Images" folder="{{$product->image_folder_ckfinder}}"/>
                                </div>
                            </div>
                            @endif
                            @if(file_exists( $product->path_datasheet ) && is_dir( $product->path_datasheet ))
                            <div class="form-group">
                                <label>File datasheet</label>
                                <div>
                                    <x-BE.elements.popup-ckfinder title="UPLOAD DATASHEET" type="Datasheets" folder="{{$product->folder_name_by_part_number}}">
                                    </x-BE.elements.popup-ckfinder>
                                    <x-be.elements.warning name="warning">Quy tắc đặt tên file KHÔNG DẤU và KHÔNG KHOẢNG TRẮNG (ví dụ: sample_file_name.pdf)</x-be.elements.warning>
                                </div>
                            </div>
                            @endif
                            @if(file_exists( $product->path_file ) && is_dir( $product->path_file ))
                                <div class="form-group">
                                    <label>Files</label>
                                    <div>
                                        <x-BE.elements.popup-ckfinder title="UPLOAD FILE" type="Any" folder="product--file/{{$product->folder_name_by_part_number}}">
                                        </x-BE.elements.popup-ckfinder>
                                        <x-be.elements.warning name="warning">Quy tắc đặt tên file KHÔNG DẤU và KHÔNG KHOẢNG TRẮNG (ví dụ: sample_file_name_2.docx)</x-be.elements.warning>
                                    </div>
                                </div>
                            @endif
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_part_number" value="{{old('prod_part_number') ?? $product->part_number}}"></x-BE.Elements.Input>
                            <x-BE.Elements.Input id="prod_part_number_old" value="{{old('prod_part_number_old') ??$product->part_number_old}}"></x-BE.Elements.Input>
                            <div class="form-group">
                                <label for="prod_part_number_type">{{__('prod_part_number_type')}}</label>
                                <select class="form-control" name="prod_part_number_type" id="prod_part_number_type">
                                    @foreach($prodPartNumberType as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_part_number_type') || $product->part_number_display_type == $key)>{{$value}}</option>
                                    @endforeach
                                </select>
                                <x-BE.Elements.Warning>
                                    + Mặc đinh: ACB.<br/>
                                    + Hai mã sp: (ex: ABC/DEF).<br/>
                                    + Xem thêm: ABC (xem thêm).<br/>
                                    *** ABC, DEF là partnumer.
                                </x-BE.Elements.Warning>
                            </div>
                            <x-BE.Elements.Input id="prod_sale_off" value="{{old('prod_sale_off') ?? $product->sale_off}}">
                                <x-BE.Elements.Warning>
                                    ex: -30%, -15$
                                </x-BE.Elements.Warning>
                            </x-BE.Elements.Input>
                            <div class="form-group">
                                <label for="prod_type">{{__('prod_type')}}</label>
                                <select class="form-control" name="prod_type" id="prod_type">
                                    @foreach($prodType as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_type') || $product->type == $key)>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_status">{{__('prod_status')}}</label>
                                <select class="form-control" name="prod_status" id="prod_status">
                                    @foreach($prodStatus as $key => $value)
                                        <option value="{{$key}}" @selected($key == old('prod_status') || $product->status == $key)>{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_brd_id">{{__('prod_brd_id')}}</label>
                                <select class="form-control" name="prod_brd_id" id="prod_brd_id">
                                    @foreach($brands as $key => $item)
                                        <option value="{{$item->prodbrd_id}}" @selected($item->prodbrd_id == old('prod_brd_id') || $product->prodbrd_id == $item->prodbrd_id)>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="prod_cat_id">{{__('prod_cat_id')}}</label>
                                <select class="form-control" name="prod_cat_id" id="prod_cat_id">
                                    @foreach($cats as $key => $item)
                                        @if($item->level != 0)
                                        <option value="{{$item->prcat_id}}" @selected($item->prcat_id == old('prod_cat_id') || $product->prcat_id == $item->prcat_id)>{{notation_by_level($item->level - 1)}} {{$item->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="vi-just" role="tabpanel" aria-labelledby="vi-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_name" value="{{old('prod_name') ?? $product->name}}"/>
                            <x-BE.Elements.Textarea content="{{old('prod_short_des') ?? $product->short_description}}" rows="9" id="prod_short_des"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_meta_des') ?? $product->meta_description}}" rows="4" id="prod_meta_des">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_meta_keyword') ?? $product->meta_keyword}}" rows="4" id="prod_meta_keyword">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="en-just" role="tabpanel" aria-labelledby="en-tab-justified">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Input id="prod_name_en" value="{{old('prod_name_en') ?? $product->name_en}}"/>
                            <x-BE.Elements.Textarea content="{{old('prod_short_des_en') ?? $product->short_description_en}}" rows="9" id="prod_short_des_en"/>
                        </div>
                        <div class="col-12 col-sm-6">
                            <x-BE.Elements.Textarea content="{{old('prod_meta_des_en') ?? $product->meta_description_en}}" rows="4" id="prod_meta_des_en">
                                <x-slot name="warning">Mô tả ngắn gọn 155-160 ký tự, giúp công cụ tìm kiếm hiểu rõ hơn về chủ đề trang web.</x-slot>
                            </x-BE.Elements.Textarea>
                            <x-BE.Elements.Textarea content="{{old('prod_meta_keyword_en') ?? $product->meta_keyword_en}}" rows="4" id="prod_meta_keyword_en">
                                <x-slot name="warning">Giúp cho công cụ tìm kiếm biết chủ đề của trang, đảm bảo rằng mỗi từ khóa phản ánh chính xác nội dung.</x-slot>
                            </x-BE.Elements.Textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="datasheet-just" role="tabpanel" aria-labelledby="datasheet-tab-justified">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên file</th>
                                        <th>Vị trí</th>
                                        <th>File</th>
                                        <th>Link</th>
                                        <th>Loại file</th>
                                        <th>Trạng thái</th>
                                        <th>Nhóm</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($datasheets as $item)
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>
                                                <span class="text-danger">VI</span>: {{$item->name}} <br>
                                                <span class="text-danger">EN</span>: {{$item->name_en}}
                                            </td>
                                            <td>{{$item->rank}}</td>
                                            <td>{{$item->file}}</td>
                                            <td>{{$item->link}}</td>
                                            <td>{{$item->type_convert_vietnamese}}</td>
                                            <td>{{optional($datasheetGroups->find($item->prdagrp_id))->name}}</td>
                                            <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
                                            <td>
                                                <a href="{{route('be.productdatasheet.edit', ['prod_sht_id' => $item->prda_id, 'prod_id' => $item->prod_id])}}" title="Sửa" class="btn btn-primary mr-1 mb-1">
                                                    <i class="bx bxs-pencil"></i>
                                                </a>
                                                <a data-url="{{route('be.productdatasheet.destroy')}}" data-id="{{$item->encode_id}}" title="Xóa" class="btn btn-outline-danger mr-1 mb-1 js-delete-item">
                                                    <i class="bx bx-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="children-just" role="tabpanel" aria-labelledby="children-tab-justified">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Part number</th>
                                        <th>Tên</th>
                                        <th>Vị trí</th>
                                        <th>Trạng thái</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($childrens as $item)
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$item->part_number}}</td>
                                            <td>
                                                <span class="text-danger">VI</span>: {{$item->name}} <br>
                                                <span class="text-danger">EN</span>: {{$item->name_en}}
                                            </td>
                                            <td>{{$item->rank}}</td>
                                            <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
                                            <td>
                                                <a href="{{route('be.productchildren.edit', ['prod_child_id' => $item->prchl_id, 'prod_id' => $item->prod_id])}}" title="Sửa" class="btn btn-primary mr-1 mb-1">
                                                    <i class="bx bxs-pencil"></i>
                                                </a>
                                                <a data-url="{{route('be.productchildren.destroy')}}" data-id="{{$item->encode_id}}" title="Xóa" class="btn btn-outline-danger mr-1 mb-1 js-delete-item">
                                                    <i class="bx bx-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="property-just" role="tabpanel" aria-labelledby="property-tab-justified">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>Tên (EN)</th>
                                        <th>Vị trí</th>
                                        <th>Trạng thái</th>
                                        <th>ACTION</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($properties as $item)
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->name_en}}</td>
                                            <td>{{$item->rank}}</td>
                                            <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
                                            <td>
                                                <a href="{{route('be.productproperty.edit', ['prod_pro_id' => $item->prppt_id, 'prod_id' => $item->prod_id])}}" title="Sửa" class="btn btn-primary mr-1 mb-1">
                                                    <i class="bx bxs-pencil"></i>
                                                </a>
                                                <a data-url="{{route('be.productproperty.destroy')}}" data-id="{{$item->encode_id}}" title="Xóa" class="btn btn-outline-danger mr-1 mb-1 js-delete-item">
                                                    <i class="bx bx-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Tab>
@endsection
