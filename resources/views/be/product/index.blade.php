@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Sản phẩm">
        <x-BE.Elements.Button type="create" href="{{route('be.product.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.product.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.product.destroy')}}"></x-BE.Elements.Button>
        <p class="text-left">
            Lưu ý khi <span class="text-danger"><strong>'XÓA SẢN PHẨM</strong>'</span>, những thứ liên quan đến sp bị <strong>XÓA</strong> sẽ không thể khôi phục:
            <br> + File (Hình ảnh, Datasheet)
            <br> + CSDL (Thuộc tính, thông tin đặt hàng, thông tin datasheet)
        </p>
    </x-BE.Action>
    @include('be.blocks.searching.product.index')
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>Ảnh đại diện</th>
            <th>Part number</th>
            <th>Part number cũ</th>
            <th>Tên sp</th>
            <th>Loại sp</th>
            <th>Nhóm sản phẩm</th>
            <th>Hãng sản phẩm</th>
            <th>Trạng thái</th>
            <th>Ngày tạo </th>
            <th>ID</th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $item)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$item->prod_id}}
                </x-BE.Tables.CheckboxID>
                <td>
                    @if($item->getRawOriginal('thumbnail'))
                        <x-BE.Elements.Image src="{{asset( $item->thumbnail)}}" alt="{{$item->name}}"/>
                    @else
                        <x-BE.Elements.Image src="{{asset('/public/uploads/no-image/380x380.png')}}" alt="{{$item->name}}"/>
                    @endif
                </td>
                <td><a class="underline-dashed" href="{{route('be.product.edit', ['id' => $item->prod_id])}}">{{$item->part_number}}</a></td>
                <td>{{$item->part_number_old}}</td>
                <td>
                    <span class="text-danger">VI:</span> {{$item->name}}
                    <br>
                    <br>
                    <span class="text-danger">EN:</span> {{$item->name_en}}
                </td>
                <td @class([
                        'text-primary' => $item->type == 'default',
                        'text-success' => $item->type == 'new',
                        'text-danger'  => $item->type == 'hot',
                    ]))>{{$prodType[$item->type]}}</td>
                <td>{{optional($item->productCategory)->name}}</td>
                <td>{{optional($item->productBrand)->name}}</td>
                <x-BE.Tables.Status status="{{$item->status}}"></x-BE.Tables.Status>
                <td>{{$item->created_at}}</td>
                <td>{{$item->prod_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.product.clone', ['id' => $item->prod_id])}}">Clone</a></td>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $products->links('be.blocks.pagination') }}
@endsection
