@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm slider [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.slidergroup.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.slidergroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.slidergroup.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="sld_grp_name" value="{{old('sld_grp_name')}}"/>
                    <x-BE.Elements.Input id="sld_grp_note" value="{{old('sld_grp_note')}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="sld_grp_code" value="{{old('sld_grp_code')}}"/>
                    <div class="form-group">
                        <label for="sld_grp_status">{{__('sld_grp_status')}}</label>
                        <select class="form-control" name="sld_grp_status" id="sld_grp_status">
                            @foreach($sldgrpStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('sld_grp_status') || $key == 'activated')>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
