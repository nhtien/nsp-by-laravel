@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm slider">
        <x-BE.Elements.Button type="create" href="{{route('be.slidergroup.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.slidergroup.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.slidergroup.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên</th>
            <th>Mã</th>
            <th>Ghi chú</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sliderGroups as $sliderGroup)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$sliderGroup->sldgrp_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$sliderGroup->sldgrp_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.slidergroup.edit', ['id' => $sliderGroup->sldgrp_id])}}">{{$sliderGroup->name}}</a></td>
                <td>{{$sliderGroup->code}}</td>
                <td>{{$sliderGroup->note}}</td>
                <x-BE.Tables.Status status="{{$sliderGroup->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $sliderGroups->links('be.blocks.pagination') }}
@endsection
