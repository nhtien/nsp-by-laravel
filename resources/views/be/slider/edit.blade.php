@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Slider [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.slider.update')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.slider.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.slider.update')}}">
            @method('PUT')
            <input type="hidden" name="sld_id" value="{{$slider->sld_id}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="sld_name" value="{{old('sld_name') ?? $slider->name}}"/>
                    <x-BE.Elements.Input id="sld_picture" type="file">
                        <x-slot name="warning">Hình được phép: png, jpg, jpeg, gif. Dung lượng < 10Mb.</x-slot>
                    </x-BE.Elements.Input>
                    <x-BE.Elements.Input type="number" id="sld_position" value="{{old('sld_position') ?? $slider->position}}"/>
                </div>
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label for="sld_status">{{__('sld_status')}}</label>
                        <select class="form-control" name="sld_status" id="sld_status">
                            @foreach($sldStatus as $key => $item)
                                <option value="{{$key}}" @selected($slider->status == $key || $key == old('sld_status'))>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="sld_grp_id">{{__('sld_grp_id')}}</label>
                        <select class="form-control" name="sld_grp_id" id="sld_grp_id">
                            @foreach($sliderGroups as $key => $item)
                                <option value="{{$item->sldgrp_id}}" @selected($slider->sldgrp_id == $item->sldgrp_id || $item->sldgrp_id == old('sld_grp_id'))>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-12">
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('sld_des') ?? $slider->description}}" rows="4" id="sld_des">
                    </x-BE.Elements.Textarea>
                    <x-BE.Elements.Textarea class="js-editor" content="{{old('sld_des_en')?? $slider->description_en}}" rows="4" id="sld_des_en">
                    </x-BE.Elements.Textarea>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
