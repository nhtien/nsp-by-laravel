@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm slider">
        <x-BE.Elements.Button type="create" href="{{route('be.slider.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.slider.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.slider.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Hình đại diện</th>
            <th>Tên</th>
            <th>Nhóm</th>
            <th>Vị trí</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($sliders as $slider)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$slider->sld_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$slider->sld_id}}</td>
                <td><x-BE.Elements.Image src="{{asset($slider->picture)}}" alt="{{$slider->name}}"/></td>
                <td><a class="underline-dashed" href="{{route('be.slider.edit', ['id' => $slider->sld_id])}}">{{$slider->name}}</a></td>
                <td>{{optional($slider->sliderGroup)->name}}</td>
                <td>{{$slider->position}}</td>
                <x-BE.Tables.Status status="{{$slider->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $sliders->links('be.blocks.pagination') }}
@endsection
