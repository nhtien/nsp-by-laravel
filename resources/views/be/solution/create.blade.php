@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Giải pháp [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.solution.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.solution.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.solution.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="title" value="{{old('title')}}"/>
                    <div class="form-group">
                        <label for="status">Trạng Thái</label>
                        <select class="form-control" name="status" id="status">
                            @foreach($solutionStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('status') || $key == 'activated')>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Textarea  content="{{old('description')}}" rows="4" id="description">
                    </x-BE.Elements.Textarea>
                </div>

            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
