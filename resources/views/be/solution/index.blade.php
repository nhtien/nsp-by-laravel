@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Giải pháp">
        <x-BE.Elements.Button type="create" href="{{route('be.solution.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.solution.edit')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>Tên</th>
            <th>Mô tả</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($solutions as $solution)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$solution->id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$solution->title}}</td>
                <td>{{$solution->description}}</td>
                <x-BE.Tables.Status status="{{$solution->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $solutions->links('be.blocks.pagination') }}
@endsection
