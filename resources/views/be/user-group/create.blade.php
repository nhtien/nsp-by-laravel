@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Nhóm nhân viên [Thêm]">
        <x-BE.Elements.Button type="save" data-url="{{route('be.usergroup.store')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="go-back" href="{{route('be.usergroup.index')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Sections.Card title="">
        <x-BE.Elements.Form action="{{route('be.usergroup.store')}}">
            <div class="row">
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Input id="ugrp_name" value="{{old('ugrp_name')}}"/>
                    <div class="form-group">
                        <label for="ugrp_status">Trạng thái</label>
                        <select class="form-control" name="ugrp_status" id="ugrp_status">
                            @foreach($groupStatus as $key => $item)
                                <option value="{{$key}}" @selected($key == old('ugrp_status') || $key == 'activated')>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-sm-6">
                    <x-BE.Elements.Textarea content="{{old('ugrp_des')}}" rows="4" id="ugrp_des"/>
                </div>
            </div>
        </x-BE.Elements.Form>
    </x-BE.Sections.Card>
@endsection
