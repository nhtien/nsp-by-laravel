@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhóm nhân viên">
        <x-BE.Elements.Button type="create" href="{{route('be.usergroup.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.usergroup.edit')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    <x-BE.Tables.Form>
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên</th>
            <th>Mô tả</th>
            <th>Trạng thái</th>
        </tr>
        </thead>
        <tbody>
        @foreach($userGroups as $userGroup)
            <tr>
                <x-BE.Tables.CheckboxID>
                    {{$userGroup->usrgroup_id}}
                </x-BE.Tables.CheckboxID>
                <td>{{$userGroup->usrgroup_id}}</td>
                <td><a class="underline-dashed" href="{{route('be.usergroup.edit', ['id' => $userGroup->usrgroup_id])}}">{{$userGroup->name}}</a></td>
                <td>{{$userGroup->description}}</td>
                <x-BE.Tables.Status status="{{$userGroup->status}}"></x-BE.Tables.Status>
            </tr>
        @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $userGroups->links('be.blocks.pagination') }}
@endsection
