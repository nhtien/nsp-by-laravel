@extends('be.layouts.main')
@section('content')
    <x-BE.Action title="Danh sách Nhân Viên">
        <x-BE.Elements.Button type="create" href="{{route('be.users.create')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="edit" data-url="{{route('be.users.edit')}}"></x-BE.Elements.Button>
        <x-BE.Elements.Button type="destroy" data-url="{{route('be.users.destroy')}}"></x-BE.Elements.Button>
    </x-BE.Action>
    @include('be.blocks.searching.user.index')
    <x-BE.Tables.Form>
        <thead>
            <tr>
                <th></th>
                <th>Họ và Tên</th>
                <th>Tài khoản đăng nhập</th>
                <th>Email</th>
                <th>Nhóm</th>
                <th>Ngày tạo</th>
                <th>Trạng thái</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <x-BE.Tables.CheckboxID>
                        {{$user->user_id}}
                    </x-BE.Tables.CheckboxID>
                    <td><a class="underline-dashed" href="{{route('be.users.edit', ['id' => $user->user_id])}}">{{$user->name}}</a></td>
                    <td>{{$user->username}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->userGroup->name}}</td>
                    <td>{{$user->created_at}}</td>
                    <x-BE.Tables.Status status="{{$user->status}}"></x-BE.Tables.Status>
                </tr>
            @endforeach
        </tbody>
    </x-BE.Tables.Form>
    {{ $users->links('be.blocks.pagination') }}
@endsection
