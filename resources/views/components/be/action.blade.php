<section id="options">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title strong-title">{{$title}}</h4>
        </div>
        <div class="card-body align-right">
            {{$slot}}
        </div>
    </div>
</section>
