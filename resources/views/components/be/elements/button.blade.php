@if($type == 'create')
    <a type="button" {{ $attributes->merge(['class' => 'btn btn-outline-primary mr-1 mb-1']) }}>
        <i class="bx bxs-add-to-queue"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Thêm'}}
    </a>
@elseif($type == 'edit')
    <button type="button" {{ $attributes->merge(['class' => 'btn btn-outline-primary mr-1 mb-1 js-edit']) }}>
        <i class="bx bx-edit"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Sửa'}}
    </button>
@elseif($type == 'detail')
    <button type="button" {{ $attributes->merge(['class' => 'btn btn-outline-primary mr-1 mb-1 js-edit']) }}>
        <i class="bx bx-show-alt"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Chi tiết'}}
    </button>
@elseif($type == 'destroy')
    <button type="button" {{ $attributes->merge(['class' => 'btn btn-outline-danger mr-1 mb-1 js-delete']) }}>
        <i class="bx bx-trash"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Xóa'}}
    </button>
@elseif($type == 'save')
    <button type="button" {{ $attributes->merge(['class' => 'btn btn-outline-primary mr-1 mb-1 js-post-edit']) }} data-action-type="save">
        <i class="bx bxs-save"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Lưu'}}
    </button>
@elseif($type == 'go-back')
    <a type="button" {{ $attributes->merge(['class' => 'btn btn-outline-primary mr-1 mb-1']) }} href="{{url()->previous() }}">
        <i class="bx bx-rewind"></i>
        {{$attributes->has('title') ? $attributes->get('title') : ' Thoát'}}
    </a>
@endif


