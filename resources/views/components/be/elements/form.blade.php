<form {{$attributes}} class="form-validate" method="post" action="" id="main-form" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="action_type" value="">
    {{$slot}}
</form>
