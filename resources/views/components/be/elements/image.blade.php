<img {{$attributes}} width="150" class="img-fluid js-lazy-loading" data-src="{{$src}}" {{ $attributes->merge(['class' => 'js-lazy-loading']) }}>
