<div class="form-group">
    <div class="controls">
        <label for="{{$id}}">{{__($id)}}</label>
        <input {{$attributes}} type="text" placeholder="{{__($id)}}"  name="{{$id}}" id="{{$id}}" {{ $attributes->merge(['class' => 'form-control']) }}>
        {{$slot}}
        <small class="form-text text-warning">{{@$warning}}</small>
    </div>
</div>
