<button type="button" {{ $attributes->merge(['class' => 'btn btn-primary glow mr-1 mb-1 js-open-popup-ckfinder']) }} data-url='{{asset("/ckfinder-show.html?type={$type}&folder={$folder}")}}' {{$attributes}}>
    {{$attributes->get('title') ? $attributes->get('title') : 'Kho ảnh'}}
</button>
