<div class="form-group">
    <div class="controls">
        <label for="{{$id}}">{{__($id)}}</label>
        <textarea {{ $attributes->class(['form-control']) }} placeholder="{{__($id)}}"  id="{{$id}}" name="{{$id}}" rows="10" {{$attributes}} >{!! $content !!}</textarea>
        {{$slot}}
        <small class="form-text text-warning">{{@$warning}}</small>
    </div>
</div>
