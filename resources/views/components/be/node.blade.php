<label for="{{$id}}">{{__($id)}}</label>
<select class="form-control" name="{{$id}}" id="{{$id}}">
    <option value="0">-- Chọn vị trí --</option>
    <option value="left">Top (con của node)</option>
    <option value="right">Bottom (con của node)</option>
    <option value="before">Top (cùng cấp node)</option>
    <option value="after">Bottom (cùng cấp node)</option>
</select>
<small class="text-warning">
    Top (con của node): con của node đã chọn, và nằm ở vị trí đầu tiên. <br>
    Bottom (con của node): con của node đã chọn, và nằm ở vị trí dưới cùng. <br>
    Top (cùng cấp node): cùng cấp với node đã chọn, và nằm ngay phía trên trên node đó. <br>
    Bottom (cùng cấp node): cùng cấp với node đã chọn, và nằm ngay phía dưới trên node đó. <br>
</small>
