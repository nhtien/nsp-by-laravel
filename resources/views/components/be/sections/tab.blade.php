<section id="nav-justified">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{$title}}</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab-justified" data-toggle="tab" href="#home-just" role="tab" aria-controls="home-just" aria-selected="true">
                                CHUNG
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="vi-tab-justified" data-toggle="tab" href="#vi-just" role="tab" aria-controls="vi-just" aria-selected="true">
                                TIẾNG VIỆT
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab-justified" data-toggle="tab" href="#en-just" role="tab" aria-controls="en-just" aria-selected="false">
                                TIẾNG ANH
                            </a>
                        </li>
                         {{--Thêm điều kiện để những chỗ không gọi đến 'slot attribute' không phát sinh lỗi--}}
                        @if ($attributes->has('add-more-tab') == 'yes')
                            {{ $addMoreTab }}
                        @endif
                    </ul>
                    {{$slot}}
                </div>
            </div>
        </div>
    </div>
</section>
