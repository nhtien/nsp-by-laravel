<section>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <form action="" method="post" enctype="multipart/form-data" id="main-form">
                    @csrf
                    <table class="table table-transparent main-table" id="table-extended-chechbox">
                        {{$slot}}
                    </table>
                </form>
            </div>
        </div>
    </div>
</section>
