<div class="card" style="width: 18rem;">
    @if (@$image)
        <img src="{{$image->attributes->get('src')}}" class="card-img-top" alt="{{$image->attributes->get('alt')}}">
    @endif
    {{ $slot }}
    <div class="card-body">
        <p class="card-text">
            @if(@$body)
                {{$body}}
            @endif
        </p>
    </div>
</div>
