@props([
    'class' => 'form-label'
])
<label {{$attributes}} class="{{$class}}">{{$label}}</label>
