<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="Frest admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Frest admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Maintenance</title>
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.ico')}}" rel="shortcut icon">
    <link href="{{asset('/public/uploads/images/favicon/favicon_192x192.png')}}" rel="icon" sizes="192x192" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_96x96.png')}}" rel="icon" sizes="96x96" type="image/png">
    <link href="{{asset('/public/uploads/images/favicon/favicon_48x48.png')}}" rel="icon" sizes="48x48" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,600%7CIBM+Plex+Sans:300,400,500,600,700" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/vendors/css/vendors.min.css">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/themes/semi-dark-layout.css">
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="/public/be/app-assets/css/core/menu/menu-types/vertical-menu.css">
    <!-- END: Page CSS-->
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <!-- maintenance start -->
            <section class="row flexbox-container">
                <div class="col-xl-7 col-md-8 col-12">
                    <div class="card bg-transparent shadow-none">
                        <div class="card-body text-center bg-transparent">
                            <img src="/public/be/app-assets/images/pages/maintenance-2.png" class="img-fluid" alt="under maintenance" width="400">
                            <h1 class="error-title my-1">Under Maintenance!</h1>
                            <p class="px-2">Sorry for the inconvenience but we're performing some maintenance at the moment. If you need you can always contact us, otherwise we'll be back online shortly!</p>
                            {{--<a href="index.html" class="btn btn-primary round glow mt-2">BACK TO HOME</a>--}}
                        </div>
                    </div>
                </div>
            </section>
            <!-- maintenance end -->

        </div>
    </div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="/public/be/app-assets/vendors/js/vendors.min.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="/public/be/app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="/public/be/app-assets/js/scripts/configs/vertical-menu-light.js"></script>
<script src="/public/be/app-assets/js/core/app-menu.js"></script>
<script src="/public/be/app-assets/js/core/app.js"></script>
<script src="/public/be/app-assets/js/scripts/components.js"></script>
<script src="/public/be/app-assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->

</body>
<!-- END: Body-->

</html>
