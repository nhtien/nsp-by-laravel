@php
    /**
     * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
     */
    if ($paginator->count() < 1) {
        return false;
    }
    $from = $paginator->currentPage() == 1 ? 1 :  ($paginator->currentPage() - 1) * $paginator->perPage() + 1;
    $to   = $from + count($paginator->items());
    $to   = $to - 1;

    // http query
    $allParams = request()->all();
    unset($allParams['page']);
    $httpQuery = url()->current() .'?'. http_build_query($allParams);

    $previous = $httpQuery . '&page='. ($paginator->currentPage() - 1 > 0 ? $paginator->currentPage() - 1 : 1);
    $next = $httpQuery . '&page='. ($paginator->lastPage() == $paginator->currentPage() ? $paginator->lastPage() : $paginator->currentPage() + 1);
@endphp

@if ($paginator->hasPages())
    <ul class="pagination pagination-lg">
        <li class="page-item"><a class="page-link" href="{{$previous}}"><i class="fa fa-angle-left"></i></a></li>
        @foreach ($elements as $element)
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><a class="page-link" href="#">{{$page}}</a></li>
                    @elseif (($page == $paginator->currentPage() + 1 || $page == $paginator->currentPage() + 2) || $page == $paginator->lastPage())
                        <li class="page-item"><a class="page-link" href="{{$httpQuery . '&page=' . $page}}">{{$page}}</a></li>
                    @elseif ($page == $paginator->lastPage() - 1)
                        <li class="page-item"><a class="page-link" href="#">...</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        <li class="page-item"><a class="page-link" href="{{$next}}"><i class="fa fa-angle-right"></i></a></li>
    </ul>
@endif
