@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="background-overlay text-light">
        <div class="parallax-container img-loaded" data-velocity="-.090"
             style="background: rgba(0, 0, 0, 0) url('/public/uploads/images/slider/bg_image_fo_patch_cords.jpg') repeat scroll 0% 0%;"></div>
        <div class="container">
            <div class="page-title"><h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">Liên hệ</h2>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="text-uppercase">LIÊN HỆ VỚI CHÚNG TÔI</h3>
                    @if (session('success'))
                        <div class="alert alert-info alert-dismissible mb-2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <div class="d-flex align-items-center">
                                <i class="bx bx-like"></i>
                                <span>{{ session('success') }}</span>
                            </div>
                        </div>
                    @endif
                    <div class="m-t-30">
                        <form class="" action="{{route('fe.contact.store')}}" role="form" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="full_name">Họ và tên (<span style="color: #dc143c">*</span>)</label>
                                    <input type="text" value="{{old('full_name')}}" aria-required="true" name="full_name" class="form-control required name" placeholder="Họ tên đầy đủ">
                                    @error('full_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="company_name">Công ty</label>
                                    <input value="{{old('company_name')}}" type="text" name="company_name" class="form-control" placeholder="Tên công ty">
                                    @error('company_name')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="company_address">Địa chỉ</label>
                                    <input value="{{old('company_address')}}" type="text" name="company_address" class="form-control" placeholder="Địa chỉ">
                                    @error('company_address')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row">
                                 <div class="form-group col-md-6">
                                     <label for="phone_number">Điện thoại (<span style="color: #dc143c">*</span>)</label>
                                     <input value="{{old('phone_number')}}" type="text" aria-required="true" name="phone_number" class="form-control required" placeholder="Số điện thoại">
                                     @error('phone_number')
                                     <span class="text-danger">{{ $message }}</span>
                                     @enderror
                                 </div>
                                 <div class="form-group col-md-6">
                                     <label for="email">Email (<span style="color: #dc143c">*</span>)</label>
                                     <input value="{{old('email')}}" type="email" aria-required="true" name="email" class="form-control required email" placeholder="E-mail">
                                     @error('email')
                                     <span class="text-danger">{{ $message }}</span>
                                     @enderror
                                 </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label for="subject">Chủ đề (<span style="color: #dc143c">*</span>)</label>
                                    <input value="{{old('subject')}}" type="text" aria-required="true" name="subject" class="form-control required" placeholder="Chủ đề">
                                    @error('subject')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="message">Nội dung (<span style="color: #dc143c">*</span>)</label>
                                <textarea type="text" name="message" rows="5" class="form-control required" placeholder="Tin nhắn của bạn">{{old('message')}}</textarea>
                                @error('message')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <button class="btn" type="submit" id="form-submit"><i class="fa fa-paper-plane"></i>&nbsp;Gửi</button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-6">
                    {{--<h3 class="text-uppercase">Address & Map</h3>--}}
                    <div class="row">
                        <div class="col-lg-12">
                            <address>
                                <strong style="font-weight: bold">Công ty TNHH Nhân Sinh Phúc</strong><br>
                                124 Hồng Hà, Phường 2, Quận Tân Bình, TP.Hồ Chí Minh, Việt Nam<br>
                                E-mail: <a href="mailto:info@nsp.com.vn" style="color: #007bff">info@nsp.com.vn</a><br>
                                Điện thoại: <a style="color: #007bff" href="tel:02838342108">(+84-28) 3834 2108</a><br>
                                Fax: <a href="tel:02838342109" style="color: #007bff">(+84-28) 3834 2109</a><br>
                                Website: <a href="http://www.nsp.com.vn" target="_blank" style="color: #007bff">https://www.nsp.com.vn</a>
                            </address>
                        </div>
{{--                        <div class="col-lg-12">
                            <address>
                                <strong style="font-weight: bold">Văn phòng Kế toán & Kho</strong><br>
                                124 Hồng Hà, Phường 2, quận Tân Bình, Hồ Chí Minh<br>
                                Điện thoại: <a style="color: #007bff" href="tel:02838110073">(+84-28) 3811 0073</a><br>
                            </address>
                        </div>--}}
                        <div class="col-lg-12">
                            <address>
                                <strong style="font-weight: bold">Chi nhánh Hà Nội</strong><br>
                                Số 8 dãy 16B1 Làng Việt Kiều Châu Âu, Khu đô thị Mỗ Lao, Phường Mỗ Lao, quận Hà Đông, Thành phố Hà Nội<br>
                                Điện thoại: <a style="color: #007bff" href="tel:02432012996">(+84-24) 3201 2996</a><br>
                                Di động: <a style="color: #007bff" href="tel:0963 896 666">0963 896 666</a> (Mr. Thanh)<br>
                            </address>
                        </div>
                        <div class="col-lg-12">
                            <address>
                                <strong style="font-weight: bold">Chi nhánh Đà Nẵng</strong><br>
                                11 Thi Sách, Phường Hòa Thuận Tây, Quận Hải Châu, Đà Nẵng<br>
                                Điện thoại: <a style="color: #007bff" href="tel:0236356 8866">(+84-23) 6356 8866</a><br>
                                Di động: <a style="color: #007bff" href="tel:0914 958 877">0914 958 877</a> (Mr. Thủy)<br>
                            </address>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7353.920055644945!2d106.67218655454937!3d10.809426204185602!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317529219f02022b%3A0x3ffe4c3787a178ce!2zMTI0IEjhu5NuZyBIw6AsIFBoxrDhu51uZyAyLCBQaMO6IE5odeG6rW4sIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1680487260997!5m2!1sen!2s"
                        width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                        referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </section>
@endsection
