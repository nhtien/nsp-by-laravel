@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="background-overlay text-light">
        <div class="parallax-container img-loaded" data-velocity="-.090" style="background: rgba(0, 0, 0, 0) url('/public/uploads/images/slider/bg_image_fo_patch_cords.jpg') repeat scroll 0% 0%;"></div>
        <div class="container">
            <div class="page-title"><h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">Giới thiệu chung</h2></div>
        </div>
    </section>
    <div class="wrapper">
        <section id="page-content">
            <div class="container">
                <div class="row">
                    <div class="">
                        <p>Thành lập từ năm 1999 với vai trò là một trong số ít những công ty
                            tiên phong trong lĩnh vực hạ tầng hệ thống cáp cấu trúc tại Việt Nam.
                            Qua hơn 18 năm phát triển, NSP đã từng bước phát triển và hoàn thiện
                            mình để trở thành một trong những công ty hàng đầu trong lĩnh vực hạ
                            tầng hệ thống cáp cấu trúc nói riêng và thị trường hạ tầng hệ thống CNTT
                            Việt Nam nói chung.</p>
                        <p>Các sản phẩm của NSP đều được nhập khẩu từ các doanh nghiệp hàng đầu
                            về CNTT trên thế giới như CommScope, Fluke Networks, Emerson, Brady,
                            Brother... Ngoài ra, NSP còn là chủ sở hữu của thương hiệu Vietrack -
                            một trong những thương hiệu tủ rack lớn nhất tại thị trường Việt Nam.
                            Các sản phẩm của NSP có mặt trong hầu hết phòng thiết bị của các doanh
                            nghiệp tại Việt Nam, từ trung tâm dữ liệu tới các phòng thiết bị CNTT
                            vừa và nhỏ.</p>
                        <p>Hiện tại công ty NSP có trụ sở chính tại Tp. HCM, chi nhánh tại Hà
                            Nội và Đà Nẵng, cùng với hệ thống đại lý trải dài từ Bắc tới Nam với hơn
                            1000 công ty lớn nhỏ. Hệ thống phân phối trải dài và có chiều sâu giúp
                            công ty tiếp cận với hầu hết dự án trong cả nước để từng bước góp phần
                            nâng cao chất lượng của thị trường CNTT cả nước thông qua việc cung cấp
                            các sản phẩm/giải pháp CNTT mang tầm quốc tế.</p>
                        <p>Các sản phẩm/giải pháp được công ty NSP cung cấp cho thị trường Việt Nam bao gồm:</p>
                        <p>- Hạ tầng hệ thống cáp cấu trúc thương hiệu Commscope/AMP Netconnect</p>
                        <p>- Thiết bị đo kiểm hệ thống cáp cấu trúc thương hiệu Fluke Networks</p>
                        <p>- Thiết bị đánh nhãn hệ thống cáp cấu trúc thương hiệu Brady; Brother</p>
                        <p>- Hệ thống theo dõi và giám sát hạ tầng trung tâm dữ liệu (DCIM) thương hiệu Emerson Network Power/Avocent</p>
                        <p>- Hệ thống tổ chức thiết bị CNTT (tủ rack) thương hiệu Vietrack</p>
                        <p>- Bộ lưu điện thương hiệu ABB, Fredton</p>
                        <p>Ngoài lĩnh vực phân phối, NSP hiện còn là đối tác ủy quyền của các
                            nhà sản xuất uy tín trong thị trường CNTT thế giới như Fluke Networks
                            (chứng chỉ CCTT - Cable Certification Test Technician), Commscope (đối
                            tác ATA duy nhất tại thị trường Việt Nam có đủ điều kiện tổ chức các
                            khóa đào tạo Netconnect Act).</p>
                        <p>Năm 2014, ông Phạm Trung Hiếu – Phó Giám đốc NSP được chọn làm thành
                            viên ban huấn luyện đội tuyển quốc gia tham gia Cuộc thi Tay nghề ASEAN
                            lần thứ 10&nbsp; được tổ chức tại Hà Nội. NSP nhận được giấy khen từ Bộ
                            trưởng Bộ Lao động vì những đóng góp trong công tác huấn luyện thí sinh
                            nhận huy chương trong cuộc thi lần này.</p>
                        <p>Năm 2015, NSP vinh dự được chủ tịch UBND Tp. HCM tặng bằng khen chứng
                            nhận là một trong những công ty có đóng góp lớn nhất cho thị trường
                            CNTT tại Tp. HCM trong những năm gần đây.</p>
                        <p>Năm 2017, ông Phạm Trung Hiếu tiếp tục được chọn vào thành viên ban
                            huấn luyện đội tuyển quốc gia tham gia Cuộc thi Tay nghề Thế giới được
                            tổ chức tại Abu Dhabi.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
