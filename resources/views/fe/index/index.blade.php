@extends('fe.layouts.main')
@section('content')
    <div id="slider" class="nsp-slider-home inspiro-slider slider-fullscreen arrows-large arrows-creative dots-creative" data-autoplay-timeout="7500">
        @foreach($sliders as $slider)
        <div class='slide background-overlay-gradient kenburns background-image' style="background-image: url({{$slider->picture}})">
            <div class='container'>
                {!! $slider->description !!}
            </div>
        </div>
        @endforeach
    </div>
    <section id="nsp20" class="text-light p-b-40 nsp-background-color--red">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-12">
                    <h2><strong>Mừng NSP tròn 20 tuổi</strong></h2>
                    <p class="lead">Chân thành cảm ơn tình cảm quý báu và những lời chúc tốt đẹp của quý vị.</p>
                </div>
                <div class="col-lg-7">
                    <div class="carousel arrows-visibile testimonial testimonial-single testimonial-left" data-items="1" data-loop="true" data-animate-in="fadeIn" data-animate-out="fadeOut" data-arrows="false">
                        @foreach($twentyYears as $twentyYear)
                            @php
                                $regency = ($twentyYear->regency) ? $twentyYear->regency . ', ' : '';
                            @endphp
                            <div class='testimonial-item'>
                                <img style='cursor: pointer' alt='logo-20y' class='js-lazy' src="{{asset('public/uploads/images/favicon/logo-20y.png')}}">
                                <p >{{$twentyYear->comment}}</p>
                                <span class=''>{{$twentyYear->name}}</span>
                                <span style='min-height: 66px'>{{$regency}}{{$twentyYear->company}}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="productsSection" class="background-overlay lazy" style="background-image: url({{asset('/public/uploads/images/slider/bg_image_fo_patch_cords.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="col-lg-4" data-animate="fadeIn">
                        <div class="heading-text heading-section">
                            <h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">
                                Sản Phẩm
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="carousel" data-items="3" data-autoplay="false" data-dots="true">
                            @foreach($productCategories as $productCategory)
                                <div class='text-light'>
                                    <div data-animate='flipInY' data-animate-delay="200">
                                        <div class='icon-box effect small clean'>
                                            <div class='icon'>
                                                <a href='{{$productCategory->url}}'><i class='{{$productCategory->icon}}'></i></a>
                                            </div>
                                            <h3><a class='nsp-font-family--SVN-Poppins-bold' href='{{$productCategory->url}}'>{{$productCategory->name}}</a></h3>
                                            <p>{{$productCategory->description}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- SOLUTIONS -->

    <section id="solutionsSection">
        <div class="container">
            <div class="heading-text heading-section m-b-80">
                <h2 class="text-medium nsp-font-family--SVN-Poppins-semibold" >Giải pháp</h2>
            </div>
            <div class="row">
                @foreach($solutions as $solution)
                    @php
                        $url = route('fe.solution.detail', ['slug' => \Illuminate\Support\Str::slug($solution->title), 'id' => $solution->id]);
                    @endphp
                    <div class="col-lg-6">
                        <div class="icon-box medium fancy">
                            <div class="icon"> <a href="{{$url}}"><i class="{{$solution->icon}}"></i></a> </div>
                            <h4 class="nsp-font-family--SVN-Poppins-semibold"><a href="{{$url}}">{{$solution->title}}</a></h4>
                            <p>{{\Illuminate\Support\Str::words($solution->description, 35)}}</p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- SOLUTIONS -->
    <section class="text-center background-grey">
        <div class="container">
            <div class="carousel" data-items="6" data-items-sm="4" data-items-xs="3"  data-items-xxs="2"  data-margin="20" data-arrows="false" data-autoplay="true" data-autoplay-timeout="3000" data-loop="true">
                @foreach($productBrands as $productBrand)
                    <div>
                        <a title='{{$productBrand->name}}'><img  alt='{{$productBrand->name}}' src="{{$productBrand->picture}}"> </a>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <div style="background-image: url({{asset('public/uploads/images/slider/bg_image_rack_door_close_up.jpg')}})" class="lazy call-to-action p-t-100 p-b-100 background-image mb-0">
        @if($promotion)
            <div class="container">
                <div class="row">
                    <div class="col-lg-10">
                        <h3 class="text-light nsp-font-family--SVN-Poppins-semibold">{{$promotion->name}}</h3>
                        <p class="text-light">{{$promotion->short_description}}</p>
                    </div>
                    <div class="col-lg-2">
                        <a href="{{$promotion->url}}" class="btn btn-light btn-outline">XEM CHI TIẾT</a>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <!-- BLOG -->
    <section class="background-grey">
        <div class="carousel container" data-items="3" data-autoplay="false">
            @foreach($posts as $post)
                <div class='post-item border'>
                    <div class='post-item-wrap'>
                        <div class='post-image'>
                            <a href='{{$post->url}}'><img src="{{$post->thumbnail}}" /></a>
<!--                            <span class='post-meta-category'><a href=''>{$groupName}</a></span>-->
                        </div>
                        <div class='post-item-description'>
                            <span class='post-meta-date nsp-font-family--SVN-Poppins'><i class='fa fa-calendar-o'></i>{{$post->created_at}}</span>
                            <h2><a title='{{$post->name}}' href='{{$post->url}}' class='nsp-font-family--SVN-Poppins-semibold' style='font-weight: 500'>{{$post->name}}</a></h2>
                            <p class=''>{{\Illuminate\Support\Str::words($post->short_description, 15, '...')}}</p>
                            <a href='{{$post->url}}' class='item-link nsp-font-family--SVN-Poppins'>
                                Đọc tiếp <i class='fa fa-arrow-right'></i>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
    <!-- end: BLOG -->

    <!-- SERVICES -->
    <section id="" class="no-padding equalize" data-equalize-item=".text-box">
        <div class="row col-no-margin">
            <!--Box 1-->
            <div class="col-lg-6">
                <div class="text-box hover-effect text-dark js-open-request-quote-popup" style="height: 330px;">
                    <a href="{{route('fe.contact.index')}}"> <i class="fa fa-calculator"></i>
                        <h3 class="nsp-font-family--SVN-Poppins-thin">Yêu cầu báo giá</h3>
                        {{--<p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>--}}
                    </a>
                </div>
            </div>
            <!--End: Box 1-->
            <!--Box 2-->
            <div class="col-lg-6">
                <div class="text-box hover-effect text-dark" style="height: 330px;">
                    <a href="tel:+84903762695" target="_blank"> <i class="fas fa-comments"></i>
                        <h3 class="nsp-font-family--SVN-Poppins-thin">Tư vấn trực tuyến</h3>
                        {{--<p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>--}}
                    </a>
                </div>
            </div>
            <!--End: Box 2-->
            <!--Box 3-->
            {{--            <div class="col-lg-4">
                            <div class="text-box hover-effect text-dark" style="height: 330px;">
                                <a href="#"> <i class="fa fa-envelope"></i>
                                    <h3 class="nsp-font-family--SVN-Poppins-thin">Đăng ký nhận tin</h3>
                                    --}}{{--<p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>--}}{{--
                                </a>
                            </div>
                        </div>--}}
            <!--End: Box 3-->
            <!--Box 4-->
            {{--            <div class="col-lg-3">
                            <div class="text-box hover-effect text-dark" style="height: 330px;">
                                <a href="#"> <i class="fa fa-pallet"></i> <!-- fa-dolly-flatbed -->
                                    <h3 class="nsp-font-family--SVN-Poppins-thin">Xả hàng tồn kho</h3>
                                    <p>Ispendisse consectetur fringilla luctus usce id mi diam, non ornare.</p>
                                </a>
                            </div>
                        </div>--}}
            <!--End: Box 4-->
        </div>
    </section>
    <!-- end: services -->
@endsection
