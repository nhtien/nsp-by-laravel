@section('css_tag')

    <title>Công ty TNHH Nhân Sinh Phúc (NSP)</title><meta name="title" content="Công ty TNHH Nhân Sinh Phúc (NSP)">
    <meta name="description" content="Hệ thống cáp cấu trúc, thiết bị đo kiểm kết nối cáp, quản lý hạ tầng mạng data center, Rack thiết bị &amp;amp; phụ kiên, quản trị kết nối vật lý, giám sát và phân tích mạng.">
    <meta name="keywords" content="NSP; Nhan Sinh Phuc; Nhân Sinh Phúc; Network Provider; Cable; Data Center; LAN Switch; KVM; Network Testing; tu Rack; Camera IP; UPS; emerson; Fluke Networks; Vietrack; AMP; Avocent; ACTi; AMP ACT;">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="googlebot" content="index,follow">
    <meta http-equiv="robots" content="index,follow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="copyright" content="Copyright © 1999 by nsp.com.vn">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="address=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="audience" content="General">
    <meta http-equiv="content-language" content="vi">
    <meta name="resource-type" content="Document">
    <meta http-equiv="Site-Exit" content="revealTrans(Duration=3.0,Transition=23)">
    <meta http-equiv="Page-Exit" content="progid:DXImageTransform.Microsoft.GradientWipe(duration=.5)">
    <meta name="geo.region" content="VN">
    <meta name="geo.placename" content="Tp HCM">
    <meta name="geo.position" content="10.796693;106.689885">
    <meta name="ICBM" content="10.796693, 106.689885">
    <link href="/public/uploads/images/favicon/favicon_48x48.ico" rel="shortcut icon">
    <link href="/public/uploads/images/favicon/favicon_192x192.png" rel="icon" sizes="192x192" type="image/png">
    <link href="/public/uploads/images/favicon/favicon_96x96.png" rel="icon" sizes="96x96" type="image/png">
    <link href="/public/uploads/images/favicon/favicon_48x48.png" rel="icon" sizes="48x48" type="image/png">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/nsp-font.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/plugins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/style2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/responsive.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/color-variations/red.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/public/template/polo/css/nsp-style.css?t' . time())}}">
@show
