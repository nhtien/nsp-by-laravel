@section('javascript_tag')
<script src="{{asset('/public/template/polo/js/jquery.js')}}"></script>
<script src="{{asset('/public/template/polo/js/plugins.js')}}"></script>
<script src="{{asset('/public/template/polo/third-party/js/jquery-cookie/jquery.cookie.js')}}"></script>
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        const d = new Date();
        let time = d.getTime();
        let tag = document.createElement("script");
        tag.setAttribute("src", "/public/template/polo/js/functions.js?v=" + time);
        document.head.appendChild(tag);
    });
</script>
@show
