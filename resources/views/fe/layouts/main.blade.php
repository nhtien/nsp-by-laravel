<!DOCTYPE html>
<html lang="vi">
<head>
    @include('fe.layouts.blocks.css')
</head>
<body data-animation-in="fadeIn"  data-animation-out="fadeOut" data-icon="10" data-icon-color="#c3393d" data-speed-in="100" data-speed-out="500">
<div {{--class="body-inner"--}} >
    <div id="app">
        <notification></notification>
        <menu-header :is-mobile="{{$isMobile ? 'true' : 'false'}}"></menu-header>
        @yield('content')
        <menu-footer></menu-footer>
    </div>
</div>
<a id="scrollTop"><i class="icon-chevron-up1"></i><i class="icon-chevron-up1"></i></a>
@include('fe.layouts.blocks.javascript')
<script src="{{asset('/public/js/app.js?id=' . time())}}"></script>
</body>
</html>
