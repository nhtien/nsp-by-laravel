@extends('fe.layouts.main')
@section('content')
    <router-view :key="$route.fullPath" id="{{$cat->pgrp_id}}" title="{{$title}}"></router-view>
@endsection
