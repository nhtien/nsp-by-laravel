@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="page-title-center text-light background-overlay-dark" style="background-image:url('/public/uploads/images/polo-theme/bg_image_fo_patch_cords.jpg');">
        <div class="container">
            <div class="page-title">
                <span class="post-meta-category"><a href="{{$urlCat}}">{{$post->cat_name}}</a></span>
                <h1>{{$post->name}}</h1>
                <div class="small m-b-20">{{$post->created_at}}</div>
            </div>
        </div>
    </section>
    <section id="page-content" class="sidebar-right">
        <div class="container">
            <div id="blog" class="single-post col-lg-10 center">
                <div class="post-item nsp-post">
                    <div class="post-item-wrap">
                        <div class="post-item-description">
                            <div class="nsp-article-summary">{{$post->short_description}}</div>
                            <div class="nsp-article-content">{!! $post->content !!}</div>
                            <p class="text-right"><b>{{$post->author}}</b></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
