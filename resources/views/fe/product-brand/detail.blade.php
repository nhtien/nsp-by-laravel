@extends('fe.layouts.main')
@section('content')
    @if($isMobile != true)
        <section id="page-title" class="dark" style="background: url(/public/template/polo/images/pattern/pattern26.png); padding-top: 40px; padding-bottom: 80px">
        </section>
    @endif
    <section>
        <div class="container">
            <div class="heading-text heading-section text-center">
                <h2 style="text-transform: uppercase; font-weight: unset" class="text-medium">Thông tin về {{$brand->name}}</h2>
            </div>
            <div class="row">
                <div class="col-md-12" style="font-size: 16px">
                    {!! $brand->description !!}
                </div>
            </div>
        </div>
    </section>
    <section id="section4" class="background-grey">
        <div class="container">
            <div class="heading-text heading-section text-center m-b-40">
                <h2 class="m-b-0 text-medium" style="font-weight: unset">SẢN PHẨM ĐẶC TRƯNG</h2>
            </div>
            <div id="blog">
                <div id="blog" class="grid-layout post-3-columns m-b-30" data-item="post-item">
                    @foreach($products as $product)
                    <div class="post-item border">
                        <div class="post-item-wrap">
                            <div class="post-image" style="max-height: 325px">
                                <a href="#">
                                    <img alt="" src="{{$product->thumbnail}}">
                                </a>
                                {{--<span class="post-meta-category"><a href="">Lifestyle</a></span>--}}
                            </div>
                            <div class="post-item-description" style="min-height: 292px">
                                <span class="post-meta-date"><i class="fa fa-calendar-o"></i>{{$product->created_at}}</span>
                                {{--<span class="post-meta-comments"><a href=""><i class="fa fa-comments-o"></i>33 Comments</a></span>--}}
                                <h2><a href="#">{{$product->name}}</a></h2>
                                <p>{{\Illuminate\Support\Str::words($product->short_description, 25)}}</p>
                                <a href="{{$product->url}}" class="item-link">Xem chi tiết <i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-12 text-right">
                <button style="padding-left: 30px; padding-right: 30px" type="button" class="btn btn-light btn-creative btn-icon-holder btn-shadow btn-light-hover">
                    <a class="nsp-color--hover" href="{{route('fe.product-brand.find-products', ['slug' => \Illuminate\Support\Str::slug($brand->name), 'id' => $brand->prodbrd_id])}}">Xem tất cả sản phẩm<i class="fa fa-arrow-right"></i></a></button>
            </div>
        </div>
    </section>
@endsection
