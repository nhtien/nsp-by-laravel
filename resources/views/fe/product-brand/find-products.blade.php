@extends('fe.layouts.main')
@section('content')
    <router-view :key="$route.fullPath" :brand="{{$brand}}"></router-view>
@endsection
