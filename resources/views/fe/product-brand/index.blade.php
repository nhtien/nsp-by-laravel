@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="background-overlay text-light">
        <div class="parallax-container img-loaded" data-velocity="-.090" style="background: rgba(0, 0, 0, 0) url('/public/uploads/images/slider/bg_image_fo_patch_cords.jpg') repeat scroll 0% 0%;"></div>
        <div class="container">
            <div class="page-title"><h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">Các Thương Hiệu</h2></div>
        </div>
    </section>
    <div class="wrapper">
        <section id="page-content" class="sidebar-right">
            <div class="container">
                <div class="row">
                    @foreach($brands as $brand)
                        <div class="col-md-6">
                            {{--<h4>
                                <strong>
                                    <a href="{{$brand->uri}}" target="_blank" class="nsp-color--hover">{{$brand->name}}</a>
                                </strong>
                            </h4>--}}
                            <a href="{{$brand->uri}}"><img src="{{$brand->picture}}" style="display: block; margin: auto"></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
