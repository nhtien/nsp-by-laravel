@extends('fe.layouts.main')
@section('content')
    <router-view :key="$route.fullPath"></router-view>
@endsection
