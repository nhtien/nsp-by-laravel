@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="background-overlay text-light"
             data-parallax-image="/public/uploads/images/slider/bg_image_fo_patch_cords.jpg">
        <div class="container">
            <div class="page-title">
                <h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">Tài liệu sản phẩm</h2>
            </div>
        </div>
    </section>
    <section id="page-content">
        <div class="container">
            <div class="row">
                <div class="content col-lg-9">
                    @foreach($datasheets as $datasheet)
                        <div style="border-bottom: 1px solid #e3e3e3; padding-bottom: 30px; margin-bottom: 30px">
                            <h2 style="font-style: italic;font-size: 22px;line-height: 32px; margin-bottom: 0px">{{$datasheet->datasheet_name}}</h2>
                            <span style="font-weight: 600;font-size: 15px;">{{$datasheet->brand_name}}</span> <br>
                            <span style="font-weight: 500;color: #bbbbbb;font-size: 13px; font-style: italic;">{{$datasheet->part_number}}</span><br>
                            <span style="font-weight: 500;color: #bbbbbb;font-size: 13px; font-style: italic;">
                                <a href="{{$datasheet->file ? $datasheet->getAttribute('download_url') : $datasheet->link}}" target="_blank" class="nsp-color--hover"><i class="fa fa-download"></i> Tải về</a>
                            </span>
                        </div>
                    @endforeach
                    {{$datasheets->links('fe.blocks.pagination')}}
                </div>
                <div class="sidebar col-lg-3">
                    <div class="widget  widget-newsletter">
                        <form id="widget-search-form-sidebar" action="{{route('fe.product.document')}}" method="get"
                              class="form-inline">
                            <div class="input-group">
                                <input type="text" aria-required="true" name="search" value="{{$search}}"
                                       class="form-control widget-search-form" placeholder="Tìm tài liệu...">
                                <div class="input-group-append">
                                        <span class="input-group-btn">
                                            <button type="submit" id="widget-widget-search-form-button" class="btn"><i
                                                    class="fa fa-search"></i></button>
                                        </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
