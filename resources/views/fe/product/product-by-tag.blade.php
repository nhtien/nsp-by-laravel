@extends('fe.layouts.main')
@section('content')
    <router-view :key="$route.fullPath" :tagId="{{$tag->prtag_id}}" tagName="{{$tag->name}}"></router-view>
@endsection
