@extends('fe.layouts.main')
@section('content')
    <section id="page-title" class="background-overlay text-light">
        <div class="parallax-container img-loaded" data-velocity="-.090" style="background: rgba(0, 0, 0, 0) url('/public/uploads/images/slider/bg_image_fo_patch_cords.jpg') repeat scroll 0% 0%;"></div>
        <div class="container">
            <div class="page-title"><h2 class="text-medium text-light nsp-font-family--SVN-Poppins-bold">Danh Mục Sản Phẩm</h2></div>
        </div>
    </section>
    <div class="wrapper">
        <section id="page-content" class="sidebar-left">
            <div class="container">
                <div class="row">
                    @foreach($catLevel1 as $cat1)
                        <div class="col-md-4" style="margin-top: 25px">
                            <h4>
                                <strong>
                                    <a href="{{$cat1->url}}" target="_blank" class="nsp-color--hover">{{$cat1->name}}</a>
                                </strong>
                            </h4>
                            @foreach($catLevel2 as $cat2)
                                @if($cat2->parent == $cat1->prcat_id)
                                    <h5><i class="icon-minus1"></i><a href="{{$cat2->url}}" target="_blank" class="nsp-color--hover">{{$cat2->name}}</a></h5>
                                @endif
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>
@endsection
