@extends('fe.layouts.main')
@section('content')
    <router-view :key="$route.fullPath" :solution="{{$solution}}"></router-view>
@endsection
