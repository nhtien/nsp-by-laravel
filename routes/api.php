<?php

use App\Http\Controllers\API\MenuController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\ProductBrandController;
use App\Http\Controllers\API\ProductCategoryController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\ProductTagController;
use App\Http\Controllers\API\SliderController;
use App\Http\Controllers\API\SolutionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(MenuController::class)->group(function () {
    Route::get('/menus', 'index')->name('api.menu.index');
});
Route::controller(ProductCategoryController::class)->group(function () {
    Route::get('/product-categories/header-menu', 'getHeaderMenu')->name('api.product-category.header-menu');
    Route::get('/product-categories/{id}', 'detail')->name('api.product-category.detail');
    Route::get('/product-categories/level/{level}', 'getCategoriesByLevel')->name('api.product-category.by-level');
    Route::get('/product-categories/children-branch/{catId}', 'getChildrenCategoriesByBranch')->name('api.product-category.children-by-branch');
    Route::get('/product-categories/branch/{catId}', 'getCategoriesByBranch')->name('api.product-category.by-branch');
    Route::get('/product-categories/first-ancestor/{catId}', 'getFirstAncestor')->name('api.product-category.first-ancestor');
});

Route::controller(SliderController::class)->group(function () {
    Route::get('/sliders', 'index')->name('api.slider.index');
});

Route::controller(ProductBrandController::class)->group(function () {
    Route::get('/product-brands', 'index')->name('api.product-brand.index');
});

Route::controller(SolutionController::class)->group(function () {
    Route::get('/solutions', 'index')->name('api.solution.index');
});

Route::controller(ProductController::class)->group(function () {
    Route::get('/products/by-branch/{id}', 'getProductsByCategory')->name('api.product.by-branch');
    Route::get('/products/by-tag/{id}', 'getProductsByTag')->name('api.product.by-tag');
    Route::get('/products/by-solution/{id}', 'getProductsBySolution')->name('api.product.by-solution');
    Route::get('/products/by-brand/{id}', 'getProductsByBrand')->name('api.product.by-brand');
    Route::get('/products/related/{id}', 'getProductsRelated')->name('api.product.related');
    Route::get('/products/{id}', 'detail')->name('api.product.detail');
});

Route::controller(ProductTagController::class)->group(function () {
    Route::get('/product-tags/by-branch/{branchId}', 'getTagsByBranch')->name('api.product-tag.by-branch');
});

Route::controller(PostController::class)->group(function () {
    Route::get('/posts/by-category/{catId}', 'findsByCate')->name('api.post.by-category');
});
