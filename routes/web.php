<?php

use App\Http\Controllers\BE\Auth\LoginController;
use App\Http\Controllers\BE\Auth\LogoutController;
use App\Http\Controllers\BE\CkfinderController;
use App\Http\Controllers\BE\ConfigController;
use App\Http\Controllers\BE\ContactController;
use App\Http\Controllers\BE\CronController;
use App\Http\Controllers\BE\IndexController;
use App\Http\Controllers\BE\MenuController;
use App\Http\Controllers\BE\MenuFrontendController;
use App\Http\Controllers\BE\ModelLogController;
use App\Http\Controllers\BE\PostController;
use App\Http\Controllers\BE\PostGroupController;
use App\Http\Controllers\BE\ProductBrandController;
use App\Http\Controllers\BE\ProductCategoryController;
use App\Http\Controllers\BE\ProductChildrenController;
use App\Http\Controllers\BE\ProductController;
use App\Http\Controllers\BE\ProductDatasheetController;
use App\Http\Controllers\BE\ProductDatasheetGroupController;
use App\Http\Controllers\BE\ProductFileController;
use App\Http\Controllers\BE\ProductPropertyController;
use App\Http\Controllers\BE\ProductTagController;
use App\Http\Controllers\BE\SliderController;
use App\Http\Controllers\BE\SliderGroupController;
use App\Http\Controllers\BE\SolutionController;
use App\Http\Controllers\BE\TestingController;
use App\Http\Controllers\BE\UserController;
use App\Http\Controllers\BE\UserGroupController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
|--------------------------------------------------------------------------
| Route(s) - FE
|--------------------------------------------------------------------------
*/
Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\ProductBrandController::class)->group(function () {
        Route::get('thuong-hieu.html', 'index')->name('fe.product-brand.index');
        Route::get('thuong-hieu/{slug}-{id}.html', 'detail')->name('fe.product-brand.detail');
        Route::get('thuong-hieu/san-pham/{slug}-{id}.html', 'findProducts')->name('fe.product-brand.find-products');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\IndexController::class)->group(function () {
        Route::get('/', 'index')->name('fe.index.index');
        Route::get('/trang-chu.html', 'homepage')->name('fe.index.homepage');
        Route::get('ve-chung-toi.html', 'aboutUs')->name('fe.index.aboutUs');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\ProductController::class)->group(function () {
        Route::get('/product/test.html', 'test')->name('fe.product.test');
        Route::get('/tags/{slug}-{hashId}.html', 'findsProductByTag')->name('fe.product.finds-product-by-tag');
        Route::get('/danh-muc-san-pham.html', 'findsProductCategory')->name('fe.product.finds-product-category');
        Route::get('/san-pham.html', 'index')->name('fe.product.index');
        Route::get('/nhom-san-pham/{slug}-{id}.html', 'getProductsByCategory')->name('fe.product.get-product-by-category');
        Route::get('/chi-tiet-san-pham/{slug}-{id}.html', 'detail')->name('fe.product.detail');
        Route::get('/tai-lieu-san-pham.html', 'document')->name('fe.product.document');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\SolutionController::class)->group(function () {
        Route::get('/giai-phap/{slug}-{id}.html', 'detail')->name('fe.solution.detail');
        Route::get('/giai-phap.html', 'index')->name('fe.solution.index');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\ContactController::class)->group(function () {
        Route::get('/lien-he.html', 'index')->name('fe.contact.index');
        Route::post('/lien-he.html', 'store')->name('fe.contact.store');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\PostController::class)->group(function () {
        Route::get('/bai-viet/tin-tuc.html', 'findsNews')->name('fe.post.finds-news');
        Route::get('/bai-viet/kien-thuc.html', 'findsKnowledge')->name('fe.post.knowledge');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\ProductDatasheetController::class)->group(function () {
        Route::get('/download/datasheet-{hashId}.html', 'downloadDatasheet')->name('fe.product-datasheet.download');
    });
});

Route::prefix('')->group(function () {
    Route::controller(\App\Http\Controllers\FE\PostController::class)->group(function () {
        Route::get('/bai-viet/{slug}-{id}.html', 'show')->name('fe.post.show');
    });
});

/*
|--------------------------------------------------------------------------
| Login/Logout Route(s) - BE
|--------------------------------------------------------------------------
*/
Route::controller(LoginController::class)->group(function () {
    Route::get('/login.html', 'loginForm')->name('login')->middleware('throttle:10,5');
    Route::post('/login.html', 'login')->middleware('throttle:10,5');
});

Route::controller(LogoutController::class)->group(function () {
    Route::get('/logout.html', 'logout')->name('logout');
    Route::get('/logout-other-devices.html', 'logoutForm')->name('logout-other-devices');
    Route::post('/logout-other-devices.html', 'logoutOtherDevices');
});

/*
|--------------------------------------------------------------------------
| BE
|--------------------------------------------------------------------------
*/
Route::get('/nsp_admin', [IndexController::class, 'dashboard'])->name('dashboard-backend-2')->middleware('auth');
Route::middleware(['auth'])->group(function () {
    $backend = config('constants.route.backend');

    Route::prefix($backend)->group(function () {
        Route::controller(IndexController::class)->group(function () {
            Route::get('/dashboard.html', 'dashboard')->name('dashboard-backend');
            Route::get('/index/cache', 'cache')->name('be.index.cache');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ContactController::class)->group(function () {
            Route::get('/contacts/index', 'index')->name('be.contact.index');
            Route::get('/contacts/view/{id}', 'view')->name('be.contact.view');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(UserController::class)->group(function () {
            Route::get('/users/index', 'index')->name('be.users.index');
            Route::get('/users/create', 'create')->name('be.users.create');
            Route::get('/users/edit', 'edit')->name('be.users.edit');
            Route::put('/users/update', 'update')->name('be.users.update');
            Route::post('/users/store', 'store')->name('be.users.store');
            Route::delete('/users/destroy', 'destroy')->name('be.users.destroy');
            Route::get('/users/edit-profile', 'editProfile')->name('be.users.edit-profile');
            Route::put('/users/update-profile', 'updateProfile')->name('be.users.update-profile');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ModelLogController::class)->group(function () {
            Route::get('/model-log/index', 'index')->name('be.modellog.index');
            Route::get('/model-log/show', 'show')->name('be.modellog.show');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(SolutionController::class)->group(function () {
            Route::get('/solution/index', 'index')->name('be.solution.index');
            Route::get('/solution/edit', 'edit')->name('be.solution.edit');
            Route::put('/solution/update', 'update')->name('be.solution.update');
            Route::get('/solution/create', 'create')->name('be.solution.create');
            Route::post('/solution/store', 'store')->name('be.solution.store');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ConfigController::class)->group(function () {
            Route::get('/config/index', 'index')->name('be.config.index');
            Route::get('/config/create', 'create')->name('be.config.create');
            Route::get('/config/edit', 'edit')->name('be.config.edit');
            Route::put('/config/update', 'update')->name('be.config.update');
            Route::post('/config/store', 'store')->name('be.config.store');
            Route::delete('/config/destroy', 'destroy')->name('be.config.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(MenuController::class)->group(function () {
            Route::get('/menu/index', 'index')->name('be.menu.index');
            Route::get('/menu/create', 'create')->name('be.menu.create');
            Route::get('/menu/edit', 'edit')->name('be.menu.edit');
            Route::put('/menu/update', 'update')->name('be.menu.update');
            Route::post('/menu/store', 'store')->name('be.menu.store');
            //Route::delete('/menu/destroy',  'destroy')->name( "be.menu.destroy");
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(UserGroupController::class)->group(function () {
            Route::get('/user-group/index', 'index')->name('be.usergroup.index');
            Route::get('/user-group/create', 'create')->name('be.usergroup.create');
            Route::get('/user-group/edit', 'edit')->name('be.usergroup.edit');
            Route::put('/user-group/update', 'update')->name('be.usergroup.update');
            Route::post('/user-group/store', 'store')->name('be.usergroup.store');
            //Route::delete('/user-group/destroy',  'destroy')->name( "be.usergroup.destroy");
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(PostController::class)->group(function () {
            Route::get('/post/index', 'index')->name('be.post.index');
            Route::get('/post/create', 'create')->name('be.post.create');
            Route::get('/post/edit', 'edit')->name('be.post.edit');
            Route::put('/post/update', 'update')->name('be.post.update');
            Route::post('/post/store', 'store')->name('be.post.store');
            Route::delete('/post/destroy', 'destroy')->name('be.post.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(PostGroupController::class)->group(function () {
            Route::get('/post-group/index', 'index')->name('be.postgroup.index');
            Route::get('/post-group/create', 'create')->name('be.postgroup.create');
            Route::get('/post-group/edit', 'edit')->name('be.postgroup.edit');
            Route::put('/post-group/update', 'update')->name('be.postgroup.update');
            Route::post('/post-group/store', 'store')->name('be.postgroup.store');
            Route::delete('/post-group/destroy', 'destroy')->name('be.postgroup.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(MenuFrontendController::class)->group(function () {
            Route::get('/menu-frontend/index', 'index')->name('be.menufrontend.index');
            Route::get('/menu-frontend/create', 'create')->name('be.menufrontend.create');
            Route::get('/menu-frontend/edit', 'edit')->name('be.menufrontend.edit');
            Route::put('/menu-frontend/update', 'update')->name('be.menufrontend.update');
            Route::post('/menu-frontend/store', 'store')->name('be.menufrontend.store');
            Route::delete('/menu-frontend/destroy', 'destroy')->name('be.menufrontend.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(SliderGroupController::class)->group(function () {
            Route::get('/slider-group/index', 'index')->name('be.slidergroup.index');
            Route::get('/slider-group/create', 'create')->name('be.slidergroup.create');
            Route::get('/slider-group/edit', 'edit')->name('be.slidergroup.edit');
            Route::put('/slider-group/update', 'update')->name('be.slidergroup.update');
            Route::post('/slider-group/store', 'store')->name('be.slidergroup.store');
            Route::delete('/slider-group/destroy', 'destroy')->name('be.slidergroup.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(SliderController::class)->group(function () {
            Route::get('/slider/index', 'index')->name('be.slider.index');
            Route::get('/slider/create', 'create')->name('be.slider.create');
            Route::get('/slider/edit', 'edit')->name('be.slider.edit');
            Route::put('/slider/update', 'update')->name('be.slider.update');
            Route::post('/slider/store', 'store')->name('be.slider.store');
            Route::delete('/slider/destroy', 'destroy')->name('be.slider.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductBrandController::class)->group(function () {
            Route::get('/product-brand/index', 'index')->name('be.productbrand.index');
            Route::get('/product-brand/create', 'create')->name('be.productbrand.create');
            Route::get('/product-brand/edit', 'edit')->name('be.productbrand.edit');
            Route::put('/product-brand/update', 'update')->name('be.productbrand.update');
            Route::post('/product-brand/store', 'store')->name('be.productbrand.store');
            //Route::delete('/product-brand/destroy',  'destroy')->name( "be.productbrand.destroy");
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductDatasheetGroupController::class)->group(function () {
            Route::get('/product-datasheet-group/index', 'index')->name('be.productdatasheetgroup.index');
            Route::get('/product-datasheet-group/create', 'create')->name('be.productdatasheetgroup.create');
            Route::get('/product-datasheet-group/edit', 'edit')->name('be.productdatasheetgroup.edit');
            Route::put('/product-datasheet-group/update', 'update')->name('be.productdatasheetgroup.update');
            Route::post('/product-datasheet-group/store', 'store')->name('be.productdatasheetgroup.store');
            //Route::delete('/product-datasheet-group/destroy',  'destroy')->name( "be.productdatasheetgroup.destroy");
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductPropertyController::class)->group(function () {
            Route::get('/product-property/index', 'index')->name('be.productproperty.index');
            Route::get('/product-property/create', 'create')->name('be.productproperty.create');
            Route::get('/product-property/edit', 'edit')->name('be.productproperty.edit');
            Route::put('/product-property/update', 'update')->name('be.productproperty.update');
            Route::post('/product-property/store', 'store')->name('be.productproperty.store');
            Route::delete('/product-property/destroy', 'destroy')->name('be.productproperty.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductDatasheetController::class)->group(function () {
            Route::get('/product-datasheet/index', 'index')->name('be.productdatasheet.index');
            Route::get('/product-datasheet/create', 'create')->name('be.productdatasheet.create');
            Route::get('/product-datasheet/edit', 'edit')->name('be.productdatasheet.edit');
            Route::put('/product-datasheet/update', 'update')->name('be.productdatasheet.update');
            Route::post('/product-datasheet/store', 'store')->name('be.productdatasheet.store');
            Route::delete('/product-datasheet/destroy', 'destroy')->name('be.productdatasheet.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductFileController::class)->group(function () {
            Route::get('/product-file/index', 'index')->name('be.productfile.index');
            Route::get('/product-file/create', 'create')->name('be.productfile.create');
            Route::get('/product-file/edit', 'edit')->name('be.productfile.edit');
            Route::put('/product-file/update', 'update')->name('be.productfile.update');
            Route::post('/product-file/store', 'store')->name('be.productfile.store');
            Route::delete('/product-file/destroy', 'destroy')->name('be.productfile.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductTagController::class)->group(function () {
            Route::get('/product-tag/index', 'index')->name('be.producttag.index');
            Route::get('/product-tag/create', 'create')->name('be.producttag.create');
            Route::get('/product-tag/edit', 'edit')->name('be.producttag.edit');
            Route::put('/product-tag/update', 'update')->name('be.producttag.update');
            Route::post('/product-tag/store', 'store')->name('be.producttag.store');
            Route::delete('/product-tag/destroy', 'destroy')->name('be.producttag.destroy');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductCategoryController::class)->group(function () {
            Route::get('/product-category/index', 'index')->name('be.productcategory.index');
            Route::get('/product-category/create', 'create')->name('be.productcategory.create');
            Route::get('/product-category/edit', 'edit')->name('be.productcategory.edit');
            Route::put('/product-category/update', 'update')->name('be.productcategory.update');
            Route::post('/product-category/store', 'store')->name('be.productcategory.store');
            //Route::delete('/product-category/destroy',  'destroy')->name( "be.productcategory.destroy");
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductController::class)->group(function () {
            Route::get('/product/index', 'index')->name('be.product.index');
            Route::get('/product/create', 'create')->name('be.product.create');
            Route::get('/product/edit', 'edit')->name('be.product.edit');
            Route::put('/product/update', 'update')->name('be.product.update');
            Route::post('/product/store', 'store')->name('be.product.store');
            Route::delete('/product/destroy', 'destroy')->name('be.product.destroy');
            Route::get('/product/clone/{id}', 'clone')->name('be.product.clone');
        });
    });

    Route::prefix($backend)->group(function () {
        Route::controller(ProductChildrenController::class)->group(function () {
            Route::get('/product-children/index', 'index')->name('be.productchildren.index');
            Route::get('/product-children/create', 'create')->name('be.productchildren.create');
            Route::get('/product-children/edit', 'edit')->name('be.productchildren.edit');
            Route::put('/product-children/update', 'update')->name('be.productchildren.update');
            Route::post('/product-children/store', 'store')->name('be.productchildren.store');
            Route::delete('/product-children/destroy', 'destroy')->name('be.productchildren.destroy');
        });
    });

    // Testing
    Route::prefix($backend)->group(function () {
        Route::controller(TestingController::class)->group(function () {
            Route::get('/testing/send-mail', 'sendMail')->name('be.testing.send-mail');
        });
    });

    Route::controller(CkfinderController::class)->group(function () {
        Route::get('/ckfinder-show.html', 'show')->name('be.ckfinder.show');
    });

    Route::prefix($backend)->group(function () {
        Route::controller(CronController::class)->group(function () {
            Route::get('/cron/create-folder-by-product.cron', 'createFolderByProduct');
        });
    });
});

/*
|--------------------------------------------------------------------------
| System Cache
|--------------------------------------------------------------------------
*/
Route::prefix('systems')->group(function () {
    Route::get('/optimize-cache', function () {
        Artisan::call('optimize');

        return '<h1>Reoptimized class loader</h1>';
    });

    Route::get('/route-cache', function () {
        Artisan::call('route:cache');

        return '<h1>Routes cached</h1>';
    });

    Route::get('/config-cache', function () {
        Artisan::call('config:cache');

        return '<h1>Config cached</h1>';
    });

    Route::get('/optimize-clear', function () {
        Artisan::call('optimize:clear');

        return '<h1>optimize:clear</h1>';
    });

    Route::get('/config-clear', function () {
        Artisan::call('config:clear');

        return '<h1>Configuration cache cleared!</h1>';
    });

    Route::get('/clear-cache', function () {
        Artisan::call('cache:clear');

        return '<h1>Cache facade value cleared</h1>';
    });

    Route::get('/route-clear', function () {
        Artisan::call('route:clear');

        return '<h1>Route cache cleared</h1>';
    });

    Route::get('/view-clear', function () {
        Artisan::call('view:clear');

        return '<h1>View cache cleared</h1>';
    });
});
